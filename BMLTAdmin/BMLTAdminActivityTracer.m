//
//  BMLTAdminActivityTracer.m
//  BMLTAdmin
//
//  Created by Chris Marshall on 8/3/16.
//  Copyright © 2016 MAGSHARE. All rights reserved.
//

#import "BMLTAdminActivityTracer.h"

void BMLTAdminActivityTracer_OSTrace(int inNumber)
{
    os_trace("Trace Point: %d", inNumber);
}
