//
//  BMLTAdminAppDelegate.swift
//  BMLTAdmin
/**
    :license:
    Created by MAGSHARE.
    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BMLT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this code.  If not, see <http://www.gnu.org/licenses/>.
*/

import UIKit
import MapKit

// MARK: - Class Extensions -
/* ###################################################################################################################################### */
/**
    This allows us to abstract the Tab Bar color displayed when the app is logged in.
*/
extension UITabBar {
    /* ################################################################## */
    /**
        This returns a color to be used while logged in.
    
        :returns: a UIColor, with the bar color.
    */
    class func getLoggedInColor() -> UIColor {
        return UIColor(red: 0, green: 0.25, blue: 0.1, alpha: 1)    // It's a dark green.
    }
}

/* ################################################################## */
/**
 A couple of basic location coordinate extensions.
 */
extension CLLocationCoordinate2D {
    /* ################################################################## */
    /**
     These return the values as Strings.
     */
    var latString: String {
        get {
            return String(self.latitude)
        }
    }
    
    var longString: String {
        get {
            return String(self.longitude)
        }
    }
    
    /* ################################################################## */
    /**
     Initialize from string values.
     
     :param: latString The latitude value, as a String
     :param: longString The longitude value, as a String
     */
    init(latitude: String, longitude: String) {
        self.latitude = CLLocationDegrees(latitude)!
        self.longitude = CLLocationDegrees(longitude)!
    }
    
    /* ################################################################## */
    /**
     This calculates a distance to another location.
     
     From here: http://stackoverflow.com/questions/3905896/distancefromlocation-calculate-distance-between-two-points#answer-33327551
     
     :returns: a distance, in meters.
     */
    func distanceInMetersFrom(_ otherCoord : CLLocationCoordinate2D) -> CLLocationDistance {
        let firstLoc = CLLocation(latitude: self.latitude, longitude: self.longitude)
        let secondLoc = CLLocation(latitude: otherCoord.latitude, longitude: otherCoord.longitude)
        return firstLoc.distance(from: secondLoc)
    }
}

/* ###################################################################################################################################### */
/**
    This adds various functionality to the String class.
*/
extension String {
    /* ################################################################## */
    /**
        This tests a string to see if a given substring is present at the start.
    
        :param: inSubstring The substring to test.
    
        :returns: true, if the string begins with the given substring.
    */
    func beginsWith (_ inSubstring: String) -> Bool {
        var ret: Bool = false
        if let range = self.range(of: inSubstring) {
            ret = (range.lowerBound == self.startIndex)
        }
        return ret
    }
    
    /* ################################################################## */
    /**
     The following function comes from this: http://stackoverflow.com/a/27736118/879365
     
     This extension function cleans up a URI string.
     
     :returns: a string, cleaned for URI.
     */
    func URLEncodedString() -> String? {
        let customAllowedSet =  CharacterSet.urlQueryAllowed
        return self.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
    }
    
    /* ################################################################## */
    /**
     The following function comes from this: http://stackoverflow.com/a/27736118/879365
     
     This extension function creates a URI query string from given parameters.
     
     :param: parameters a dictionary containing query parameters and their values.
     
     :returns: a String, with the parameter list.
     */
    static func queryStringFromParameters(_ parameters: Dictionary<String,String>) -> String? {
        if (parameters.count == 0)
        {
            return nil
        }
        var queryString : String? = nil
        for (key, value) in parameters {
            if let encodedKey = key.URLEncodedString() {
                if let encodedValue = value.URLEncodedString() {
                    if queryString == nil
                    {
                        queryString = "?"
                    }
                    else
                    {
                        queryString! += "&"
                    }
                    queryString! += encodedKey + "=" + encodedValue
                }
            }
        }
        return queryString
    }
}

/* ###################################################################################################################################### */
/**
 This adds a quick way to find the first responder in a view hierarchy, and to close the keyboard.
 */
extension UIView {
    /* ################################################################## */
    /**
     This will return the first responder of a view hierarchy.
     
     :returns: the first responder. Nil if none.
     */
    func findFirstResponder() -> UIResponder! {
        if self.isFirstResponder {
            return self
        }
        for subView in self.subviews {
            if let responder = subView.findFirstResponder() {
                return responder
            }
        }
        return nil
    }
    
    /* ################################################################## */
    /**
     This will close the keyboard.
     
     Making this an IBAction means that it can be linked in a storyboard or Interface Builder screen.
     
     :param: sender This is ignored. It's here for the signature.
     */
    @IBAction func closeKeyboard(_ sender: AnyObject) {
        if let firstResponder = self.findFirstResponder() {
            if (firstResponder.isKind(of: UITextView.self)) || (firstResponder.isKind(of: UITextField.self)) {
                firstResponder.resignFirstResponder()
            }
        }
    }
}

/* ###################################################################################################################################### */
/**
 This adds a quick way to find the first responder in a view hierarchy, and to close the keyboard.
 */
extension UIViewController {
    /* ################################################################## */
    /**
     This will return the first responder of a view hierarchy.
     
     :returns: the first responder. Nil if none.
     */
    func findFirstResponder() -> UIResponder! {
        for subView in self.view.subviews {
            if let responder = subView.findFirstResponder() {
                return responder
            }
        }
        return nil
    }
    
    /* ################################################################## */
    /**
     This will close the keyboard.
     
     Making this an IBAction means that it can be linked in a storyboard or Interface Builder screen.
     
     :param: sender This is ignored. It's here for the signature.
     */
    @IBAction func closeKeyboard(_ sender: AnyObject) {
        if let firstResponder = self.findFirstResponder() {
            if (firstResponder.isKind(of: UITextView.self)) || (firstResponder.isKind(of: UITextField.self)) {
                firstResponder.resignFirstResponder()
            }
        }
    }
}

// MARK: - Classes -
/* ###################################################################################################################################### */
/**
 This is a simple class that allows you to set an IB object to display the version from the main Info.plist
 */
class BMLTAdminInfoPlistVersionDisplayLabel : UILabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) {
                let string1 = dict["CFBundleShortVersionString"] as! String
                let string2 = dict["CFBundleVersion"] as! String
                self.text = NSLocalizedString("LOCAL-SETTINGS-VERSION-LABEL", comment: "") + " " + string1 + "." + string2
            }
        }
    }
}

/* ###################################################################################################################################### */
/**
 This class implements a global-scope preferences manager for the app. It will store the preferences in the bundle persistent prefs, and
 is composed of a Dictionary of Dictionaries.
 It uses the standard user defaults to save the data, so it isn't very robust, but it does the job fine for basic needs.
 */
class BMLTAdminPrefs {
    // MARK: - Class Types -
    /** This is the type for the basic stored login. */
    typealias loginTuple = (url: String, loginID: String)
    
    // MARK: - Class Enums -
    /* ################################################################################################################################## */
    /** These are the keys for the individual prefs types. */
    enum PrefsKeys: String {
        case Logins                 = "BMLTAdminMainPrefs_Logins"
        case SelectedServiceBodies  = "BMLTAdminMainPrefs_LoginSelectedServiceBodies"
        case LastSuccessfulLogin    = "BMLTAdminMainPrefs_LastSuccessfulLogin"
        case TimeoutSetting         = "BMLTAdminMainPrefs_TimeoutSetting"
        case RequireSSL             = "BMLTAdminMainPrefs_RequireSSL"
        case DefaultDuration        = "BMLTAdminMainPrefs_DefaultDuration"
    }
    
    // MARK: - Internal Constants -
    /* ################################################################################################################################## */
    /** This is the key we save the main prefs Dictionary under. */
    let _mainPrefsKey: String   = "BMLTAdminMainPrefs"
    
    // MARK: - Internal Properties -
    /* ################################################################################################################################## */
    /** This contains our loaded prefs Dictionary. */
    var _loadedPrefs: NSMutableDictionary! = nil
    
    // MARK: - Dynamic Properties -
    /* ################################################################################################################################## */
    /** This property is for the "Requires SSL" switch. */
    var requireSSL: Bool {
        /* ################################################################## */
        /**
         This method fetches the require SSL setting.
         */
        get {
            var ret: Bool! = nil
            
            if self._loadPrefs() {
                if let temp:Bool = self._loadedPrefs.object(forKey: PrefsKeys.RequireSSL.rawValue) as? Bool {
                    ret = temp
                }
            }
            
            if nil == ret {
                ret = false
            }
            
            return ret
        }
        
        /* ################################################################## */
        /**
         This method saves the require SSL setting.
         */
        set {
            if self._loadPrefs() {
                self._loadedPrefs.setObject(newValue, forKey: PrefsKeys.RequireSSL.rawValue as NSCopying)
                self._savePrefs()
                if newValue {
                    BMLTAdminAppDelegate.validateSession()
                }
            }
        }
    }
    
    /** This is the timeout disconnnection pref. */
    var timeoutInSeconds: Int {
        /* ################################################################## */
        /**
         This method fetches the timeout setting.
         */
        get {
            var ret: Int! = nil
            
            if self._loadPrefs() {
                if let temp:Int = self._loadedPrefs.object(forKey: PrefsKeys.TimeoutSetting.rawValue) as? Int {
                    ret = temp
                }
            }
            
            if nil == ret {
                ret = BMLTAdminAppDelegate.AutoDisconnectTimeouts.tenSeconds.rawValue
            }
            
            return ret
        }
        
        /* ################################################################## */
        /**
         This method saves the timeout setting.
         */
        set {
            if self._loadPrefs() {
                self._loadedPrefs.setObject(newValue, forKey: PrefsKeys.TimeoutSetting.rawValue as NSCopying)
                self._savePrefs()
            }
        }
    }
    
    var defaultDuration: Int {
        /* ################################################################## */
        /**
         This method returns the default duration setting.
         */
        get {
            var ret: Int! = nil
            
            if self._loadPrefs() {
                if let temp:Int = self._loadedPrefs.object(forKey: PrefsKeys.DefaultDuration.rawValue) as? Int {
                    ret = temp
                }
            }
            
            if nil == ret {
                ret = 60
            }
            
            return ret
        }
        
        /* ################################################################## */
        /**
         This method saves the default duration setting.
         */
        set {
            if self._loadPrefs() {
                self._loadedPrefs.setObject(newValue, forKey: PrefsKeys.DefaultDuration.rawValue as NSCopying)
                self._savePrefs()
            }
        }
    }
    
    // MARK: - Private Methods -
    /* ################################################################################################################################## */
    /* ################################################################## */
    /**
     This method simply saves the main preferences Dictionary into the standard user defaults.
     */
    func _savePrefs() {
        UserDefaults.standard.set(self._loadedPrefs, forKey: self._mainPrefsKey)
    }
    
    /* ################################################################## */
    /**
     This method loads the main prefs into our instance storage.
     
     NOTE: This will overwrite any unsaved changes to the current _loadedPrefs property.
     
     :returns: a Bool. True, if the load was successful.
     */
    func _loadPrefs() -> Bool {
        let temp = UserDefaults.standard.object(forKey: self._mainPrefsKey) as? NSDictionary
        
        if nil == temp {
            self._loadedPrefs = NSMutableDictionary()
        } else {
            self._loadedPrefs = NSMutableDictionary(dictionary: temp!)
        }
        
        return nil != self._loadedPrefs
    }
    
    // MARK: - Instance Methods -
    /* ################################################################################################################################## */
    /* ################################################################## */
    /**
     This method fetches the last successful login URI and Login ID.
     
     :returns: an optional LoginTuple. Nil, if no last successful Login
     */
    func getLastSuccessfulLoginFromPref() -> loginTuple! {
        var ret: loginTuple! = nil
        
        if self._loadPrefs() {
            if let temp:[String] = self._loadedPrefs.object(forKey: PrefsKeys.LastSuccessfulLogin.rawValue) as? [String] {
                ret = (url: temp[0], loginID: temp[1])
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This method saves the last successful login URI and Login ID.
     
     :param: inURI The URI to be stored.
     :param: inLoginID The login to be stored.

     :returns: a Bool. True, if the store was successful.
     */
    func setLastSuccessfulLoginToPref(_ inURI: String, inLoginID: String) -> Bool {
        var ret: Bool = false
        
        if self._loadPrefs() {
            self._loadedPrefs.setObject([inURI, inLoginID], forKey: PrefsKeys.LastSuccessfulLogin.rawValue as NSCopying)
            self._savePrefs()
            ret = true
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This method fetches the stored URIs and Login IDs.
     
     :param: inURI optional String. If supplied with a URI, then only logins for that URI are returned.
     
     :returns: an Array of tuples with the uri and the login. This array is unsorted.
     */
    func getLoginsFromPref(_ inURI: String!) -> [loginTuple] {
        var ret: [loginTuple] = []
        var temp: NSArray
        
        if self._loadPrefs() {
            if nil != self._loadedPrefs.object(forKey: PrefsKeys.Logins.rawValue) {
                temp = self._loadedPrefs.object(forKey: PrefsKeys.Logins.rawValue) as! NSArray
                for c: Int in 0..<temp.count {
                    let value = temp[c] as! [String]
                    // We can't store tuples, so the data was stored in small 2-member String Array objects.
                    var castValue: loginTuple
                    castValue.url = value[0]
                    castValue.loginID = value[1]
                    
                    // If we are getting all stored objects, or are only looking for ones for a particular URI, and this is a login for that URI, we add it to the returned Array.
                    if (nil == inURI) || ((nil != inURI) && (castValue.url == inURI)) {
                        ret.append(castValue)
                    }
                }
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This method saves a login into the preferences, or deletes one or a URI's worth from the preferences.
     
     This works by rebuilding the list, minus the specific login being added/deleted, or, if a group of logins is being deleted, minus that group.
     After that, if a specific login is being added, then it is appended to the list and saved.
     If inDeleteLogin is true, the login is deleted. If inLogin is nil, and inDeleteLogin is true, then every login in the URI in inURI is deleted.
     It is illegal for inDeleteLogin to be false, and inLogin to be nil.
     
     :param: inURI The URI, as a String.
     :param: inLogin The login ID as an optional String. If nil, and inDeleteLogin is true, then all the logins for this URI are to be deleted.
     :param: inDeleteLogin A Bool. This requires that at least inURI be set to a valid value. If false, then both inURI and inLogin need to be provided.
     
     :returns: a Bool, true, if the store/removal was successful.
     */
    func setLoginPref(_ inURI: String, inLogin: String!, inDeleteLogin: Bool) -> Bool {
        let currentLogins = self.getLoginsFromPref(nil) // Get all of the current logins.
        var ret: Bool = inDeleteLogin
        var newLogins: [loginTuple] = []
        
        // What we do here, is cycle through the existing logins.
        // If we find we already have this login in our records, or we are deleting it, we set the return value to true.
        // This prevents it from being appended to the end.
        for loginValue: loginTuple in currentLogins {
            if !inURI.isEmpty && (nil != inLogin) && !inLogin.isEmpty {
                if (loginValue.loginID != inLogin) || (loginValue.url != inURI) {
                    newLogins.append(loginValue)
                }
            }
        }
        
        // If we aren't deleting or already have the login, we simply append it.
        if !inDeleteLogin {
            newLogins.append((url: inURI, loginID: inLogin))
            ret = true
        }
        
        // Re-save our logins.
        if ret {
            let savedArray = NSMutableArray()
            
            for individualLogin:loginTuple in newLogins {
                // Because we can't store tuples, we cast them into small, 2-member String Array objects.
                let item: [NSString] = [individualLogin.url as NSString, individualLogin.loginID as NSString]
                savedArray.add(item as NSArray)
            }
            
            self._loadedPrefs.setObject(savedArray, forKey: PrefsKeys.Logins.rawValue as NSCopying)
            self._savePrefs()
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This method fetches the Service Bodies preference as a generic Dictionary.
     
     :returns: an optional NSDictionary. Nil, if the prefs doesn't exist.
     */
    func getSBPref() -> NSDictionary! {
        var ret: NSDictionary! = nil
        
        if self._loadPrefs() {
            if nil != self._loadedPrefs.object(forKey: PrefsKeys.SelectedServiceBodies.rawValue) {
                ret = self._loadedPrefs.object(forKey: PrefsKeys.SelectedServiceBodies.rawValue) as! NSDictionary
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This method fetches the stored Service bodies for a login.
     
     :param: inSBKey a String, with the login key.
     
     :returns: an Array of Int.
     */
    func getServiceBodies(_ inSBKey: String) -> [Int] {
        var ret: [Int] = []
        
        if let temp = self.getSBPref() {
            if let temp2 = temp.object(forKey: inSBKey) as! NSArray! {
                for c: Int in 0 ..< temp2.count {
                    ret.append((temp2[c] as AnyObject).intValue)
                }
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This method saves a selected Service body list to the preferences.
     
     :param: inSBKey The login/URI key, as a String.
     :param: inValue The value to be stored (an Array of Int). If nil, then this key is to be deleted.
     
     :returns: a Bool, true, if the store/removal was successful.
     */
    func setServiceBodies(_ inSBKey: String, inValue: [Int]!) -> Bool {
        var curr = self.getSBPref()
        if nil == curr {
            curr = NSDictionary()
        }
        
        let currentBodies = NSMutableDictionary(dictionary: curr!)
        var ret: Bool = false
        
        if nil != inValue {
            let storeArray = NSMutableArray()
            
            for temp in inValue {
                storeArray.add(temp as NSNumber)
            }
            
            currentBodies.setObject(storeArray, forKey: inSBKey as NSCopying)
            
            ret = true
        } else {
            ret = true
            currentBodies.removeObject(forKey: inSBKey)
        }
        
        if ret {
            self._loadedPrefs.setObject(currentBodies, forKey: PrefsKeys.SelectedServiceBodies.rawValue as NSCopying)
            self._savePrefs()
        }
        
        return ret
    }
}

/* ###################################################################################################################################### */
/**
    In addition to being the App Delegate, this class will also be the "communication hub" of the app. All of the actual server
    communication will go through this app. The individual view controllers are responsible for handling the responses, but they
    will use an instance of this class (and a set of static class methods) to handle the server interaction.

    The app delegate will be a "hub," and will maintain a number of static properties and class methods that can be accessed by the view controllers.
*/
@UIApplicationMain class BMLTAdminAppDelegate: UIResponder, UIApplicationDelegate, UITabBarControllerDelegate, URLSessionTaskDelegate {
    // MARK: - Public Enumerations -
    /** The indexes for the three view controllers. */
    enum ControllerIndex: Int {
                /** Settings */
        case    settings = 0
                /** Connect */
        case    connect = 1
                /** Search for Meetings. */
        case    search  = 2
                /** Deleted Meetings Page. */
        case    deleted = 3
                /** New Meeting Page. */
        case    new     = 4
    }
    
    /** These are the choices for auto-logout timeouts, in seconds. */
    enum AutoDisconnectTimeouts: Int {
        case    never           = -1    /** Special case. If this is selected, then there will be no timeout. */
        case    immediately     = 0
        case    fiveSeconds     = 5
        case    tenSeconds      = 10
        case    twentySeconds   = 20
        case    thirtySeconds   = 30
        case    oneMinute       = 60
        case    twoMinutes      = 120
        case    fiveMinutes     = 300
        case    tenMinutes      = 600
        case    fifteenMinutes  = 900
        case    twentyMinutes   = 1200
        case    thirtyMinutes   = 1800
    }
    
    /** This is how many of the above choices we will give to the user. */
    static let s_availableTimeoutsInSeconds: [AutoDisconnectTimeouts] = [.immediately,
                                                                         .fiveSeconds,
                                                                         .tenSeconds,
                                                                         .twentySeconds,
                                                                         .thirtySeconds,
                                                                         .oneMinute,
                                                                         .twoMinutes,
                                                                         .fiveMinutes,
                                                                         .tenMinutes,
                                                                         .fifteenMinutes,
                                                                         .twentyMinutes,
                                                                         .thirtyMinutes
    ]
    
    // MARK: - Public Static View Controller Properties -
    /** This will hold our location manager. */
    static var v_locationManager: CLLocationManager!                  = nil
    static var v_lightButtonColor: UIColor                            = UIColor(colorLiteralRed: 0.5, green: 0.5, blue: 1, alpha: 1)
    static var v_darkButtonColor: UIColor                             = UIColor(colorLiteralRed: 0, green: 0, blue: 0.6, alpha: 1)
    static var v_defaultLocation: CLLocationCoordinate2D              = CLLocationCoordinate2D()
    
    // These properties are simply here to make it easy to access the main tabs.
    /** This is the tab bar controller (convenience property). */
    static var v_tabBarController: UITabBarController!                = nil
    /** The settings view controller. */
    static var v_settingsController: BMLTAdminSettingsViewController! = nil
    /** The Connection view controller. */
    static var v_connectController: BMLTAdminConnectViewController!   = nil
    /** The search view controller. */
    static var v_searchController: BMLTAdminSearchViewController!     = nil
    /** The map view controller. */
    static var v_deletedController: BMLTAdminDeletedViewController!   = nil
    /** The map view controller. */
    static var v_newMeetingController: BMLTAdminNewMeetingViewController!   = nil
    
    /** This will be true if we are connected. */
    static var v_connected: Bool                                      = false
    /** This is the base prefix for our Root Server (only filled when we are connected). */
    static var v_rootServerBaseURI: String                            = ""
    /** These are all of the various formats available on the server. */
    static var v_formats: [Int:[String:String]]!                      = nil
    /** This contains the actual Service body data. */
    static var v_serviceBodyData: [Int: [String:String]]!             = nil
    /** This will contain an array of JSON objects that contains the meeting search results. */
    static var v_searchResults: [Int:[String:String]]!                = nil
    /** This contains access to the app preferences. */
    static var v_appPrefs: BMLTAdminPrefs                             = BMLTAdminPrefs()
    /** This has the number of seconds we need to wait before logging out automatically. */
    static var v_autoDisconnectTimeoutInSeconds: Int                  = AutoDisconnectTimeouts.immediately.rawValue
    
    // MARK: - Public Standard Properties -
    
    /** The main application window object. Set by the application. */
    var window: UIWindow?
    
    /** This will be used to force a disconnect after the app has been in the background for a while. */
    var autoDisconnectTimer: Timer! = nil

    /** This will hold our session. */
    var connectionSession: BMLTAdminSession!                     = nil

    /** This will hold the time the app was put into the background (to ensure auto-disconnect). */
    var lastBackgroundTimestamp: Date!                          = nil
    
    // MARK: - Class Methods -
    /* ################################################################## */
    /**
     This cancels a session if SSL is required, but the session is not SSL.
     */
    class func validateSession() {
        if self.v_appPrefs.requireSSL && self.v_connected && !self.appDelegate().connectionSession.isSSL {
            self.clearLoggedInSession()
            self.displayErrorAlert("LOCAL-SETTINGS-NON-SSL-CONNECTION-TERMINATED-TITLE", inMessage: "LOCAL-SETTINGS-NON-SSL-CONNECTION-TERMINATED-MESSAGE")
        }
    }
    
    /* ################################################################## */
    /**
     Returns the time from the given string as an integer (number of minutes).
     The string can either be comma-separated 00:00 to 23:59, or a whole number between 0000 and 2359
     
     :param: inString The string to be parsed.
     
     :returns: an Int, 0 to 1439
     */
    static func parseTimeValueAsInteger(_ inString: String!) -> Int {
        var hours: Int = 0
        var minutes: Int = 0
        
        // First, check for colon-separated values (HH:MM).
        if let startTimeArray = inString?.components(separatedBy: ":").map({ Int($0) }) {
            if 1 < startTimeArray.count {
                if let hoursTemp = startTimeArray[0] {
                    if let minutesTemp = startTimeArray[1] {
                        hours = hoursTemp
                        minutes = minutesTemp
                    }
                }
            }
        } else {    // If not, see if we are representing the time as HHMM
            if let miltime = Int ( inString ) {
                hours = Int ( miltime / 100 )
                minutes = Int ( miltime - (hours * 100) )
            }
        }
        
        return min ( 1439, max ( 0, (hours * 60) + minutes ) )
    }
    
    /* ################################################################## */
    /**
        Returns the app delegate singleton.
    
        :returns: an instance of BMLTAdminAppDelegate (the main app delegate).
    */
    class func appDelegate() -> BMLTAdminAppDelegate! {
        return UIApplication.shared.delegate as! BMLTAdminAppDelegate
    }
    
    /* ################################################################## */
    /**
        "Cleans" a given URI
        
        :param: inURIAsAString This contains a string, with the URI.
    
        :returns: an implicitly unwrapped optional String. This is the given URI, "cleaned up."
                "http[s]://" may be prefixed.
    */
    class func cleanURI(_ inURIAsAString: String) -> String! {
        var ret: String! = inURIAsAString.URLEncodedString()
        
        // Very kludgy way of checking for an HTTPS URI.
        let wasHTTP: Bool = ret.lowercased().beginsWith("http://")
        let wasHTTPS: Bool = ret.lowercased().beginsWith("https://")
        
        // Yeah, this is pathetic, but it's quick, simple, and works a charm.
        ret = ret.replacingOccurrences(of: "^http[s]{0,1}://", with: "", options: NSString.CompareOptions.regularExpression)

        if wasHTTPS || (self.v_appPrefs.requireSSL && !wasHTTP && !wasHTTPS) {
            ret = "https://" + ret
        } else {
            ret = "http://" + ret
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
        Displays the given error in an alert with an "OK" button.
    
        :param: inTitle a string to be displayed as the title of the alert. It is localized by this method.
        :param: inMessage a string to be displayed as the message of the alert. It is localized by this method.
    */
    class func displayErrorAlert(_ inTitle: String, inMessage: String) {
        if(nil != self.v_tabBarController) {  // If we are testing, these will be nil, and we shouldn't display an error.
            if let targetViewController = self.v_tabBarController.selectedViewController {
                if nil != targetViewController.presentingViewController {
                    targetViewController.presentingViewController?.dismiss(animated: false, completion: nil)
                }
                
                let alertController = UIAlertController(title: NSLocalizedString(inTitle, comment: ""), message: NSLocalizedString(inMessage, comment: ""), preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-ERROR-OK-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
                
                alertController.addAction(okAction)

                targetViewController.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    /* ################################################################## */
    /**
     Displays the given strings in an alert with an "OK" button.
     
     :param: inTitle a string to be displayed as the title of the alert. It is localized by this method.
     :param: inMessage a string to be displayed as the message of the alert. It is localized by this method.
     :param: inCallback a callback ()->Void to be called upon completion.
     */
    class func displayAlert(_ inTitle: String, inMessage: String, inCallback: @escaping ()->Void ) {
        if(nil != self.v_tabBarController) {  // If we are testing, these will be nil, and we shouldn't display an error.
            if let targetViewController = self.v_tabBarController.selectedViewController {
                if nil != targetViewController.presentingViewController {
                    targetViewController.presentingViewController?.dismiss(animated: false, completion: nil)
                }
                
                let alertController = UIAlertController(title: NSLocalizedString(inTitle, comment: ""), message: NSLocalizedString(inMessage, comment: ""), preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-ERROR-OK-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
                
                alertController.addAction(okAction)
                
                targetViewController.present(alertController, animated: true, completion: inCallback)
            }
        }
    }
    
    /* ################################################################## */
    /**
        Tells the app delegate to change the UI to reflect the connection status.
    */
    class func setConnectionStatus() {
        if(nil != self.v_tabBarController) {  // If we are testing, these will be nil.
            // The tab items beyond settings are not enabled unless we have a logged-in connection.
            let isConnected:Bool = self.v_connected
            
            // If we don't have a logged-in connection, then we need to make sure that we are in the settings page.
            if(!isConnected || (0 == BMLTAdminConnectViewController.v_userServiceBodyIDs.count)) {
                self.v_connectController.navigationController!.popToRootViewController(animated: false)
                self.v_tabBarController.selectedIndex = ControllerIndex.connect.rawValue
            }
            
            self.v_searchController.navigationController!.tabBarItem.isEnabled = (isConnected && (0 < BMLTAdminConnectViewController.v_userServiceBodyIDs.count))

            // Make sure we reload the meeting data.
            if !self.v_searchController.navigationController!.tabBarItem.isEnabled {
                self.v_searchController.searchManager = nil
                self.v_searchController.needReload = true
                self.v_deletedController.appDisconnected = true
                self.v_searchController.filter = nil
            }
            
            self.v_deletedController.tabBarItem.isEnabled = self.v_searchController.navigationController!.tabBarItem.isEnabled
            self.v_newMeetingController.navigationController!.tabBarItem.isEnabled = self.v_searchController.navigationController!.tabBarItem.isEnabled
            
            self.v_tabBarController.tabBar.barTintColor = isConnected ? UITabBar.getLoggedInColor() : UIColor.black
            
            self.v_connectController.determineUIState()
        }
    }
    
    /* ################################################################## */
    /**
        This forces a logout.
    */
    class func clearLoggedInSession() {
        self.appDelegate().clearTimeout()
        // On the off chance we are not in the main thread, we use the dispatch handler.
        // We clean up every loose end we can think of.
        DispatchQueue.main.async(execute: {
            if nil != BMLTAdminAppDelegate.v_searchController.locationManager {
                BMLTAdminAppDelegate.v_searchController.locationManager.delegate = nil
            }
            self.appDelegate().connectionSession.disconnectSession()
            
            self.v_rootServerBaseURI = ""
            self.v_connected = false
            self.v_formats = nil
            self.v_serviceBodyData = nil
            
            BMLTAdminConnectViewController.v_userServiceBodyIDs = []
            
            self.v_connectController.weAreConnecting = false
            self.v_connectController.serverVersionAsInt = 0
            self.v_connectController.activeCommunicator = nil
            self.v_connectController.cleanPassword()
            self.v_connectController.navigationController!.popToRootViewController(animated: false)

            self.v_searchController.filter = nil
            self.v_searchController.searchManager = nil
            self.v_searchController.selectedMeeting = nil
            self.v_searchController.needReload = true
            self.v_searchController.navigationController!.popToRootViewController(animated: false)

            self.v_connectController.appDisconnected = true
            self.v_searchController.appDisconnected = true
            self.v_deletedController.appDisconnected = true
            
            self.v_connectController.processServerCredentials()
            
            self.setConnectionStatus()
            })
    }
    
    /* ################################################################## */
    /**
     */
    class func getServiceBodyObjectFromIndex(_ index: Int) -> [String:String]! {
        if (0 <= index) && (self.v_connectController.allUserServiceBodyIDs.count > index) {
            return self.getServiceBodyObjectFromId(self.v_connectController.allUserServiceBodyIDs[index])
        }
        
        return nil
    }
    
    /* ################################################################## */
    /**
     */
    class func getSelectedServiceBodyObjectFromIndex(_ index: Int) -> [String:String]! {
        if (0 <= index) && (BMLTAdminConnectViewController.v_userServiceBodyIDs.count > index) {
            return self.getServiceBodyObjectFromId(BMLTAdminConnectViewController.v_userServiceBodyIDs[index])
        }
        
        return nil
    }
    
    /* ################################################################## */
    /**
     */
    class func getServiceBodyIDFromIndex(_ index: Int) -> Int {
        if let sb = self.getServiceBodyObjectFromIndex(index) {
            if let ret = Int(sb["id"]!) {
                return ret
            }
        }
        
        return 0
    }
    
    /* ################################################################## */
    /**
     */
    class func getSelectedServiceBodyIDFromIndex(_ index: Int) -> Int {
        if let sb = self.getSelectedServiceBodyObjectFromIndex(index) {
            if let ret = Int(sb["id"]!) {
                return ret
            }
        }
        
        return 0
    }
    
    /* ################################################################## */
    /**
     */
    class func getServiceBodyIndexFromID(_ inID: Int) -> Int {
        var index = -1
        for sb in self.v_connectController.allUserServiceBodyIDs {
            index += 1
            if sb == inID {
                break
            }
        }
        
        return index
    }
    
    /* ################################################################## */
    /**
     */
    class func getSelectedServiceBodyIndexFromID(_ inID: Int) -> Int {
        var index = -1
        for sb in BMLTAdminConnectViewController.v_userServiceBodyIDs {
            index += 1
            if sb == inID {
                break
            }
        }
        
        return index
    }
    
    /* ################################################################## */
    /**
     */
    class func getServiceBodyObjectFromId(_ inID: Int) -> [String:String]! {
        var ret:[String:String]! = nil
        
        for sb in self.v_serviceBodyData {
            if sb.0 == inID {
                ret = sb.1
                break
            }
        }
        
        return ret
    }
    
    // MARK: - Instance Methods -
    
    /* ################################################################## */
    /**
        The destructor. We just make sure we clean up after ourselves.
    */
    deinit {
        type(of: self).clearLoggedInSession()
    }
    
    /* ################################################################## */
    /**
     */
    func setTimeout() {
        if nil == self.autoDisconnectTimer {    // We don't interfere if a timeout was already set.
            self.lastBackgroundTimestamp = Date() // We set the last timeout to now.
            
            if type(of: self).v_autoDisconnectTimeoutInSeconds != AutoDisconnectTimeouts.never.rawValue {
                self.autoDisconnectTimer = Timer.scheduledTimer(timeInterval: Double(type(of: self).v_autoDisconnectTimeoutInSeconds), target: self, selector: #selector(type(of: self).handleAutoTimeout(_:)), userInfo: nil, repeats: false)
            }
        }
    }
    
    /* ################################################################## */
    /**
     */
    func clearTimeout() {
        if nil != self.autoDisconnectTimer {
            self.autoDisconnectTimer.invalidate()
            self.autoDisconnectTimer = nil
            self.lastBackgroundTimestamp = nil
        }
    }
    
    // MARK: - UIApplicationDelegate Methods -
    
    /* ################################################################## */
    /**
        Called when the application has completed its initial startup, but before it opens the window.
    
        :param: application The application Object
        :param: launchOptions A dictionary of launch options.
    
        :returns: a Bool. False, if the application is to be terminated.
    */
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        var ret: Bool = false
        type(of: self).v_rootServerBaseURI = ""
        type(of: self).v_connected = false
        self.connectionSession = BMLTAdminSession()
        
        if ( nil != self.window ) {
            type(of: self).v_tabBarController = self.window!.rootViewController as! UITabBarController
            
            BMLTAdminActivityTracer_OSTrace(1)
            
            if ( nil != type(of: self).v_tabBarController ) {
                // Load all the static view controller convenience properties.
                type(of: self).v_tabBarController.delegate = self
                // We simply fetch the controllers from their positions in the viewControllers array.
                type(of: self).v_settingsController = type(of: self).v_tabBarController.viewControllers![ControllerIndex.settings.rawValue] as!BMLTAdminSettingsViewController
                // We simply fetch the controllers from their positions in the viewControllers array.
                type(of: self).v_connectController = (type(of: self).v_tabBarController.viewControllers![ControllerIndex.connect.rawValue] as? UINavigationController)!.topViewController as! BMLTAdminConnectViewController
                type(of: self).v_searchController = (type(of: self).v_tabBarController.viewControllers![ControllerIndex.search.rawValue] as? UINavigationController)!.topViewController as! BMLTAdminSearchViewController
                type(of: self).v_deletedController = type(of: self).v_tabBarController.viewControllers![ControllerIndex.deleted.rawValue] as! BMLTAdminDeletedViewController
                type(of: self).v_newMeetingController = (type(of: self).v_tabBarController.viewControllers![ControllerIndex.new.rawValue] as? UINavigationController)!.topViewController as! BMLTAdminNewMeetingViewController
                
                type(of: self).v_connectController.communicatorSource = self.connectionSession
                type(of: self).v_searchController.communicatorSource = self.connectionSession
                type(of: self).v_deletedController.communicatorSource = self.connectionSession
                type(of: self).v_newMeetingController.communicatorSource = self.connectionSession
                
                // Since the controllers may be navigation controllers, we access the root controllers to set the tab item names.
                let settingsRootController = type(of: self).v_tabBarController.viewControllers![ControllerIndex.settings.rawValue]
                let connectRootController = type(of: self).v_tabBarController.viewControllers![ControllerIndex.connect.rawValue]
                let searchRootController = type(of: self).v_tabBarController.viewControllers![ControllerIndex.search.rawValue]
                let deletedRootController = type(of: self).v_tabBarController.viewControllers![ControllerIndex.deleted.rawValue]
                let newRootController = type(of: self).v_tabBarController.viewControllers![ControllerIndex.new.rawValue]
                
                settingsRootController.tabBarItem.title = NSLocalizedString(settingsRootController.tabBarItem.title!, comment: "")
                connectRootController.tabBarItem.title = NSLocalizedString(connectRootController.tabBarItem.title!, comment: "")
                searchRootController.tabBarItem.title = NSLocalizedString(searchRootController.tabBarItem.title!, comment: "")
                deletedRootController.tabBarItem.title = NSLocalizedString(deletedRootController.tabBarItem.title!, comment: "")
                newRootController.tabBarItem.title = NSLocalizedString(newRootController.tabBarItem.title!, comment: "")
                
                type(of: self).v_autoDisconnectTimeoutInSeconds = type(of: self).v_appPrefs.timeoutInSeconds
                type(of: self).clearLoggedInSession()
                (type(of: self).v_connectController as BMLTAdminBaseViewController).runFirstSelectionTasks()

                ret = true
            }
        }
        return ret
    }
    
    /* ################################################################## */
    /**
     */
    @objc func handleAutoTimeout(_ inTimer: Timer) {
        type(of: self).clearLoggedInSession()
    }
    
    /* ################################################################## */
    /**
     */
    func applicationWillResignActive(_ application: UIApplication) {
        if self.connectionSession.isSessionConnected() {
            if type(of: self).v_autoDisconnectTimeoutInSeconds == AutoDisconnectTimeouts.immediately.rawValue {
                type(of: self).clearLoggedInSession()
                self.clearTimeout()
            } else {
                self.setTimeout()
            }
        }
    }
    
    /* ################################################################## */
    /**
     */
    func applicationDidEnterBackground(_ application: UIApplication) {
        if self.connectionSession.isSessionConnected() {
            if type(of: self).v_autoDisconnectTimeoutInSeconds == AutoDisconnectTimeouts.immediately.rawValue {
                type(of: self).clearLoggedInSession()
                self.clearTimeout()
            } else {
                self.setTimeout()
            }
        }
    }
    
    /* ################################################################## */
    /**
     When the app becomes active, we need to start the network monitor.
     */
    func applicationDidBecomeActive(_ application: UIApplication) {
        if (nil != self.lastBackgroundTimestamp) {
            if type(of: self).v_autoDisconnectTimeoutInSeconds == AutoDisconnectTimeouts.immediately.rawValue {
                type(of: self).clearLoggedInSession()
            } else {
                let now = Date()
                let secondsBetween: TimeInterval = now.timeIntervalSince(self.lastBackgroundTimestamp)
                let disconnectTimeout = TimeInterval (type(of: self).v_autoDisconnectTimeoutInSeconds)
                if secondsBetween >= disconnectTimeout {
                    type(of: self).clearLoggedInSession()
                }
            }
        }
        
        self.clearTimeout()
        
        if nil != self.connectionSession {
            type(of: self).v_connectController.activeCommunicator = nil
            type(of: self).setConnectionStatus()
        }
    }
    
    /* ################################################################## */
    /**
     When the app becomes active, we need to start the network monitor.
     */
    func applicationWillEnterForeground(_ application: UIApplication) {
        self.clearTimeout()
        type(of: self).v_connectController.activeCommunicator = nil
        type(of: self).setConnectionStatus()
    }
    
    /* ################################################################## */
    /**
        When the app is about to terminate. We clean up anything that needs it.
    */
    func applicationWillTerminate(_ application: UIApplication) {
        type(of: self).clearLoggedInSession()
    }
    
    // MARK: - UITabBarControllerDelegate Methods -
    /* ################################################################## */
    /**
     Responds to a new item being selected in the tab bar after the fact.
     
     :param: inTabBarController The tab bar controller that had an item selected.
     :param: viewController   The newly-selected view controller.
     
     */
    func tabBarController(_ inTabBarController: UITabBarController, didSelect viewController: UIViewController) {
        var newViewController: UIViewController = viewController
        BMLTAdminActivityTracer_OSTrace(2)
        
        if newViewController.isKind(of: UINavigationController.self) {
            newViewController = (viewController as! UINavigationController).viewControllers[0]
        }
        
        if newViewController.isKind(of: BMLTAdminBaseViewController.self) {
            (newViewController as! BMLTAdminBaseViewController).runFirstSelectionTasks()
        }
        
        // What we do here, is set the bar item to the tint specified for the controller's main view.
        inTabBarController.tabBar.tintColor = newViewController.view.tintColor
    }
}

