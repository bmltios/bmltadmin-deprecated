//
//  BMLTAdminBaseViewController.swift
//  BMLTAdmin
/**
    :license:
    Created by MAGSHARE.
    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BMLT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this code.  If not, see <http://www.gnu.org/licenses/>.
*/

import UIKit

/* ###################################################################################################################################### */
/**
    This is a base controller for our Tab Bar views.
*/
class BMLTAdminBaseViewController : UIViewController, BMLTAdminCommunicatorDataSinkProtocol {
    // MARK: - Instance Properties
    /** This is where communications will come from. */
    // We'll use the central App Delegate connection session as our default data source.
    var communicatorSource:BMLTAdminCommunicatorDataSourceProtocol! = nil
    
    var activeCommunicator: BMLTAdminCommunicator! = nil
    
    /** This will contain any meeting that is selected. */
    var selectedMeeting: BMLTAdminMeeting! = nil
    
    /** This is a semaphore that tells the controller to account for the app being previously disconnected. */
    var appDisconnected: Bool = true
    
    // MARK: - BMLTAdminCommunicatorDataSinkProtocol Methods -
    /* ################################################################## */
    /**
        This is the callback handler for the URL dispatches.
    
        This method should be overridden in derived classes.
        The base class method does nothing.
    
        :param: inHandler The handler for this call.
        :param: inJSONObject The data from the URL request. If nil, the call failed to produce. Check the handler's error data member.
        :param: inError Any errors that occurred
        :param: inRefCon The data/object passed in via the 'refCon' parameter in the initializer.
    */
    func responseData(_ inHandler: BMLTAdminCommunicator?, inResponseData: Data?, inError: Error?, inRefCon: AnyObject?) {
    }
    
    /* ################################################################## */
    /**
     This is where we set the tint color of the tab bar.
     
     :param: animated. True, if the transition will be animated.
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let controller = self.tabBarController {
            controller.tabBar.tintColor = self.view.tintColor
        }
    }
    
    /* ################################################################## */
    /**
     This is called when the controller is first set up.
     */
    func runFirstSelectionTasks() {
        if (nil == communicatorSource) || self.appDisconnected {
            _ = self.navigationController?.popToRootViewController(animated: false)
        }
    }
}
