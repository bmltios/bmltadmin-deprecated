//
//  BMLTAdminCommunicator.swift
//  BMLTAdmin
/**
    :license:
    Created by MAGSHARE.
    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BMLT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this code.  If not, see <http://www.gnu.org/licenses/>.
*/

import Foundation

// MARK: - Protocols -
/* ###################################################################################################################################### */
/**
    This is a protocol for a data source that defines a "send and call back" structure.
*/
protocol BMLTAdminCommunicatorDataSourceProtocol {
    /** If this is set to true, then errors are ignored. */
    var suppressErrors:Bool { get set }
    
    /* ################################################################## */
    /**
        Calls a URL
        
        :param: inCommunicator The communicator instance calling this.
        :param: inURIAsAString This contains a string, with the URI.
        :param: inCompletionBlock This is the completion block supplied by the caller. It is to be called upon receipt of data.
        :param: inIsTest If true, then we don't report task errors (the call is expected to fail).
    
        :returns: a Bool. True, if the call was successfully initiated.
    */
    func callURI(_ inCommunicator: BMLTAdminCommunicator, inURIAsAString: String!, inCompletionBlock: BMLTAdminCommunicator.requestCompletionBlock!) -> Bool
}

/* ###################################################################################################################################### */
/**
    This defines a protocol for a communicator delegate, which receives the responses.
*/
protocol BMLTAdminCommunicatorDataSinkProtocol {
    /* ################################################################## */
    /**
        The response callback.
    
        :param: inHandler The handler for this call.
        :param: inResponseData The data from the URL request. If nil, the call failed to produce. Check the handler's error data member.
        :param: inError Any errors that occurred
        :param: inRefCon The data/object passed in via the 'refCon' parameter in the initializer.
    */
    func responseData(_ inHandler: BMLTAdminCommunicator?, inResponseData: Data?, inError: Error?, inRefCon: AnyObject?)
}

// MARK: - Classes -
/* ###################################################################################################################################### */
/**
 This manages an interactive session connection with the server.
 
 For whatever reason, Cocoa/Swift won't let me derive from NSURLSession, so this class aggregates it, as opposed to extends it
 (not a big deal, probably better design in the long run anyway).
 
 The semantic administration requires a constant session. Authentication is done once at the start of the session, then the
 server maintains the authentication for the duration of the session. For this reason, we need to maintain a consistent session
 throughout our administration duties. This class is instantiated once by the App Delegate, and holds the session open.
 
 This class maintains a dictionary of completion blocks that it uses to forward server responses to the view controllers.
 View controllers need to define a completion block/callback, in the requestCompletionBlock format. When they call a URL with
 this class, they provide this completion block. When the class instance gets the response, it forwards the data as an NSData
 instance to the completion block.
 
 Errors are handled by the App Delegate instance.
 */
class BMLTAdminSession : NSObject, URLSessionDataDelegate, BMLTAdminCommunicatorDataSourceProtocol {
    // MARK: - Enums and Macros -
    
    /**
     These are used to denote a cache size for the URL handler.
     
     These are the default values provided in the Apple documentation.
     */
    enum CacheSize: Int {
        /** The amount of RAM to reserve for our URL Session cache (in bytes). */
        case memory = 16384
        /** The amount of disk space to reserve for our cache spool (in bytes). */
        case disk = 268435456
    }

    // MARK: - Instance Properties -
    
    /** This will be the URLSession that we manage. */
    var mySession: Foundation.URLSession!    = nil
    /** This will be set to true if the URL call is expected to fail (suppresses error report). */
    var suppressErrors: Bool        = false
    /** This is true if this is an SSL session */
    var isSSL: Bool = false
    /**
     This is a dictionary of callbacks for open requests. It keys on the URL called.
     
     I hate pulling crap like this, as it's clumsy and thread-unfriendly. Unfortunately,
     there doesn't seem to be much choice, as there's no way to attach a refCon to a URL task.
     */
    var urlExtraData: Dictionary<String,BMLTAdminCommunicator.requestCompletionBlock?> = Dictionary<String,BMLTAdminCommunicator.requestCompletionBlock!>()
    
    // MARK: - Instance Methods -
    
    /* ################################################################## */
    /**
     Make sure we clean up after ourselves.
     */
    deinit {
        self.disconnectSession()
    }
    
    /* ################################################################## */
    /**
     Pretty much exactly what it says on the tin.
     */
    func disconnectSession() {
        if(self.isSessionConnected()) {
            let session = self.mySession
            self.isSSL = false
            session?.reset(completionHandler: self.resetCompletion)
            self.mySession = nil
        }
    }
    
    /* ################################################################## */
    /**
     Pretty much exactly what it says on the tin.
     */
    @objc func resetCompletion() {
    }
    
    /* ################################################################## */
    /**
     This is called when a URL task is complete.
     
     :param: inData The data returned from the connection.
     :param: inResponse The response object.
     :param: inError Any error that occurred.
     */
    func taskCompletion (_ inData: Data?, inResponse: URLResponse?, inError: Error?) -> Void {
        if ((nil != inError) && !self.suppressErrors) {   // We don't display the error if we have been asked not to.
            DispatchQueue.main.async(execute: {
                BMLTAdminAppDelegate.clearLoggedInSession()
                BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-CONN-FAIL-TITLE", inMessage: "LOCAL-ERROR-CONN-FAILURE-MESSAGE")
            })
        }
        
        // See if we need to call the completion handler.
        if let url = inResponse?.url {
            // We don't do squat if there is no callback in our table.
            let urlLookup = url.absoluteString
            if(nil != self.urlExtraData.index(forKey: urlLookup)) {
                // Fetch the stored comnpletion block from our dictionary.
                if let callback: BMLTAdminCommunicator.requestCompletionBlock = self.urlExtraData[(url.absoluteString)]! {
                    self.urlExtraData.removeValue(forKey: (url.absoluteString)) // Remove the callback from the dictionary.
                    callback(inData, inError)
                }
            }
        } else {    // If there was no specific callback URL, we cycle through the list, and send responses to everyone we know about.
            let keys = self.urlExtraData.keys
            for key in keys {
                if let callback: BMLTAdminCommunicator.requestCompletionBlock = self.urlExtraData[key]! {
                    self.urlExtraData.removeValue(forKey: key) // Remove the callback from the dictionary.
                    callback(inData, inError)
                }
            }
        }
    }
    
    /* ################################################################## */
    /**
     Declares the connection status.
     
     :returns: a Bool, true if the session is currently connected.
     */
    func isSessionConnected() -> Bool {
        return (nil != self.mySession)
    }
    
    // MARK: - NSURLSessionDataDelegate Methods -
    
    /* ################################################################## */
    /**
     Called when a session throws a nutty.
     
     :param: session The NSURLSession that controls this task.
     :param: error The error returned.
     */
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        if ((nil != error) && !self.suppressErrors) {   // We don't display the error if we have been asked not to.
            DispatchQueue.main.async(execute: {
                BMLTAdminAppDelegate.clearLoggedInSession()
                BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-CONN-FAIL-TITLE", inMessage: "LOCAL-ERROR-CONN-FAILURE-MESSAGE")
            })
        }
    }
    
    // MARK: - BMLTAdminCommunicatorDataSourceProtocol Methods -
    
    /* ################################################################## */
    /**
     Calls a URL
     
     :param: inCommunicator The communicator instance calling this.
     :param: inURIAsAString This contains a string, with the URI.
     :param: inCompletionBlock This is the completion block supplied by the caller. It is to be called upon receipt of data.
     :param: inIsTest If true, then we don't report task errors (the call is expected to fail).
     
     :returns: a Bool. True, if the call was successfully initiated.
     */
    func callURI(_ inCommunicator: BMLTAdminCommunicator, inURIAsAString: String!, inCompletionBlock: BMLTAdminCommunicator.requestCompletionBlock!) -> Bool {
        // If we did not already have a session established, we set one up.
        if(!self.isSessionConnected()) {
            BMLTAdminActivityTracer_OSTrace(1)
            self.isSSL = false
            let config: URLSessionConfiguration = URLSessionConfiguration.default
            let myPathList  = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let myPath: URL = URL ( fileURLWithPath: myPathList[0] )
            let bundleIdentifier = Bundle.main.bundleIdentifier
            var fullCachePath: URL = myPath.appendingPathComponent(bundleIdentifier!)
            fullCachePath = fullCachePath.appendingPathComponent("/CacheDirectory")
            config.urlCache = URLCache(memoryCapacity: CacheSize.memory.rawValue, diskCapacity: CacheSize.disk.rawValue, diskPath: fullCachePath.absoluteString);
            config.requestCachePolicy = NSURLRequest.CachePolicy.useProtocolCachePolicy
            self.mySession = Foundation.URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        }
        
        var ret: Bool = false
        
        // This is because it is possible to crash the app by inserting a less-than character into a URI.
        let uriString = inURIAsAString.replacingOccurrences(of: "<", with: "%3C")

        let url: URL! = URL(string: uriString)
        
        // We can't have the URL already in play. That would be bad.
        if(nil == self.urlExtraData[inURIAsAString]) {
            // Assuming we have a completion block and a URI, we will actually try to get a version from the server (determine its validity).
            if((nil != inCompletionBlock) && (nil != inURIAsAString) && self.isSessionConnected()) {
                self.urlExtraData.updateValue(inCompletionBlock, forKey: url.absoluteString)    // Store the completion block for recall later.
                let dataTask: URLSessionTask = self.mySession.dataTask(with: url, completionHandler: self.taskCompletion)
                dataTask.resume()   // Throw the switch, Igor!
                ret = true
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     */
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        BMLTAdminActivityTracer_OSTrace(4)
        let protectionSpace = challenge.protectionSpace
        if protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            self.isSSL = true
            completionHandler(.useCredential, URLCredential(trust: protectionSpace.serverTrust!))
        } else if protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPBasic {
            if challenge.previousFailureCount > 0 {
                completionHandler(Foundation.URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
            } else {
                let credential = URLCredential(user:"username", password:"password", persistence: .forSession)
                completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential,credential)
            }
        }
    }
    
    /* ################################################################## */
    /**
     */
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        BMLTAdminActivityTracer_OSTrace(5)
        if challenge.previousFailureCount > 0 {
            print("Alert Please check the credential")
            completionHandler(Foundation.URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
        } else {
            let credential = URLCredential(user:"username", password:"password", persistence: .forSession)
            completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential,credential)
        }
    }
}

/* ###################################################################################################################################### */
/**
    The main reason for the existence of this class is to make the communications testable. Its structure allows the tester to inject
    data sources and sinks.
    
    Data sources and sinks are adornments. This allows us to substitute mocks when testing.

    This is a lightweight, short-lifetime class. It is meant to be instantiated and destroyed on an "as needed" basis.
*/

class BMLTAdminCommunicator {
    // MARK: - Type Definitions -
    /* ################################################################## */
    /**
        This is the definition for the testRootServerURI completion block.

        The routine is called upon completion of a URL connection. When the
        connection ends (either successfully or not), this routine is called.
        If it is successful, then the inData parameter will be non-nil.
        If it failed, then the parameter will be nil, and the inError
        parameter may have a value.

        :param: inData  The Data returned.
        :param: inError Any error that ocurred. Nil, if no error.
    */
    typealias requestCompletionBlock = (_ inData: Data?, _ inError: Error?)->Void
    
    // MARK: - Fixed Data Members -
    /** This is the data source for the call. */
    let dataSource:BMLTAdminCommunicatorDataSourceProtocol!
    /** This is the completion handler to be called when the URL is called. */
    let delegate:BMLTAdminCommunicatorDataSinkProtocol!
    /** This is the completion handler to be called when the URL is called. */
    let uriAsAString:String!
    /** This is any extra data the caller may want passed to the callback. */
    let refCon:AnyObject?
    
    // MARK: - Dynamic Data Members -
    /** This contains any error from the call. */
    var error:Error!
    /** This contains the data response (if any). */
    var data:Data!
    
    // MARK: - Instance Methods -
    /* ################################################################## */
    /**
        This is the designated initializer for this class. You have to give
        a URL, as well as a delegate (sink) and data source.
        This class executes the connection upon initialization.
    
        :param: inURI The URL to be called, as a string.
        :param: dataSource The data source.
        :param: delegate The delegate object (the sink) to be notified when new data arrives.
        :param: refCon This is any object or data that you want to send to the delegate receive method. Default is nil.
        :param: executeImmediately If true (the default), the instance executes immediately.
                The reason for this flag, as opposed to simply overriding the "execute()" method,
                is so we have a bit more flexibility. We don't need to rewrite that method if we
                don't want to. Since the parameter has a default, it's not an issue to ignore it.
    */
    init(_ inURI:String, dataSource inDataSource:BMLTAdminCommunicatorDataSourceProtocol, delegate inDelegate:BMLTAdminCommunicatorDataSinkProtocol, refCon inRefCon: AnyObject? = nil, executeImmediately inExecute: Bool = true) {
        self.uriAsAString = inURI
        self.dataSource = inDataSource
        self.delegate = inDelegate
        self.refCon = inRefCon
        self.data = nil
        self.error = nil
        
        if(inExecute) {
            self.execute()
        }
    }
    
    /* ################################################################## */
    /**
        This actually executes the connection.
    */
    func execute() {
        BMLTAdminActivityTracer_OSTrace(6)
        _ = self.dataSource.callURI(self, inURIAsAString: self.uriAsAString, inCompletionBlock: self.handleResponseFromHandler)
    }
    
    /* ################################################################## */
    /**
        This is the callback for the URL request.
    
        :param: inData the data from the URL request.
        :param: inError Any error that ocurred. Nil, if no error.
    */
    func handleResponseFromHandler(_ inData: Data?, inError: Error?) {
        BMLTAdminActivityTracer_OSTrace(7)
        self.data = inData
        self.error = inError
        if(nil != self.delegate) {
            self.delegate.responseData(self, inResponseData: self.data, inError: self.error, inRefCon: self.refCon)
        }
    }
}
