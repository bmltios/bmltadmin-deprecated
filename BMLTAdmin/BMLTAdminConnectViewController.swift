//
//  BMLTAdminConnectViewController.swift
//  BMLTAdmin
/**
    :license:
    Created by MAGSHARE.
    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BMLT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this code.  If not, see <http://www.gnu.org/licenses/>.
*/

import UIKit
import LocalAuthentication
import Security
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


// MARK: - Classes -
/* ###################################################################################################################################### */
/**
    This class will handle the storage and retrieval, as well as display and interaction, of the app's user login data.
    It will use TouchID, if the device supports it.
    It uses the simple NSUserDefaults functionality to store the basic simple parameters, including a simple list of up
    to 10 cached URLs for Root Servers.

    The password is only stored in the keychain as an encrypted value, although the login ID is just kept in the NSUserDefaults
    data.

    The way that a connection works, is that we:
       1.  We start by only enabling the URL entry text entry. The "Search" and "Edit" tabs are disabled when disconnected.
       2.  As the user types into this, we check the server regularly by trying to download the GetServerInfo response.
       3.  If it is valid, we enable the login ID and the password text entry fields.
       4.  As the user enters a login ID, we check the combination of the URL and the login ID against our stored logins, to see if we have a record.
       5.  If so, we display a button that allows the user to "forget" the stored login.
       6.  If the device supports TouchID, we display a TouchID button upon entry of a valid stored login for the given URL.
       7.  The user can log in directly using TouchID. Otherwise, they need to manually enter the password.
       8.  Once some text is in the password field, we enable the connect button. We do not check the validity of the password (security measure).
       9.  Once the user has successfully logged in, we disable the three text entries, hide the forget button, and change the connect button title.
       10. We also, at this point, read in the formats from the server, the user permissions, and the Service bodies.
       11. If the user does not have any meeting edit permissions with this login, we log the user out.
       12. If the above all passes, and the user is allowed to edit more than one Service body, we display a button that allows them to select them. In the case of a new URL/login combo, we immediately go to the Service body selector screen. If the URL has been executed before (is saved), we go directly to the search tab.

    A successful connection also enables the "Search" and "Edit" tab bar items.
    All communications are via JSON. We use the clunky "built in" way of parsing JSON, because third-party Swift libraries make updating to the next
    version of Swift a big problem (Yes, I tried SwiftyJSON, and it blew up when I tried updating to Swift 2.0).
*/
class BMLTAdminConnectViewController: BMLTAdminBaseViewController, UITextFieldDelegate {
    // MARK: - Types -
    
    /** This enum contains values that indicate what the sent URL is doing. These are returned in the "refCon" field. */
    enum BMLTAdminConnectViewControllerCallType: String {
        /** This is if the call is a test probe. */
        case TestProbe  = "test"
        /** This means we are trying to log in. */
        case Login      = "login"
        /** This means we are getting our Service body IDs. */
        case GetIDs     = "getIDs"
        /** This means we are getting our available formats. */
        case GetFormats = "getFormats"
        /** This means we are getting our Service body info. */
        case GetServiceBodies = "getServiceBodies"
    }
    
    // MARK: - Static Variables -
    /** This is the current login URI */
    static var v_loginURI: String               =   ""
    /** This is the current login ID */
    static var v_loginID: String                =   ""
    /** This will contain all the Service Body IDs this user has chosen to edit. */
    static var v_userServiceBodyIDs: [Int]!     =   []
    
    // MARK: - Instance Constant Properties -
    /** This will be the key for the persistent array of recent URIs. */
    let s_keychainID: String              =    "BMLTAdmin.Key"
    /** This is the height of the gutter above and below the TouchID button. */
    let s_heightWithButtonsGutter:CGFloat =    13
    /** This is the minimum server version we'll support. */
    let s_minServerVersion:Int            =    2008000
    /** This is the meinimum number of entries in a valid JSON response to "Switcher=GetServerInfo" */
    let s_minServerInfoLen:Int            =    15
    /** This is the suffix appended to the Root Server URL for a test. */
    let s_testSuffix                      =    "/client_interface/json/?switcher=GetServerInfo"
    /** This is the suffix appended to the Root Server URL for the formats. */
    let s_formatsSuffix                   =    "/client_interface/json/?switcher=GetFormats"
    /** This is the suffix appended to the Root Server URL for the formats. */
    let s_serviceBodiesSuffix             =     "/client_interface/json/?switcher=GetServiceBodies"
    /** This is the suffix appended to the Root Server URL for a standard admin command. */
    let s_adminAdminQuerySuffix           =     "/local_server/server_admin/json.php?admin_action="
    /** This is the Service Body Editor Permission Value    */
    let s_isServiceBodyAdmin              =     3
    /** This is the Meeting Editor Permission Value    */
    let s_isMeetingAdmin                  =     2
    /** This is the Server Admin ID    */
    let s_serverAdminID                   =     1
    /** This is a keychain simplifier. */
    let keychainWrapper:FXKeychain!       =    FXKeychain.default()
    
    // MARK: - Variable Instance Properties -
    /** This will hold the server version (as an integer). */
    var serverVersionAsInt: Int           = 0
    /** This is a semaphore that is set to indicate that we are connecting (prevents the button title from being changed). */
    var weAreConnecting: Bool             = false
    /** This flag is true if we support TouchID. */
    var supportsTouchID:Bool              = false
    /** This contains all Service bodies available to the user (not just the selected ones). */
    var allUserServiceBodyIDs: [Int]!     = []
    /** This is set to true to indicate that the select Service Bodies screen should automatically open. */
    var showSBAuto: Bool                  = true
    /** This is used to see if the URL has changed. */
    var uriChanged: Bool                  =     false
    /** This is used to see if the login ID has changed. */
    var loginChanged: Bool                =     false
    /** This contains the localization key for why the URL is wrong. It should be nil if everything is OK. */
    var badURLReason: String!             =     nil
    
    // MARK: - IBObject Properties -
    /** This contains all of the various labels and text items for the URL and login. */
    @IBOutlet weak var loginItemsContainer: UIView!
    /** The label for the URL text item. */
    @IBOutlet weak var uriEntryLabel: UILabel!
    /** The URL entry text item itself. */
    @IBOutlet weak var uriEntryTextField: UITextField!
    /** The label for the Login ID text item. */
    @IBOutlet weak var loginIDEntryLabel: UILabel!
    /** This is displayed while the URI is being checked. */
    @IBOutlet weak var uriActivityView: UIActivityIndicatorView!
    /** This is the view that aggregates just the login items. */
    @IBOutlet weak var loginGroupContainer: UIView!
    /** The Login ID text item. */
    @IBOutlet weak var loginIDEntryTextField: UITextField!
    /** The label for the Login Password text item. */
    @IBOutlet weak var loginPasswordEntryLabel: UILabel!
    /** The Login Password text item. */
    @IBOutlet weak var loginPasswordEntryTextField: UITextField!
    /** THis is the "thumbprint" TouchID button that will appear when TouchID is available. */
    @IBOutlet weak var touchIDButton: UIButton!
    /** The Connect/Disconnect button. */
    @IBOutlet weak var connectButton: UIButton!
    /** This is the "Forget This URL" button. */
    @IBOutlet weak var deleteSingleURIButton: UIButton!
    /** This is an activity indicator for the connection. */
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    /** This is the button that will bring in the Service Body Selection. */
    @IBOutlet weak var getServiceBodySelectorButton: UIButton!
    /** This button is displayed when the URL is not pointing to a valid Root Server. It will bring up an alert, stating the reason. */
    @IBOutlet weak var whyCantIConnectButton: UIButton!
    
    // MARK: - Class Methods -
    /* ################################################################## */
    /**
     This is a read-only accessor to the static Service Body Selection array.
     
     :returns: an array of Int. The BMLT IDs of the selected Service bodies.
     */
    class func getSelectedServiceBodyIDs() -> [Int] {
        if let userServiceBodyIDs = self.fetchStoredServiceBodyIDs(self.v_loginURI, inLoginID: self.v_loginID) {
            if 0 < userServiceBodyIDs.count && (nil != BMLTAdminAppDelegate.v_serviceBodyData) {
                var sbIDs: [Int] = []
                for sb in userServiceBodyIDs {
                    if let _ = BMLTAdminAppDelegate.v_serviceBodyData[sb] {
                        sbIDs.append(sb)
                    }
                }
                self.v_userServiceBodyIDs = sbIDs
            }
        }
        
        return self.v_userServiceBodyIDs
    }
    
    /* ################################################################## */
    /**
     This is an accessor to the static Service Body Selection array that allows it to be set.
     
     :param: inSelectedServiceBodies an array of Int. The BMLT IDs of the selected Service bodies.
     */
    class func setSelectedServiceBodyIDs(_ inSelectedServiceBodies: [Int]) {
        self.v_userServiceBodyIDs = inSelectedServiceBodies
    }
    
    /* ################################################################## */
    /**
     This looks up the requested Service body, and sees if it on the guest list.
     
     :param: inServiceBodyID The BMLT ID of the Service body being checked
     
     :returns: a Bool. True, if the Service body is selected for editing.
     */
    class func isServiceBodyValid(_ inServiceBodyID:Int) -> Bool {
        var ret: Bool = false
        
        for sbID in self.getSelectedServiceBodyIDs() {
            if sbID == inServiceBodyID {
                ret = true
                break
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This stores the Service body IDs for the given login/URI combo.
     It is a class function, in order to make it easier for other classes to access the data.
     
     :param: inURI              The URI component of the login.
     :param: inLoginID          The login ID of the login.
     :param: inServiceBodyIDs   An optional array of Int, containing the BMLT IDs of selected/authorized Service bodies for the login. If nil, then the stored value will be deleted (if it is there).
     
     */
    class func storeServiceBodyIDs(_ inURL: String, inLoginID: String, inServiceBodyIDs: [Int]!) {
        _ = BMLTAdminAppDelegate.v_appPrefs.setServiceBodies(inURL + "-" + inLoginID, inValue: inServiceBodyIDs)
    }

    /* ################################################################## */
    /**
     This class function fetches stored service bodies for the login from our defaults.
     If there are none, then it fetches all the Service bodies the login is authorized for.
     It is a class function, in order to make it easier for other classes to access the data.
     
     :param: inURI The "cleaned" URI.
     :param: inLoginID   The login ID
     
     :returns: an optional array of Int, with the BMLT IDs of the Service boies managed by this login.
     */
    class func fetchStoredServiceBodyIDs(_ inURL: String, inLoginID: String) -> [Int]! {
        let ret: [Int]! = BMLTAdminAppDelegate.v_appPrefs.getServiceBodies(inURL + "-" + inLoginID)
        
        return ret
    }
    
    // MARK: - Instance Methods -
    /* ################################################################## */
    /**
     Tells the activity indicator to disappear.
     */
    func hideURIActivity() {
        self.uriActivityView.isHidden = true
    }
    
    /* ################################################################## */
    /**
     Tells the activity indicator to display.
     */
    func displayURIActivity() {
        self.uriActivityView.isHidden = false
    }
    
    /* ################################################################## */
    /**
        This tells us whether or not to show the "Forget Stored Login" button.
        
        :returns: a Bool, true, if the Forget button should be hidden.
    */
    func hideForgetButton() -> Bool {
        var ret:Bool = true
        
        if !BMLTAdminAppDelegate.v_connected {  // We don't display it if we are connected.
            let cleanedURI = BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!)
            
            if !(cleanedURI?.isEmpty)! {
                let loginText = self.loginIDEntryTextField.text
                
                let fetchedArray = BMLTAdminAppDelegate.v_appPrefs.getLoginsFromPref(cleanedURI)
                
                for loginTuple in fetchedArray {
                    if loginTuple.loginID == loginText {
                        ret = false
                    }
                }
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**tp
     This tells us whether or not to show the TouchID button.
     
     :returns: a Bool, true, if the TouchID button should be hidden.
     */
    func hideTouchID() -> Bool {
        return  !self.supportsTouchID ||     // We don't display if TouchID is not supported by the device.
            self.loginIDEntryTextField.text!.isEmpty || // There also needs to be a login ID
            (0 == self.serverVersionAsInt) ||   // Have to have a valid server.o
            self.hideForgetButton()     // The rest of the rules are covered by the "forget me" button.
    }
    
    /* ################################################################## */
    /**tp
     This tells us whether or not to show the "Why Can't I Connect?" button.
     
     :returns: a Bool, true, if the "Why Can't I Connect?" button should be hidden.
     */
    func hideBadURIButton() -> Bool {
        return (nil == self.badURLReason) || self.badURLReason.isEmpty
    }
    
    // MARK: - Stored Data Handler Methods
    /* ################################################################## */
    /**
     Store the given URL and login info in the user defaults and the keychain.
     
     :param: inURL The Root Server URL. This will be the dictionary key.
     :param: inLoginID The User login. This will be stored unencrypted.
     :param: inPassword The password. This will be stored in the keychain.
     
     :returns: A Bool. True, if the login is new.
     
     We will store our logins and URIs in the standard user defaults
     store. The passwords will be kept in the keychain.
     The passwords will be looked up via a key of (URI + login),
     as we may have multiple logins for the same URI.
     */
    func storeLogin(_ inURL: String, inLoginID: String, inPassword: String, inServiceBodyList: [Int]!) -> Bool {
        var ret: Bool = BMLTAdminAppDelegate.v_appPrefs.setLoginPref(inURL, inLogin: inLoginID, inDeleteLogin: false)
        
        self.keychainWrapper.setObject(inPassword, forKey: inURL + "-" + inLoginID)
        
        if ret && (nil != inServiceBodyList) {
            ret = BMLTAdminAppDelegate.v_appPrefs.setServiceBodies(inURL + "-" + inLoginID, inValue: inServiceBodyList)
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     Used to delete a login from the stored logins.
     
     :param: inURI The URI
     
     :param: inLogin The login ID
     */
    func deleteLogin (_ inURI: String, inLoginID: String) {
        _ = BMLTAdminAppDelegate.v_appPrefs.setLoginPref(inURI, inLogin: inLoginID, inDeleteLogin: true)
        _ = BMLTAdminAppDelegate.v_appPrefs.setServiceBodies(inURI + "-" + inLoginID, inValue: nil)
        self.keychainWrapper.removeObject(forKey: inURI + "-" + inLoginID)
    }
    
    // MARK: - UI Setup Methods
    /* ################################################################## */
    /**
     Called to set the various enables and whatnot.
     
     :discussion: We test the server via a quick XML server info call. If the
     test returns valid XML data, we test that to see if it's a valid version.
     if so, we then test to see if there is text in both the login ID and
     password fields.
     We enable various items, based on these tests.
     */
    func determineUIState() {
        if      (nil != self.connectButton)
            &&  (nil != self.deleteSingleURIButton) { // This applies for all the elements, but we only need to check one. We don't call this before the system is ready.
            // Just being completely anal. This shouldn't be called when these are nil, but just in case...
            if      (nil != self.connectButton)
                &&  (nil != self.touchIDButton)
                &&  (nil != self.deleteSingleURIButton)
                &&  (nil != self.activityIndicator)
                &&  (nil != self.getServiceBodySelectorButton)
                &&  (nil != self.uriEntryTextField) {
                DispatchQueue.main.async(execute: {
                    self.connectButton.isEnabled = false
                    
                    // These are hidden by default.
                    self.activityIndicator.isHidden = true
                    self.getServiceBodySelectorButton.isHidden = true
                    
                    // If the URI is empty, then this is just a plain old bad URI.
                    if self.uriEntryTextField.text!.isEmpty {
                        self.disableLoginEntry()
                        self.disablePasswordEntry()
                        self.loginPasswordEntryTextField.text = ""
                        self.loginIDEntryTextField.text = ""
                        self.badURLReason = "LOCAL-CONNECT-REASON-BAD-URL"
                    } else {
                        let cleanedURI = BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!)
                        
                        // We see if we require SSL. If so, we are a lot more stringent, hereafter.
                        let failedSSLTest = !(cleanedURI?.beginsWith("https://"))! && BMLTAdminAppDelegate.v_appPrefs.requireSSL
                        
                        // See if we have a previously stored login at this URI.
                        var fetchedArray = BMLTAdminAppDelegate.v_appPrefs.getLoginsFromPref(cleanedURI)
                        
                        if 0 < self.serverVersionAsInt {    // Make sure we have a valid version (it will be 0, otherwise).
                            if BMLTAdminAppDelegate.v_connected {
                                if (nil != self.allUserServiceBodyIDs) {
                                    let numIDs = self.allUserServiceBodyIDs.count
                                    let isHidden = (2 > numIDs)
                                    self.getServiceBodySelectorButton.isHidden = isHidden
                                }
                                
                                // If we have a successful connection, then we can't change what is in the text fields. We "disable" them by dimming the container, and eschewing user interaction for the container.
                                self.disableURIEntry()
                                self.disableLoginEntry()
                                self.disablePasswordEntry()
                                self.connectButton.isEnabled = true   // The disconnect button is enabled, however.
                            } else {
                                // If we have a valid server, then we enable the login ID and password items.
                                if !failedSSLTest {
                                    self.enableLoginEntry()
                                    
                                    if self.uriChanged {
                                        self.loginPasswordEntryTextField.text = ""
                                        self.loginIDEntryTextField.text = ""
                                        
                                        // See if we had stored a previous successful URI.
                                        let lastSuccessfulLogin = BMLTAdminAppDelegate.v_appPrefs.getLastSuccessfulLoginFromPref()
                                        
                                        // If we have a single login for this URI, we automatically insert it into the login ID field.
                                        if !fetchedArray.isEmpty {
                                            if (1 == fetchedArray.count) &&  ("" != fetchedArray[0].loginID) {
                                                self.loginIDEntryTextField.text = fetchedArray[0].loginID
                                            } else {
                                                // We will also enter the last successful login ID, if the URI matches.
                                                if (nil != lastSuccessfulLogin) && (lastSuccessfulLogin?.url == cleanedURI) {
                                                    self.loginIDEntryTextField.text = lastSuccessfulLogin?.loginID
                                                }
                                            }
                                        }
                                    } else {
                                        if self.loginIDEntryTextField.text!.isEmpty {
                                            self.disablePasswordEntry()
                                            self.loginPasswordEntryTextField.text = ""
                                        }
                                    }
                                } else {
                                    self.disableLoginEntry()
                                    self.disablePasswordEntry()
                                    self.loginPasswordEntryTextField.text = ""
                                    self.loginIDEntryTextField.text = ""
                                }
                                
                                if !self.loginIDEntryTextField.text!.isEmpty {
                                    self.enablePasswordEntry()
                                }
                                
                                self.connectButton.isEnabled = !self.loginIDEntryTextField.text!.isEmpty && !self.loginPasswordEntryTextField.text!.isEmpty
                            }
                        } else {  // Otherwise, we disable everything except the URI field.
                            self.enableURIEntry()
                            self.disableLoginEntry()
                            self.disablePasswordEntry()
                            self.loginPasswordEntryTextField.text = ""
                            if !BMLTAdminAppDelegate.v_connected {
                                self.loginIDEntryTextField.text = ""
                            }
                        }
                        
                        if(!self.weAreConnecting) { // We don't change if we are in the process of connecting or disconnecting.
                            self.connectButton.isHidden = false
                            if(BMLTAdminAppDelegate.v_connected) {  // If we are connected, we will display "DISCONNECT" in the button title.
                                self.connectButton.setTitle(NSLocalizedString("LOCAL-CONNECT-DISCONNECT", comment: ""), for: UIControlState())
                            }
                            else {
                                self.connectButton.setTitle(NSLocalizedString("LOCAL-CONNECT-CONNECT", comment: ""), for: UIControlState())
                            }
                        }
                        else {
                            self.activityIndicator.isHidden = false
                            self.connectButton.isHidden = true
                        }
                    }
                })
            }
            
            DispatchQueue.main.async(execute: {
                if self.loginIDEntryTextField.text!.isEmpty || BMLTAdminAppDelegate.v_connected {
                    self.loginPasswordEntryLabel.isHidden = true
                    self.loginPasswordEntryTextField.isHidden = true
                    self.loginPasswordEntryTextField.text = ""
                }
                
                self.deleteSingleURIButton.isHidden = self.hideForgetButton()
                self.touchIDButton.isHidden = self.hideTouchID()
                self.whyCantIConnectButton.isHidden = self.loginIDEntryTextField.isEnabled || self.weAreConnecting || BMLTAdminAppDelegate.v_connected
            })
        }
    }
    
    /* ################################################################## */
    /**
     This will choose whether or not to enable the URI entry text field and label.
     
     :param: inEnabled If true, the fields are to be enabled. If false, disabled, and their alpha set to 50%.
     */
    func setURIEntryEnabled(_ inEnabled: Bool) {
        if inEnabled {
            self.enableURIEntry()
        } else {
            self.disableURIEntry()
        }
    }
   
    /* ################################################################## */
    /**
     Disable the text item and label.
     This also reduces the alpha to 50%.
     */
    func disableURIEntry() {
        if (nil != self.uriEntryTextField) && (nil != self.uriEntryLabel) {
            self.uriEntryTextField.resignFirstResponder()
            self.uriEntryTextField.isEnabled = false
            self.uriEntryTextField.alpha = 0.5
            self.uriEntryLabel.alpha = 0.5
        }
    }
    
    /* ################################################################## */
    /**
     Enable the entry and label.
     This also restores the alpha to 100%
     */
    func enableURIEntry() {
        if (nil != self.uriEntryTextField) && (nil != self.uriEntryLabel) {
            self.uriEntryTextField.isEnabled = true
            self.uriEntryTextField.alpha = 1.0
            self.uriEntryLabel.alpha = 1.0
        }
    }
   
    /* ################################################################## */
    /**
     This will choose whether or not to enable the login entry text field and label.
     
     :param: inEnabled If true, the fields are to be enabled. If false, disabled, and their alpha set to 50%.
     */
    func setLoginEntryEnabled(_ inEnabled: Bool) {
        if inEnabled {
            self.enableLoginEntry()
        } else {
            self.disableLoginEntry()
        }
    }
    
    /* ################################################################## */
    /**
     Disable the text item and label.
     This also reduces the alpha to 50%.
     */
    func disableLoginEntry() {
        if (nil != self.loginIDEntryTextField) && (nil != self.loginIDEntryLabel) {
            self.loginIDEntryTextField.resignFirstResponder()
            self.loginIDEntryTextField.isEnabled = false
            self.loginIDEntryTextField.alpha = 0.5
            self.loginIDEntryLabel.alpha = 0.5
        }
    }
    
    /* ################################################################## */
    /**
     Enable the entry and label.
     This also restores the alpha to 100%
     */
    func enableLoginEntry() {
        if (nil != self.loginIDEntryTextField) && (nil != self.loginIDEntryLabel) {
            self.loginIDEntryTextField.isEnabled = true
            self.loginIDEntryTextField.alpha = 1.0
            self.loginIDEntryLabel.alpha = 1.0
        }
    }
    
    /* ################################################################## */
    /**
     This will choose whether or not to enable the password entry text field and label.
     
     :param: inEnabled If true, the fields are to be enabled. If false, disabled, and their alpha set to 50%.
     */
    func setPasswordEntryEnabled(_ inEnabled: Bool) {
        if inEnabled {
            self.enablePasswordEntry()
        } else {
            self.disablePasswordEntry()
        }
    }
    
    /* ################################################################## */
    /**
     Disable the text item and label.
     This also reduces the alpha to 50%.
     */
    func disablePasswordEntry() {
        if (nil != self.loginPasswordEntryTextField) && (nil != self.loginPasswordEntryLabel) {
            self.loginPasswordEntryTextField.resignFirstResponder()
            self.loginPasswordEntryTextField.isEnabled = false
            self.loginPasswordEntryTextField.alpha = 0.5
            self.loginPasswordEntryLabel.alpha = 0.5
        }
    }
    
    /* ################################################################## */
    /**
     Enable the entry and label.
     This also restores the alpha to 100%
     */
    func enablePasswordEntry() {
        if (nil != self.loginPasswordEntryTextField) && (nil != self.loginPasswordEntryLabel) {
            self.loginPasswordEntryTextField.isEnabled = true
            self.loginPasswordEntryTextField.alpha = 1.0
            self.loginPasswordEntryLabel.alpha = 1.0
            self.loginPasswordEntryLabel.isHidden = false
            self.loginPasswordEntryTextField.isHidden = false
        }
    }
    
    /* ################################################################## */
    /**
     Simply clears the password out.
     */
    func cleanPassword() {
        if nil != self.loginPasswordEntryTextField {
            self.loginPasswordEntryTextField.resignFirstResponder() // If we were editing, that stops.
            self.loginPasswordEntryTextField.text = ""
        }
    }

    // MARK: - Callback Methods
    /* ################################################################## */
    /**
        This is the callback for the URL test.
    
        :param: inData the data from the URL request.
        :param: inError Any error that occurred.
    */
    func handleTestResults(_ inData: Data!, inError: Error!) throws {
        DispatchQueue.main.async(execute: {
            self.hideURIActivity()
        })

        self.badURLReason = "LOCAL-CONNECT-REASON-BAD-URL"

        if(nil != inData) {
            let rawJSONObject: Any? = try JSONSerialization.jsonObject(with: inData, options: JSONSerialization.ReadingOptions() )
            if(nil != rawJSONObject) {
                if let jsonObject = rawJSONObject as? [[String:String]]! {  // Test to make sure the JSON is what we expect.
                    let valueArray: [[String:String]] = jsonObject as [[String:String]]
                    if(0 < valueArray.count) {
                        let jsonDictionary: [String:String] = valueArray[0]
                        if "1" == jsonDictionary["semanticAdmin"] { // We have to have the semantic admin flag set, and at least enough fields.
                            // First, extract the version as an integer
                            let testString: String!  = jsonDictionary["versionInt"]
                            // Next, get the server default long/lat
                            if let long = Double(jsonDictionary["centerLongitude"]! as String) {
                                if let lat = Double(jsonDictionary["centerLatitude"]! as String) {
                                    BMLTAdminAppDelegate.v_defaultLocation.latitude = lat
                                    BMLTAdminAppDelegate.v_defaultLocation.longitude = long
                                }
                            }
                            
                            if nil != testString {
                                let version = Int ( testString )
                                if version < self.s_minServerVersion {    // Has to be a valid version for us to pay attention.
                                    self.badURLReason = "LOCAL-CONNECT-REASON-WRONG-VERSION"
                                } else {
                                    self.badURLReason = "LOCAL-CONNECT-REASON-MISSING-FIELDS"
                                    // Next, we check the server to make sure it has all the data keys we need.
                                    // If not, we nuke the version.
                                    if let keyString = jsonDictionary["available_keys"] {
                                        let key_array = keyString.components(separatedBy: ",")
                                        if      key_array.contains("id_bigint")
                                            &&  key_array.contains("service_body_bigint")
                                            &&  key_array.contains("weekday_tinyint")
                                            &&  key_array.contains("start_time")
                                            &&  key_array.contains("duration_time")
                                            &&  key_array.contains("formats")
                                            &&  key_array.contains("longitude")
                                            &&  key_array.contains("latitude")
                                            &&  key_array.contains("meeting_name")
                                            &&  key_array.contains("location_text")
                                            &&  key_array.contains("location_info")
                                            &&  key_array.contains("location_street")
                                            &&  key_array.contains("location_city_subsection")
                                            &&  key_array.contains("location_neighborhood")
                                            &&  key_array.contains("location_municipality")
                                            &&  key_array.contains("location_sub_province")
                                            &&  key_array.contains("location_province")
                                            &&  key_array.contains("location_postal_code_1")
                                            &&  key_array.contains("comments") {
                                            self.serverVersionAsInt = version!
                                            self.badURLReason = nil
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        DispatchQueue.main.async(execute: {
            self.determineUIState()
        })
    }

    /* ################################################################## */
    /**
        This is the callback for the login attempt
    
        :param: inData the data from the URL request.
        :param: inError Any error that occurred.
    
        :discussion: We are simply looking for a response of "OK". Anything
                     else is a failed login.
    */
    func handleLoginResponse(_ inData: Data!, inError: Error!) {
        if(nil != inData) {
            let cleanedURI = BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!)
            
            // All this rigamarole is the process of parsing string data directly from the NSData response.
            var stringBuffer = Array<UInt8>(repeating: 0, count: inData.count + 1)
            
            inData.copyBytes(to: &stringBuffer, count: inData.count)
            
            let testString: NSString = NSString(bytes: UnsafeRawPointer(stringBuffer), length: inData.count, encoding: String.Encoding.ascii.rawValue)!
            
            DispatchQueue.main.async(execute: {
                // Now that we have a string, let's see if it says what we want to hear...
                if(testString == "OK") {
                    self.showSBAuto = self.hideForgetButton()   // If we indicate the forget button is hidden, then that means we are connecting for the first time.
                    
                    // We had a successful login. Tell the app delegate to mark us as connected (which will call us back), and store the login.
                    BMLTAdminAppDelegate.v_connected = true
                    BMLTAdminAppDelegate.v_rootServerBaseURI = cleanedURI!
                    type(of: self).setSelectedServiceBodyIDs([])
                    BMLTAdminAppDelegate.setConnectionStatus()
                    _ = self.storeLogin(cleanedURI!, inLoginID: self.loginIDEntryTextField!.text!, inPassword: self.loginPasswordEntryTextField.text!, inServiceBodyList: nil)
                    // Make sure what is displayed is the "cleaned" URI.
                    self.uriEntryTextField.text = cleanedURI

                    // Ask the server to send us a list of our permissions.
                    self.getAuthorizedServiceBodyIDsFromServer()
                }
                else {
                    // Throw up a message, and go back to disconnected state.
                    BMLTAdminAppDelegate.clearLoggedInSession()
                    BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-AUTH-FAIL-TITLE", inMessage: "LOCAL-ERROR-LOGIN-FAILURE-MESSAGE")
                }
            })
        }
    }
    
    /* ################################################################## */
    /**
        This is the callback for the call to get the IDs for the Service bodies for which the user is authorized.
    
        :param: inData the data from the URL request.
        :param: inError Any error that occurred.
    */
    func handleGetIDsResponse(_ inData: Data!, inError: Error!) throws {
        var serviceBodyArray: [Int] = []
        
        self.allUserServiceBodyIDs = nil
        
        if let jsonObject = try JSONSerialization.jsonObject(with: inData, options: JSONSerialization.ReadingOptions() ) as? [String:AnyObject] {
            if let serviceBodies = jsonObject["service_body"] as? [[String:AnyObject]] {
                // We go through the Service bodies, and look for ones in which we have permission to edit meetings.

                for serviceBody in serviceBodies {
                    if let id: Int = serviceBody["id"] as? Int {
                        let userPermission = serviceBody["permissions"] as! Int
                        if self.s_isMeetingAdmin <= userPermission {
                            serviceBodyArray.append(id)
                            }
                        }
                    }
                } else if let serviceBody = jsonObject["service_body"] as? [String:AnyObject] {
                    if let id: Int = serviceBody["id"] as? Int {
                        let userPermission = serviceBody["permissions"] as! Int
                        if self.s_isMeetingAdmin <= userPermission {
                            serviceBodyArray = [id]
                        }
                    }
                }
            }
        
        DispatchQueue.main.async(execute: {
            // OK. At this point, the static v_userServiceBodyIDs data member has the IDs of all the Service bodies that this user is allowed to edit.
            
            var scramTheReactor: Bool = true    // This is a "dead man's switch." It will prevent the user from logging in if there are no Service bodies they are allowed to edit.
            
            // This app only allows the user to search for meetings they are cleared to administer.
            for sb_id in serviceBodyArray {
                if(0 < sb_id) {
                    scramTheReactor = false
                }
            }
            
            if(scramTheReactor) { // We respond to no allowed Service bodies by logging out and taunting the user.
                self.weAreConnecting = false
                BMLTAdminAppDelegate.clearLoggedInSession() // Throw the switch, Igor!
                BMLTAdminAppDelegate.displayErrorAlert(NSLocalizedString("LOCAL-ERROR-AUTH-TITLE", comment: ""), inMessage: NSLocalizedString("LOCAL-ERROR-CANT-ADMIN-MESSAGE", comment: ""))
                self.processServerCredentials()
            }
            else {
                self.allUserServiceBodyIDs = serviceBodyArray
                self.getFormatsFromServer() // Otherwise, we get the formats next.
            }
        })
    }
    
    /* ################################################################## */
    /**
        This is the callback for the call to get the formats available on the server.
    
        :param: inData the data from the URL request.
        :param: inError Any error that occurred.
    */
    func handleLoadFormatData(_ inData: Data!, inError: Error!) throws {
        var result: [Int:[String:String]]! = nil
        
        let jsonObject = try JSONSerialization.jsonObject(with: inData, options: JSONSerialization.ReadingOptions()) as! NSArray
        if(0 < jsonObject.count) {
            for format in jsonObject {
                if let formatObject = format as? [String:String] {
                    if let id = formatObject["id"] {
                        if(nil == result) {
                            result = [Int:[String:String]]()
                            }
                        
                        result.updateValue(formatObject, forKey: Int ( id )!)
                        }
                    }
                }
            }

        DispatchQueue.main.async(execute: {
            BMLTAdminAppDelegate.v_formats = result
        
            if(nil != result) {
                self.getServiceBodyInfoFromServer()
            }
        })
    }
    
    /* ################################################################## */
    /**
        This is the callback for the call to get the Service body info.
    
        :param: inData the data from the URL request.
        :param: inError Any error that occurred.
    */
    func handleLoadServiceBodyData(_ inData: Data!, inError: Error!) throws {
        var result: [Int:[String:String]]! = nil
        
        if let jsonObject: NSArray = try JSONSerialization.jsonObject(with: inData, options: JSONSerialization.ReadingOptions.allowFragments ) as? NSArray {
            if(0 < jsonObject.count) {
                for serviceBody in jsonObject {
                    if let sbObject = serviceBody as? [String:String] {
                        if let id = sbObject["id"] {    // We only add the Service body if the ID matches.
                            if let intVal: Int = Int( id ) {
                                if(self.allUserServiceBodyIDs.contains(intVal)) {
                                    if(nil == result) {
                                        result = [Int:[String:String]]()
                                    }
                                    result.updateValue(sbObject, forKey: intVal)
                                }
                            }
                        }
                    }
                }
            }
            
            DispatchQueue.main.async(execute: {
                BMLTAdminAppDelegate.v_serviceBodyData = result

                self.weAreConnecting = false
                
                if nil != result {
                    type(of: self).setSelectedServiceBodyIDs(type(of: self).fetchStoredServiceBodyIDs(type(of: self).v_loginURI, inLoginID: type(of: self).v_loginID))
                    
                    if 0 == type(of: self).getSelectedServiceBodyIDs().count {
                        type(of: self).setSelectedServiceBodyIDs(self.allUserServiceBodyIDs)
                    }
                    
                    // We quickly show this button if need be.
                    let numIDs = self.allUserServiceBodyIDs.count
                    let isHidden = (2 > numIDs)
                    self.getServiceBodySelectorButton.isHidden = isHidden
                    
                    // Save the last successful login, as well as the password in the keychain.
                    _ = BMLTAdminAppDelegate.v_appPrefs.setLastSuccessfulLoginToPref(type(of: self).v_loginURI, inLoginID: type(of: self).v_loginID)
                    self.loginPasswordEntryTextField.text = ""
                    
                    if 1 == self.allUserServiceBodyIDs.count {
                        type(of: self).v_userServiceBodyIDs = self.allUserServiceBodyIDs
                    }
                    
                    // If we need to immediately open the Service Body Selection, we simulate a press of the Service Bodies button
                    if (1 < self.allUserServiceBodyIDs.count) && self.showSBAuto {
                        self.getServiceBodySelectorButton.sendActions(for: UIControlEvents.touchUpInside)
                    } else {
                        if 0 < type(of: self).v_userServiceBodyIDs.count {
                            self.tabBarController?.selectedIndex = BMLTAdminAppDelegate.ControllerIndex.search.rawValue
                        }
                    }
                }
                
                self.uriChanged = false
                self.loginChanged = false
            })
        }
    }
    
    /* ################################################################## */
    /**
     This is the callback from the TouchID login attempt.
     
     :param: inSuccess If true, then the TouchID was successful.
     :param: inError Any error that may have occurred.
     */
    func touchIDCallback ( _ inSuccess: Bool, inError: Error?) -> Void {
        DispatchQueue.main.async(execute: {
            var success = inSuccess
            
            if(success) {
                if(nil != self.keychainWrapper) {
                    let passwordFetched: AnyObject? = self.keychainWrapper.object(forKey: BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!) + "-" + self.loginIDEntryTextField!.text!) as AnyObject?
                    
                    if(nil != passwordFetched) {
                        self.loginPasswordEntryTextField.text = (passwordFetched as! String)
                        self.connectButtonHit(self.connectButton)
                    }
                    else {
                        success = false
                    }
                }
                else {
                    success = false
                }
            }
            
            if(!success) {
                // This sets the password login as the text entry, and is the default response to any error or failed login.
                if nil != self.loginPasswordEntryTextField {
                    self.loginPasswordEntryTextField.becomeFirstResponder()
                }
                self.determineUIState()
            }
        })
    }
    
    // MARK: - Response Request Methods
    /* ################################################################## */
    /**
     This is called to request the authorized Service bodies for the logged-in user.
     */
    func getAuthorizedServiceBodyIDsFromServer() {
        let cleanedURI = BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!)
        
        if(BMLTAdminAppDelegate.v_connected) {
            if((nil != self.loginIDEntryTextField) && (nil != self.loginPasswordEntryTextField) && ("" != cleanedURI) && ("" != self.loginIDEntryTextField.text) && ("" != self.loginPasswordEntryTextField.text)) {
                let refCon: BMLTAdminConnectViewControllerCallType = BMLTAdminConnectViewControllerCallType.GetIDs
                let connectionURI = cleanedURI! + self.s_adminAdminQuerySuffix + "get_permissions"
                _ = BMLTAdminCommunicator(connectionURI, dataSource: self.communicatorSource, delegate: self, refCon: refCon.rawValue as AnyObject?)
            }
        }
        else {
            BMLTAdminAppDelegate.clearLoggedInSession() // This will force a disconnect.
        }
    }
    
    /* ################################################################## */
    /**
     This is called to request the available formats from the server.
     */
    func getFormatsFromServer() {
        if nil != self.uriEntryTextField {
            DispatchQueue.main.async(execute: {
                let cleanedURI = BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!)
                BMLTAdminAppDelegate.v_formats = nil
                if((BMLTAdminAppDelegate.v_connected) && ("" != cleanedURI)) {
                    let refCon: BMLTAdminConnectViewControllerCallType = BMLTAdminConnectViewControllerCallType.GetFormats
                    let connectionURI = cleanedURI! + self.s_formatsSuffix
                    _ = BMLTAdminCommunicator(connectionURI, dataSource: self.communicatorSource, delegate: self, refCon: refCon.rawValue as AnyObject?)
                }
                else {
                    BMLTAdminAppDelegate.clearLoggedInSession() // This will force a disconnect.
                }
            })
        }
    }
    
    /* ################################################################## */
    /**
     This is called to request the Service Body Info from the server.
     */
    func getServiceBodyInfoFromServer() {
        if nil != self.uriEntryTextField {
            DispatchQueue.main.async(execute: {
                let cleanedURI = BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!)
                BMLTAdminAppDelegate.v_serviceBodyData = nil
                if((BMLTAdminAppDelegate.v_connected) && ("" != cleanedURI)) {
                    let refCon: BMLTAdminConnectViewControllerCallType = BMLTAdminConnectViewControllerCallType.GetServiceBodies
                    let connectionURI = cleanedURI! + self.s_serviceBodiesSuffix
                    _ = BMLTAdminCommunicator(connectionURI, dataSource: self.communicatorSource, delegate: self, refCon: refCon.rawValue as AnyObject?)
                }
                else {
                    BMLTAdminAppDelegate.clearLoggedInSession() // This will force a disconnect.
                }
            })
        }
    }
    
    /* ################################################################## */
    /**
     Called when we want to validate the server credentials (after text entry).
     */
    func processServerCredentials() {
        DispatchQueue.main.async(execute: {
            self.uriChanged = true
            self.serverVersionAsInt = 0
            
            if (nil != self.communicatorSource) && (nil != self.uriEntryTextField.text) && !(self.uriEntryTextField.text?.isEmpty)! {
                self.displayURIActivity()

                // Make sure this is cleared out, if we aren't connected.
                if !BMLTAdminAppDelegate.v_connected {
                    type(of: self).setSelectedServiceBodyIDs([])
                }
                
                var cleanedURI = BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!)
                let refCon: BMLTAdminConnectViewControllerCallType = BMLTAdminConnectViewControllerCallType.TestProbe
                
                if "" != cleanedURI {  // If we have anything in the root server URI, we try it out.
                    self.communicatorSource.suppressErrors = true
                    cleanedURI = cleanedURI! + self.s_testSuffix
                    self.activeCommunicator = BMLTAdminCommunicator(cleanedURI!, dataSource: self.communicatorSource, delegate: self, refCon: refCon.rawValue as AnyObject?)
                }
            }
        })
    }
    
    // MARK: - BMLTAdminCommunicatorDataSinkProtocol Methods -
    /* ################################################################## */
    /**
        This is the callback handler for the URL dispatches.
    
        :param: inHandler The handler for this call.
        :param: inJSONObject The data from the URL request. If nil, the call failed to produce. Check the handler's error data member.
        :param: inError Any errors that occurred
        :param: inRefCon The data/object passed in via the 'refCon' parameter in the initializer.
    */
    override func responseData(_ inHandler: BMLTAdminCommunicator?, inResponseData: Data?, inError: Error?, inRefCon: AnyObject?) {
        self.activeCommunicator = nil
        if(nil != inRefCon) {   // Make sure we have a selector object.
            let callType: BMLTAdminConnectViewControllerCallType = BMLTAdminConnectViewControllerCallType(rawValue: inRefCon as! String)!
            
            do {
                switch(callType) {  // See what this call wanted us to do.
                    case .TestProbe:
                        try self.handleTestResults(inResponseData, inError: inError)
                    case .Login:
                        self.communicatorSource.suppressErrors = false
                        self.uriChanged = false
                        self.loginChanged = false
                        self.handleLoginResponse(inResponseData, inError: inError)
                    case .GetIDs:
                        self.communicatorSource.suppressErrors = false
                        self.uriChanged = false
                        self.loginChanged = false
                        try self.handleGetIDsResponse(inResponseData, inError: inError)
                    case .GetFormats:
                        self.communicatorSource.suppressErrors = false
                        self.uriChanged = false
                        self.loginChanged = false
                        try self.handleLoadFormatData(inResponseData, inError: inError)
                    case .GetServiceBodies:
                        self.communicatorSource.suppressErrors = false
                        self.uriChanged = false
                        self.loginChanged = false
                        try self.handleLoadServiceBodyData(inResponseData, inError: inError)
                }
            } catch {
                // We ignore errors, here.
            }
            
            // Just make sure we update the connection status properly.
            DispatchQueue.main.async(execute: {
                BMLTAdminAppDelegate.setConnectionStatus()
            })
        }
    }
   
    // MARK: - IBAction Methods -    
    /* ################################################################## */
    /**
     Called when somoene touches the "Why Can't I Connect?" button.
     It simply brings up an alert, explaining why the URL is wrong.
     
     :param: inSender The Button Object that caused this to be called.
     */
    @IBAction func whyCantIConnectHit(_ sender: UIButton) {
        if (nil != self.badURLReason) && !self.badURLReason.isEmpty && !self.hideBadURIButton() {
            BMLTAdminAppDelegate.displayErrorAlert("LOCAL-CONNECT-REASON-ALERT-TITLE", inMessage: self.badURLReason)
        }
    }
    
    /* ################################################################## */
    /**
        Called when text is entered into a Text Entry.
        
        :param: inSender The Text Entry Field that caused this to be called.
    */
    @IBAction func textEntered(_ inSender: UITextField) {
        // Determine which text field had text, and set the relevant data member to that value.
        self.uriChanged = false
        self.loginChanged = false
        
        switch inSender {
        case self.uriEntryTextField:
            self.weAreConnecting = false
            if inSender.text!.isEmpty {
                self.determineUIState()
            } else {
                self.processServerCredentials()
            }
            
        case self.loginIDEntryTextField:
            self.loginChanged = true
            self.determineUIState()
            
        default:
            self.determineUIState()
        }
    }
    
    /* ################################################################## */
    /**
        Called when the "CONNECT" button is hit.
        
        :param: inSender The CONNECT button.
    */
    @IBAction func connectButtonHit(_ inSender: UIButton) {
        if nil != BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!) {
            var cleanedURI: String = BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!)!

            let failedSSLTest = !(cleanedURI.beginsWith("https://")) && BMLTAdminAppDelegate.v_appPrefs.requireSSL
            self.uriChanged = false
            self.loginChanged = false

            if(!BMLTAdminAppDelegate.v_connected && !failedSSLTest) {
                BMLTAdminAppDelegate.v_rootServerBaseURI = ""
                if(("" != cleanedURI) && ("" != self.loginIDEntryTextField.text) && ("" != self.loginPasswordEntryTextField.text)) {
                    self.connectButton.setTitle(NSLocalizedString("LOCAL-CONNECT-CONNECTING", comment: ""), for: UIControlState())
                    self.connectButton.isEnabled = false
                    self.weAreConnecting = true
                    let loginText:String = self.loginIDEntryTextField.text!
                    type(of: self).v_loginURI = cleanedURI
                    type(of: self).v_loginID = loginText
                    let passwordText:String = self.loginPasswordEntryTextField.text!
                    let refCon: BMLTAdminConnectViewControllerCallType = BMLTAdminConnectViewControllerCallType.Login
                    
                    cleanedURI += self.s_adminAdminQuerySuffix
                    
                    cleanedURI += "login&c_comdef_admin_login=" + loginText
                    
                    cleanedURI += "&c_comdef_admin_password=" + passwordText
                    
                    _ = BMLTAdminCommunicator(cleanedURI, dataSource: self.communicatorSource, delegate: self, refCon: refCon.rawValue as AnyObject?)
                }
            } else {
                BMLTAdminAppDelegate.clearLoggedInSession() // This will force a disconnect.
            }
        }
    }
    
    /* ################################################################## */
    /**
     Called when the "Forget This Login" button is hit.
     
     :param: inSender The FORGET button.
     */
    @IBAction func forgetURIHit(_ inSender: UIButton) {
        let cleanedURI = BMLTAdminAppDelegate.cleanURI(self.uriEntryTextField.text!)
        if let loginID = self.loginIDEntryTextField!.text {
            self.deleteLogin(cleanedURI!, inLoginID: loginID)
            self.uriChanged = true
            type(of: self).v_loginURI = ""
            type(of: self).v_loginID = ""
            self.allUserServiceBodyIDs = []
            type(of: self).setSelectedServiceBodyIDs([])
            
            self.determineUIState()
        }
    }

    /* ################################################################## */
    /**
        Called when the TouchID ("Thumbprint") button is hit.
    
        :param: inSender The TouchID button.
    */
    @IBAction func touchIDButtonHit(_ inSender: UIButton) {
        if(self.supportsTouchID) {
            self.closeKeyboard(inSender)
            self.uriChanged = false
            self.loginChanged = false
            self.cleanPassword()
            // We create a new authentication context every time, which forces the user to log in again.
            let authenticationContext = LAContext()
            authenticationContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: NSLocalizedString("LOCAL-TOUCHID-REASON", comment: ""), reply: self.touchIDCallback )
        }
    }
    
    // MARK: - UITextFieldDelegate Methods -
    /* ################################################################## */
    /**
        Called when the "DONE" button is hit.
        
        :param: textField The Text Entry Field that triggered this.
    
        :returns: a Bool Always false (intercepts the event).
    */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.closeKeyboard(textField)   // Close the keyboard. We don't need to do anything else.
        self.uriChanged = false
        self.loginChanged = false
        return false    // Intercept the event.
    }
    
    // MARK: - Base Class Override Methods -
    /* ################################################################## */
    /**
     */
    override func runFirstSelectionTasks() {
        super.runFirstSelectionTasks()
        self.appDisconnected = false
    }
    
    /* ################################################################## */
    /**
        Called after the view has finished loading.
    
        We use this opportunity to apply the localization.
        All of the storyboard items have localization keys as their
        initial text, and we use those keys to do the localization.
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        self.badURLReason = "LOCAL-CONNECT-REASON-BAD-URL"
        
        if(nil != self.uriEntryLabel) { // We don't do any of this if nothing loaded (like when we test).
            // Set the back item for the Service Body Selection Screen.
            let backItem = UIBarButtonItem()
            backItem.title = NSLocalizedString("LOCAL-TAB-CONNECT", comment: "")
            self.navigationItem.backBarButtonItem = backItem
            
            self.uriEntryLabel.text = NSLocalizedString(self.uriEntryLabel.text!, comment: "")
            self.uriEntryTextField.placeholder = NSLocalizedString(self.uriEntryTextField.placeholder!, comment: "")
            
            self.loginIDEntryLabel.text = NSLocalizedString(self.loginIDEntryLabel.text!, comment: "")
            self.loginIDEntryTextField.placeholder = NSLocalizedString(self.loginIDEntryTextField.placeholder!, comment: "")
            self.loginPasswordEntryLabel.text = NSLocalizedString(self.loginPasswordEntryLabel.text!, comment: "")
            self.loginPasswordEntryTextField.placeholder = NSLocalizedString(self.loginPasswordEntryTextField.placeholder!, comment: "")
            self.connectButton.setTitle(NSLocalizedString("LOCAL-CONNECT-CONNECT", comment: ""), for: UIControlState())
            self.deleteSingleURIButton.setTitle(NSLocalizedString(self.deleteSingleURIButton.title(for: UIControlState())!, comment: ""), for: UIControlState())
            self.getServiceBodySelectorButton.setTitle(NSLocalizedString(self.getServiceBodySelectorButton.title(for: UIControlState())!, comment: ""), for: UIControlState())
            self.whyCantIConnectButton.setTitle(NSLocalizedString(self.whyCantIConnectButton.title(for: UIControlState())!, comment: ""), for: UIControlState())
            
            self.activeCommunicator = nil
            
            // See if we had stored a previous successful URI.
            if let lastSuccessfulLogin = BMLTAdminAppDelegate.v_appPrefs.getLastSuccessfulLoginFromPref() {
                self.uriEntryTextField.text = lastSuccessfulLogin.url
                self.loginIDEntryTextField.text = lastSuccessfulLogin.loginID
                self.weAreConnecting = false
            }
        }
    }
    
    /* ################################################################## */
    /**
        Called when the view is loaded.
    
        This is when we sniff for TouchID support.
    */
   override func loadView() {
        super.loadView()
        var error: NSError? = nil
        /** This will be our temporary TouchID authentication context. */
        let authenticationContext = LAContext()
    
        self.supportsTouchID = authenticationContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error)
        
        if(nil != error) {  // Very basic. Any problems, no can do.
            self.supportsTouchID = false
        }
    }
    
    /* ################################################################## */
    /**
     Called when the view has appeared.
     
     :param: animated True, if the appearance is animated.
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.processServerCredentials()
    }
}

