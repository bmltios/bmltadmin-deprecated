//
//  BMLTAdminDeletedViewController.swift
//  BMLTAdmin
/**
    :license:
    Created by MAGSHARE.
    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BMLT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this code.  If not, see <http://www.gnu.org/licenses/>.
*/

import UIKit

// MARK: - Classes -
/* ###################################################################################################################################### */
/**
*/
class BMLTAdminDeletedViewController: BMLTAdminBaseViewController, UITableViewDelegate, UITableViewDataSource {
    static let sPathPrefix: String  = "/local_server/server_admin/json.php"
    static let intervalJumpInMonths: Int = 3
    /** This is the URL path prefix for saving changes. */
    
    /** This tracks the currently selected table row. */
    var currentlySelectedIndexPath: IndexPath = IndexPath()
    
    var deletedMeetingObjects: [[String:String]] = []
    var restoringID: Int = 0
    
    @IBOutlet weak var deletedMeetingsTableView: UITableView!
    @IBOutlet weak var busyLabel: UILabel!
    @IBOutlet weak var busyActivityIndicatorView: UIActivityIndicatorView!
    
    // MARK: - Internal Methods -
    /* ################################################################## */
    /**
     This hides most of the main window items, and displays a busy throbber.
     */
    func _showBusyItems() {
        self.busyLabel.text = NSLocalizedString("LOCAL-DELETED-BUSY-LABEL", comment: "")
        self.deletedMeetingsTableView.isHidden = true
        self.busyLabel.isHidden = false
        self.busyActivityIndicatorView.isHidden = false
    }
    
    /* ################################################################## */
    /**
     This hides most of the main window items, and displays a busy throbber.
     */
    func _showRestoringBusyItems() {
        self.busyLabel.text = NSLocalizedString("LOCAL-DELETED-RESTORING-BUSY-LABEL", comment: "")
        self.deletedMeetingsTableView.isHidden = true
        self.busyLabel.isHidden = false
        self.deletedMeetingsTableView.deselectRow(at: self.currentlySelectedIndexPath, animated: true)
        self.currentlySelectedIndexPath = IndexPath()
        self.selectedMeeting = nil
        self.busyActivityIndicatorView.isHidden = false
    }
    
    /* ################################################################## */
    /**
     This hides most of the main window items, and displays a label that indicates no results were found.
     */
    func _showNoresultItems() {
        self.busyLabel.text = NSLocalizedString("LOCAL-DELETED-NO-RESULTS-LABEL", comment: "")
        self.deletedMeetingsTableView.isHidden = true
        self.busyLabel.isHidden = false
        self.deletedMeetingsTableView.deselectRow(at: self.currentlySelectedIndexPath, animated: true)
        self.currentlySelectedIndexPath = IndexPath()
        self.selectedMeeting = nil
        self.busyActivityIndicatorView.isHidden = true
    }
    
    /* ################################################################## */
    /**
     This hides the busy throbber, and restores the main window.
     */
    func _hideBusyItems() {
        self.deletedMeetingsTableView.isHidden = false
        self.busyLabel.isHidden = true
        self.busyActivityIndicatorView.isHidden = true
    }
    
    /* ################################################################## */
    /**
     This returns the URI for the restore command.
     
     :returns: the URI, as a string.
     */
    func _generateLoadURI() -> String {
        var ret: String = BMLTAdminAppDelegate.v_rootServerBaseURI + type(of: self).sPathPrefix

        ret += "?admin_action=get_deleted_meetings"
        
        ret += "&service_body_id="
        
        var temp: String = ""
        
        for id in BMLTAdminConnectViewController.getSelectedServiceBodyIDs() {
            if BMLTAdminConnectViewController.isServiceBodyValid(id) {
                if !temp.isEmpty {
                    temp += ","
                }
                
                temp += String(id)
            }
        }
        
        ret += temp
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This begins the process of loading all available meetings into the app.
     */
    func _loadMeetings() {
        self._showBusyItems()
        self.selectedMeeting = nil
        self.deletedMeetingsTableView.deselectRow(at: self.currentlySelectedIndexPath, animated: true)
        self.currentlySelectedIndexPath = IndexPath()
        _ = BMLTAdminCommunicator (self._generateLoadURI(), dataSource: BMLTAdminAppDelegate.appDelegate().connectionSession, delegate: self, refCon: nil)
    }
    
    /* ################################################################## */
    /**
     This presents a confirmation dialog, asking if the user really wants to restore the meeting.
     
     If they say yes, the meeting is restored, and the edit screen is brought up for that meeting.
     
     If they say no, then they are returned to the deleted meetings list.
     */
    func _displayRestoreConfirmAlert() {
        if nil != self.presentingViewController {
            self.presentingViewController?.dismiss(animated: false, completion: nil)
        }
        
        let alertController = UIAlertController(title: NSLocalizedString("LOCAL-DELETED-RESTORE-CONFIRM-TITLE", comment: ""), message: NSLocalizedString("LOCAL-DELETED-RESTORE-MESSAGE-TITLE", comment: ""), preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-DELETED-RESTORE-FINAL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: self._restoreOKCallback)
        
        alertController.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("LOCAL-DELETED-RESTORE-CANCEL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.cancel, handler: self._restoreCancelCallback)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /* ################################################################## */
    /**
     Called when the user dismisses the success alert.
     */
    func successCallback() {
        BMLTAdminAppDelegate.v_searchController.reloadAndSelectMeetingByID(self.restoringID)
        self._loadMeetings()
    }
    
    /* ################################################################## */
    /**
     If the user wants to restore the meeting, we generate a restore URI, and send it to the server.
     
     :param: inAction The alert action object (ignored)
     */
    func _generateRestoreURI(_ inID: Int) -> String {
        return BMLTAdminAppDelegate.v_rootServerBaseURI + type(of: self).sPathPrefix + "?admin_action=restore_deleted_meeting&meeting_id=" + String(inID)
    }
    
    /* ################################################################## */
    /**
     If the user wants to restore the meeting, we generate a restore URI, and send it to the server.
     
     :param: inAction The alert action object (ignored)
     */
    func _restoreOKCallback(_ inAction: UIAlertAction) {
        if let meeting = self.selectedMeeting {
            self.selectedMeeting = nil
            if let id = Int(meeting["meeting_id"]!) {
                self._showRestoringBusyItems()
                _ = BMLTAdminCommunicator (self._generateRestoreURI(id), dataSource: BMLTAdminAppDelegate.appDelegate().connectionSession, delegate: self, refCon: id as AnyObject?)
            }
        }
        
        self.deletedMeetingsTableView.deselectRow(at: self.currentlySelectedIndexPath, animated: true)
        self.currentlySelectedIndexPath = IndexPath()
        self.selectedMeeting = nil
    }
    
    /* ################################################################## */
    /**
     If the user wants to restore the meeting, we generate a restore URI, and send it to the server.
     
     :param: inAction The alert action object (ignored)
     */
    func _restoreCancelCallback(_ inAction: UIAlertAction) {
        self.selectedMeeting = nil
        self.deletedMeetingsTableView.deselectRow(at: self.currentlySelectedIndexPath, animated: true)
        self.currentlySelectedIndexPath = IndexPath()
    }
    
    // MARK: - BMLTAdminCommunicatorDataSinkProtocol Methods -
    /* ################################################################## */
    /**
     The response callback.
     
     :param: inHandler The handler for this call.
     :param: inResponseData The data from the URL request. If nil, the call failed to produce. Check the handler's error data member.
     :param: inError Any errors that occurred
     :param: inRefCon The data/object passed in via the 'refCon' parameter in the initializer.
     */
    override func responseData(_ inHandler: BMLTAdminCommunicator?, inResponseData: Data?, inError: Error?, inRefCon: AnyObject?) {
        if (nil != inError) {   // This message is if there was some communication error.
            BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-COMM-FAIL-TITLE", inMessage: "LOCAL-ERROR-COMM-FAILURE-MESSAGE")
        } else {
            if nil != inRefCon {
                self.restoringID = inRefCon as! Int
                BMLTAdminAppDelegate.displayAlert("LOCAL-DELETED-RESTORE-ALERT-TITLE", inMessage: "LOCAL-DELETED-RESTORE-ALERT-MESSAGE", inCallback: self.successCallback)
            } else {
                self.selectedMeeting = nil
                if(nil != inResponseData) {
                    // We carefully unwind the JSON response.
                    if(nil != inResponseData) {
                        do {
                            let jsonObject = try JSONSerialization.jsonObject(with: inResponseData!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary

                            if nil != jsonObject {
                                // Well, as it turns out, there's not really a "toll-free" bridging between the NS types and the native Swift types. We need to unwind the data, and re-form it.
                                let castDictionary = jsonObject as! [String:AnyObject]
                                var searchResults: [[String:String]] = []
                                
                                // The data are gathered in a single array, called "row."
                                if let rowElement = castDictionary["row"] {
                                    let changeListTemp = rowElement as! NSArray
                                    let changeListTemp2 = changeListTemp as! [NSDictionary]
                                    for element in changeListTemp2 {
                                        if (nil == element["weekday_tinyint"]) || (((element["weekday_tinyint"] as! NSNumber).int32Value) == 0) {
                                            continue
                                        }
                                        
                                        if (nil == element["start_time"]) || (element["start_time"] as! String).isEmpty {
                                            continue
                                        }
                                        
                                        if (nil == element["meeting_name"]) || (element["meeting_name"] as! String).isEmpty {
                                            continue
                                        }
                                        
                                        if ((nil == element["location_municipality"]) || (element["location_municipality"] as! String).isEmpty) && ((nil == element["location_city_subsection"]) || (element["location_city_subsection"] as! String).isEmpty) {
                                            continue
                                        }
                                        
                                        var newElement:[String:String] = [:]
                                        
                                        for (key, value) in element {
                                            if (value as AnyObject) is NSNumber {
                                                newElement[key as! String] = String((value as! NSNumber).int32Value)
                                            } else {
                                                if (value as AnyObject) is NSString {
                                                    newElement[key as! String] = value as? String
                                                }
                                            }
                                        }
                                        searchResults.append(newElement)
                                    }
                                    
                                    self.deletedMeetingObjects = searchResults
                                }
                            }
                            
                            // If we found results, we display them. If not, we indicate no results found.
                            if 0 < self.deletedMeetingObjects.count {
                                DispatchQueue.main.async(execute: {
                                    self._hideBusyItems()
                                    self.deletedMeetingsTableView.reloadData()
                                })
                            } else {
                                DispatchQueue.main.async(execute: {
                                    self._showNoresultItems()
                                })
                            }

                        } catch {
                            DispatchQueue.main.async(execute: {
                                BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-CONN-FAIL-TITLE", inMessage: "LOCAL-ERROR-CONN-FAILURE-MESSAGE")
                            })
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - UIScrollViewDelegate Protocol Methods -
    /* ################################################################## */
    /**
     This is called when the scroll view has ended dragging.
     We use this to trigger a reload, if the scroll was pulled beyond its limit by a certain number of display units.
     
     :param: scrollView The text view that experienced the text change.
     :param: velocity The velocity of the scroll at the time of this call.
     :param: targetContentOffset The offset we can send back (unused).
     */
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let offsetTarget = BMLTAdminAppDelegate.v_searchController.sScrollToReloadThreshold
        if ( (velocity.y < 0) && (scrollView.contentOffset.y < offsetTarget) ) {
            self._loadMeetings()
        }
    }
    
    // MARK: - UITableViewDelegate Protocol Methods -
    /* ################################################################## */
    /**
     This is called when the user selects a table row. It will open the editor
     for the selected meeting.
     
     :param: tableView The Table View
     :param: indexPath The index path of the selected row.
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedMeeting = self.deletedMeetingObjects[(indexPath as NSIndexPath).row]
        self.currentlySelectedIndexPath = indexPath
        self._displayRestoreConfirmAlert()
    }
    
    // MARK: - UITableViewDataSource Protocol Methods -
    /* ################################################################## */
    /**
     This returns the number of Meetings we'll need to display.
     
     :param: tableView The Table View
     :param: section The 0-based index of the section.
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.deletedMeetingObjects.count
    }
    
    /* ################################################################## */
    /**
     This returns one cell for a Meeting.
     We reuse the cell class from the search screen.
     
     :param: tableView The Table View
     :param: indexPath The index path to the cell we need.
     
     :returns:   one cell object for the given Meeting
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ret: BMLTAdminSearchViewControllerTableCell! = nil

        var meeting = self.deletedMeetingObjects[(indexPath as NSIndexPath).row]
        
        if let reuseID = meeting["meeting_id"] {
            meeting["published"] = "1"  // It's not actually published, but doing this causes the background to be clear.
            
            // If we can't reuse an existing cell, we create a new one.
            ret = tableView.dequeueReusableCell(withIdentifier: reuseID) as? BMLTAdminSearchViewControllerTableCell
            if nil == ret {
                ret = BMLTAdminSearchViewControllerTableCell(inMeeting: meeting, style: UITableViewCellStyle.default, reuseIdentifier: reuseID)
                // We load the prototype from our xib file.
                if nil != ret {
                    if let view = Bundle.main.loadNibNamed("TablePrototype", owner: ret, options: nil)?.first as? UIView {
                        ret.addSubview(view)
                        if 0 == ((indexPath as NSIndexPath).row % 2) {
                            ret.mainView.backgroundColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.1)
                        } else {
                            ret.mainView.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0)
                        }
                    }
                }
            }
            
            ret.meeting = meeting
        }
        
        ret.setNeedsLayout()
        
        return ret
    }
    
    // MARK: - Superclass Override Methods -
    /* ################################################################## */
    /**
     This is called when the tab is first selected and set up.
     */
    override func runFirstSelectionTasks() {
        super.runFirstSelectionTasks()
        if self.appDisconnected {
            self._loadMeetings()
        }

        self.currentlySelectedIndexPath = IndexPath()
        self.appDisconnected = false
    }
}
