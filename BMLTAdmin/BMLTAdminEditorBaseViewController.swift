//
//  BMLTAdminEditorBaseViewController.swift
//  BMLTAdmin
/**
 :license:
 Created by MAGSHARE.
 This is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 BMLT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this code.  If not, see <http://www.gnu.org/licenses/>.
 */

import UIKit
import MapKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


// MARK: - Classes -
/* ###################################################################################################################################### */
/**
 This handles the display of the main editor screen, and the new meeting screen. It is a base class for the embedded editable items.
 The editor has a scrolling area with a large set of editing tools. This class controls what is inside that scrolling area.
 
 Both the Meeting Editor (From the Search Tab) and the New Meeting Editor (New Tab) use this class to control the contents of large
 scrollable areas that are filled with the editor.
 */
class BMLTAdminEditorBaseViewController: BMLTAdminBaseViewController, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MKMapViewDelegate, UITextViewDelegate, CLLocationManagerDelegate {
    /** These represent the segments for the map type. */
    enum MapSwitchSegments: Int {
        case mapType = 0
        case satelliteType = 1
    }
    
    // MARK: - Class Variables -
    /** This will specify the map zoom we want to see. */
    static let sMapSizeInDegrees: CLLocationDegrees     = 0.0125
    
    /** This specifies the side length of a format checkbox. */
    static let sFormatCheckboxIndent: CGFloat           = 2
    static let sFormatCheckboxContainerHeight: CGFloat  = 40
    static let sFormatCheckboxContainerWidth: CGFloat   = 80
    
    /** This is how much extra space to pad the bottom of the scroll area. */
    static let sScreenPadding: CGFloat                  = 8

    /** This is the URL path prefix for saving changes. */
    static let sPathPrefix: String  = "/local_server/server_admin/json.php"
    
    // This contains the weekday names for the weekday picker.
    static let sWeekdays: [String] = [ NSLocalizedString("LOCAL-WEEKDAY-SUN", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-MON", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-TUE", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-WED", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-THU", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-FRI", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-SAT", comment: "")
    ]

    /** A couple of default values for new empty meetings. */
    static let sDefaultStartTimeString: String  =   "20:30:00"
    static let sDefaultWeekdayString: String    =   "1"
    
    // MARK: - Instance Constants -
    let sMeetingStorageKey: String = "BMLTAdminEditorBaseViewController_Meeting"
    
    // MARK: - Instance Properties -
    /** This contains the meeting this editor is working on. */
    var meeting:BMLTAdminMeeting! = nil

    /** This tracks the actual view that contains all the components, and that is embedded into the UIScrollView. */
    var editorView: UIView! = nil
    
    /** This is the annotation for the meeting map marker. */
    var meetingMarker: BMLTAdminAnnotation! = nil
    
    /** This is used in determining the layout of the format checkbox table. We calculate how many checkboxes go across each row. */
    var numFormatCheckboxesPerRow: Int      = 0
    
    /** This contains our formats, sorted by code, alphabetically. */
    var sortedFormats:[[String:String]]!    = nil

    /** This is a meeting object that holds a snapshot of the original meeting. It is used to compare against the current meeting object to set/clear the meetingDirty flag. */
    var compMeeting: BMLTAdminMeeting! = nil
    
    /** This will hold our location manager. */
    var locationManager: CLLocationManager! = nil
    
    /** This holds our geocoder object when we are looking up addresses. */
    var geocoder: CLGeocoder! = nil
    
    /** This is set to true if any changes have been made to the meeting. */
    var meetingDirty: Bool = false
    
    /** This is set to true if this is a brand new meeting. */
    var newMeeting: Bool = false
    
    // MARK: - Instance IB Properties -
    /** This goes to the top of the scrollable view, and is used when we hide the published items. */
    @IBOutlet weak var nameTopConstraint: NSLayoutConstraint!
    
    /** This displays the correct image background. */
    @IBOutlet weak var backgroundGradientImageView: UIImageView!

    /** This is the Save button. It remains disabled unless changes have been made. */
    @IBOutlet weak var saveButton: UIBarButtonItem!
    /** This is the navigation item. We reference it here as a convenience. */
    @IBOutlet weak var navItem: UINavigationItem!
    
    /** The scroll view that will hold the editable items. */
    @IBOutlet weak var editableItemsScroller: UIScrollView!

    /** The checkbox and label for the published state. */
    @IBOutlet weak var publishedCheckbox: BMLTAdminCheckbox!
    @IBOutlet weak var publishedLabel: UILabel!
    
    /** The label and editable text item for the meeting name. */
    @IBOutlet weak var meetingNameLabel: UILabel!
    @IBOutlet weak var meetingNameEditableText: UITextField!
    
    /** This is the picker for the weekday. */
    @IBOutlet weak var weekdayPickerLabel: UILabel!
    @IBOutlet weak var weekdayPickerView: UIPickerView!
    
    /** The label, text item and stepper for the start time. */
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var startTimeEditField: UITextField!
    @IBOutlet weak var startTimeStepper: UIStepper!
    
    /** The label, text item and stepper for the duration. */
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var durationEditField: UITextField!
    @IBOutlet weak var durationStepper: UIStepper!
    
    /** The label and text item for the World ID. */
    @IBOutlet weak var worldIDLabel: UILabel!
    @IBOutlet weak var worldIDEditText: UITextField!
    
    @IBOutlet weak var locationHeaderLabel: UILabel!
    
    @IBOutlet weak var setAddressToLocationButton: UIButton!
    
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var locationNameEditField: UITextField!
    
    @IBOutlet weak var streetAddressLabel: UILabel!
    @IBOutlet weak var streetAddressEditField: UITextField!
    
    @IBOutlet weak var extraInfoLabel: UILabel!
    @IBOutlet weak var extraInfoEditField: UITextField!
    
    @IBOutlet weak var neighborhoodLabel: UILabel!
    @IBOutlet weak var neighborhoodEditField: UITextField!
    
    @IBOutlet weak var boroughLabel: UILabel!
    @IBOutlet weak var boroughEditField: UITextField!
    
    @IBOutlet weak var townLabel: UILabel!
    @IBOutlet weak var townEditField: UITextField!
    
    @IBOutlet weak var countyLabel: UILabel!
    @IBOutlet weak var countyEditField: UITextField!
    
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var stateEditField: UITextField!
    @IBOutlet weak var stateNagLabel: UILabel!
    
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var zipEditField: UITextField!
    
    @IBOutlet weak var setMapToAddressButton: UIButton!
    
    @IBOutlet weak var locationMapView: MKMapView!
    
    @IBOutlet var singleTapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet var doubleTapGestureRecognizer: UITapGestureRecognizer!
    
    @IBOutlet weak var mapTypeSwitch: UISegmentedControl!
    
    @IBOutlet weak var formatLabel: UILabel!
    @IBOutlet weak var formatTableView: UITableView!
    
    @IBOutlet weak var formatTableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    
    @IBOutlet weak var bottomLayoutOfScrollArea: NSLayoutConstraint!
    
    @IBOutlet weak var savingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var savingLabel: UILabel!
    
    @IBOutlet weak var serviceBodyContainerView: UIView!
    @IBOutlet weak var serviceBodyLabel: UILabel!
    @IBOutlet weak var serviceBodyPickerView: UIPickerView!
    @IBOutlet weak var serviceBodyShimConstraint: NSLayoutConstraint!

    @IBOutlet weak var deleteButton: UIButton!

    // This is really meant for subclasses. If true, the "Delete This Meeting" item is not shown.
    var deleteButtonHidden: Bool            = true
    
    // MARK: - Instance Methods -
    
    /* ################################################################## */
    /**
     Called to fille the editor view.
     */
    func editorViewLoad() {
        // We reach up to our sender, and grab any selected meeting.
        if let navC = self.navigationController {
            let myParent = navC.viewControllers[0] as! BMLTAdminBaseViewController
            self.meeting = myParent.selectedMeeting
            self.compMeeting = self.meeting
            
            // Load in the Editable Items Scroll View contents from the xib file.
            if let view = Bundle.main.loadNibNamed("BMLTAdminEditorView", owner: self, options: nil)?.first as? UIView {
                self.editorView = view
                self.editableItemsScroller.addSubview(editorView)
            } else {
                BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-PROG-FAIL-TITLE", inMessage: "LOCAL-ERROR-PROGRAM-ERROR-MESSAGE")
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
        } else {
            BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-PROG-FAIL-TITLE", inMessage: "LOCAL-ERROR-PROGRAM-ERROR-MESSAGE")
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
        
        self.singleTapGestureRecognizer.require(toFail: self.doubleTapGestureRecognizer)
        self.meetingNameLabel.text = NSLocalizedString(self.meetingNameLabel.text!, comment: "")
        self.meetingNameEditableText.placeholder = NSLocalizedString(self.meetingNameEditableText.placeholder!, comment: "")
        self.startTimeLabel.text = NSLocalizedString(self.startTimeLabel.text!, comment: "")
        self.durationLabel.text = NSLocalizedString(self.durationLabel.text!, comment: "")
        self.worldIDLabel.text = NSLocalizedString(self.worldIDLabel.text!, comment: "")
        self.worldIDEditText.placeholder = NSLocalizedString(self.worldIDEditText.placeholder!, comment: "")
        self.weekdayPickerLabel.text = NSLocalizedString(self.weekdayPickerLabel.text!, comment: "")
        self.locationHeaderLabel.text = NSLocalizedString(self.locationHeaderLabel.text!, comment: "")
        self.locationNameLabel.text = NSLocalizedString(self.locationNameLabel.text!, comment: "")
        self.locationNameEditField.placeholder = NSLocalizedString(self.locationNameEditField.placeholder!, comment: "")
        self.streetAddressLabel.text = NSLocalizedString(self.streetAddressLabel.text!, comment: "")
        self.streetAddressEditField.placeholder = NSLocalizedString(self.streetAddressEditField.placeholder!, comment: "")
        self.extraInfoLabel.text = NSLocalizedString(self.extraInfoLabel.text!, comment: "")
        self.extraInfoEditField.placeholder = NSLocalizedString(self.extraInfoEditField.placeholder!, comment: "")
        self.neighborhoodLabel.text = NSLocalizedString(self.neighborhoodLabel.text!, comment: "")
        self.neighborhoodEditField.placeholder = NSLocalizedString(self.neighborhoodEditField.placeholder!, comment: "")
        self.boroughLabel.text = NSLocalizedString(self.boroughLabel.text!, comment: "")
        self.boroughEditField.placeholder = NSLocalizedString(self.boroughEditField.placeholder!, comment: "")
        self.townLabel.text = NSLocalizedString(self.townLabel.text!, comment: "")
        self.townEditField.placeholder = NSLocalizedString(self.townEditField.placeholder!, comment: "")
        self.countyLabel.text = NSLocalizedString(self.countyLabel.text!, comment: "")
        self.countyEditField.placeholder = NSLocalizedString(self.countyEditField.placeholder!, comment: "")
        self.stateLabel.text = NSLocalizedString(self.stateLabel.text!, comment: "")
        self.stateNagLabel.text = NSLocalizedString(self.stateNagLabel.text!, comment: "")
        self.zipLabel.text = NSLocalizedString(self.zipLabel.text!, comment: "")
        self.formatLabel.text = NSLocalizedString(self.formatLabel.text!, comment: "")
        self.savingLabel.text = NSLocalizedString(self.savingLabel.text!, comment: "")
        self.commentLabel.text = NSLocalizedString(self.commentLabel.text!, comment: "")
        self.setAddressToLocationButton.setTitle(NSLocalizedString(self.setAddressToLocationButton.title(for: UIControlState())!, comment: ""), for: UIControlState())
        self.setMapToAddressButton.setTitle(NSLocalizedString(self.setMapToAddressButton.title(for: UIControlState())!, comment: ""), for: UIControlState())
        self.mapTypeSwitch.setTitle(NSLocalizedString(self.mapTypeSwitch.titleForSegment(at: MapSwitchSegments.mapType.rawValue)!, comment: ""), forSegmentAt: MapSwitchSegments.mapType.rawValue)
        self.mapTypeSwitch.setTitle(NSLocalizedString(self.mapTypeSwitch.titleForSegment(at: MapSwitchSegments.satelliteType.rawValue)!, comment: ""), forSegmentAt: MapSwitchSegments.satelliteType.rawValue)
        self.mapTypeSwitch.selectedSegmentIndex = MapSwitchSegments.mapType.rawValue
        
        if !self.deleteButtonHidden {
            self.deleteButton.setTitle(NSLocalizedString(self.deleteButton.title(for: UIControlState())!, comment: ""), for: UIControlState())
            self.deleteButton.isHidden = false
        }
        
        if nil != self.locationManager {
            self.locationManager.stopUpdatingLocation()
            self.locationManager = nil
        }
    }
    
    /* ################################################################## */
    /**
     This establishes the initial state of the editable items.
     If no meeting is provided, we set a default meeting.
     */
    func setMeetingData() {
        self.newMeeting = false

        if nil == self.meeting {
            self.meeting = ["id_bigint":"0",
                            "service_body_bigint":"0",
                            "start_time":type(of: self).sDefaultStartTimeString,
                            "duration_time":self.getDurationValueAsStringFromInteger(BMLTAdminAppDelegate.v_appPrefs.defaultDuration),
                            "weekday_tinyint":type(of: self).sDefaultWeekdayString,
                            "published":"0",
                            "longitude":String(BMLTAdminAppDelegate.v_defaultLocation.longitude),
                            "latitude":String(BMLTAdminAppDelegate.v_defaultLocation.latitude),
                            "worldid_mixed":"",
                            "location_text":"",
                            "location_street":"",
                            "location_info":"",
                            "location_neighborhood":"",
                            "location_city_subsection":"",
                            "location_municipality":"",
                            "location_sub_province":"",
                            "location_province":"",
                            "location_postal_code_1":"",
                            "formats":"",
                            "comments":""
            ]
            
            if (nil != BMLTAdminConnectViewController.v_userServiceBodyIDs) && (0 < BMLTAdminConnectViewController.v_userServiceBodyIDs.count) {
                for sbID in BMLTAdminConnectViewController.v_userServiceBodyIDs {
                    if 0 < sbID {
                        self.meeting["service_body_bigint"] = String(sbID)
                        break
                    }
                }
            }
            self.compMeeting = self.meeting
        }
        
        // Set the weekday picker.
        if let weekdayString = self.meeting["weekday_tinyint"] {
            if let selectedWeekday = Int(weekdayString) {
                if (0 < selectedWeekday) && (8 > selectedWeekday) {
                    self.weekdayPickerView.selectRow(selectedWeekday - 1, inComponent: 0, animated: false)
                }
            }
        } else {
            self.weekdayPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        
        // Set the published checkbox.
        if let publishedString = self.meeting["published"] {
            if let published = Int(publishedString) {
                self.publishedCheckbox.checked = published != 0
            }
        } else {
            self.publishedCheckbox.checked = false
        }
        
        self.setTimeValueFromInteger( self.getTimeValueAsInteger(), inChangeTextItem: true )
        self.setDurationValueFromInteger( self.getDurationValueAsInteger(), inChangeTextItem: true )

        self.meetingNameEditableText.text = self.meeting["meeting_name"]
        self.worldIDEditText.text = self.meeting["worldid_mixed"]
        self.locationNameEditField.text = self.meeting["location_text"]
        self.streetAddressEditField.text = self.meeting["location_street"]
        self.extraInfoEditField.text = self.meeting["location_info"]
        self.neighborhoodEditField.text = self.meeting["location_neighborhood"]
        self.boroughEditField.text = self.meeting["location_city_subsection"]
        self.townEditField.text = self.meeting["location_municipality"]
        self.countyEditField.text = self.meeting["location_sub_province"]
        self.stateEditField.text = self.meeting["location_province"]
        self.zipEditField.text = self.meeting["location_postal_code_1"]
        self.commentTextView.text = self.meeting["comments"]
        
        self.meetingDirty = false
        self.locationMapView.removeAnnotations(self.locationMapView.annotations)
        self.addMeetingMarker(true)
        self.setPublishedText()
        self.checkSaveStatus()
        self.selectAppropriateBackground()
        self.hideSavingActivity()
    }
    
    /* ################################################################## */
    /**
     Checks the edited state. If the meeting is different, the "Save" button is activated.
     */
    func checkSaveStatus() {
        if (nil != self.meeting) && (0 < self.meeting.count) {
            self.meetingDirty = (self.meeting != self.compMeeting)
        }
        self.saveButton.isEnabled = self.meetingDirty
    }
    
    /* ################################################################## */
    /**
     If the meeting is unpublished, it gets an orange background.
     */
    func selectAppropriateBackground() {
        var backgroundImage: UIImage = UIImage(named: "EditBackground")!
        
        if meeting["published"] == "0" {
            backgroundImage = UIImage(named: "EditGradientUnpublished")!
        }
        
        self.backgroundGradientImageView.image = backgroundImage
    }
    
    /* ################################################################## */
    /**
     Returns the time from the meeting object as an integer (number of minutes).
     
     :returns: an Int, 0 to 1439
     */
    func getTimeValueAsInteger() -> Int {
        if nil != self.meeting["start_time"] {
            return BMLTAdminAppDelegate.parseTimeValueAsInteger ( self.meeting["start_time"] )
        } else {
            return 0
        }
    }
    
    /* ################################################################## */
    /**
     Returns the time from the start time text field as an integer (number of minutes).
     
     :returns: an Int, 0 to 1439
     */
    func getTimeValueFromTextAsInteger() -> Int {
        return BMLTAdminAppDelegate.parseTimeValueAsInteger ( self.startTimeEditField.text )
    }
    
    /* ################################################################## */
    /**
     Returns the time from the meeting object as an integer (number of minutes).
     
     :returns: an Int, 0 to 1439
     */
    func getDurationValueAsInteger() -> Int {
        if nil != self.meeting["duration_time"] {
            return BMLTAdminAppDelegate.parseTimeValueAsInteger ( self.meeting["duration_time"] )
        } else {
            return 0
        }
    }
    
    /* ################################################################## */
    /**
     Returns the time from the start time text field as an integer (number of minutes).
     
     :returns: an Int, 0 to 1439
     */
    func getDurationValueFromTextAsInteger() -> Int {
        return BMLTAdminAppDelegate.parseTimeValueAsInteger ( self.durationEditField.text )
    }
    
    /* ################################################################## */
    /**
     Sets the meeting time and the text item from an integer.
     
     :param: inValue an Int, 0 to 1439
     :param: inChangeTextItem If true, then we also change the text item.
     */
    func setTimeValueFromInteger( _ inValue: Int, inChangeTextItem: Bool ) {
        if (0 <= inValue) && (1440 > inValue) {
            if (nil != self.meeting) && (0 < self.meeting.count) {
                self.startTimeStepper.value = Double (inValue)
                
                let hours = inValue / 60
                let minutes = inValue - (hours * 60)
                
                var hrs: String = ""
                if 10 > hours {
                    hrs = "0"
                }
                
                hrs += String(hours)
                
                var min = ""
                if 10 > minutes {
                    min = "0" + min
                }
                
                min += String(minutes)
                
                self.meeting["start_time"] = hrs + ":" + min + ":00"
                
                if inChangeTextItem {
                    self.startTimeEditField.text = String(hours) + ":" + min
                }
            }
        }
    }
    
    /* ################################################################## */
    /**
     Sets the duration and the text item from an integer.
     
     :param: inValue an Int, 0 to 1439
     */
    func getDurationValueAsStringFromInteger( _ inValue: Int ) -> String {
        var ret: String = ""
        
        if (0 <= inValue) && (1440 > inValue) {
            let hours = inValue / 60
            let minutes = inValue - (hours * 60)
            
            var hrs: String = ""
            if 10 > hours {
                hrs = "0"
            }
            
            hrs += String(hours)
            
            var min = ""
            if 10 > minutes {
                min = "0" + min
            }
            
            min += String(minutes)
            
            ret = hrs + ":" + min + ":00"
        }
    
        return ret
    }
    
    /* ################################################################## */
    /**
     Sets the duration and the text item from an integer.
     
     :param: inValue an Int, 0 to 1439
     :param: inChangeTextItem If true, then we also change the text item.
     */
    func setDurationValueFromInteger( _ inValue: Int, inChangeTextItem: Bool ) {
        if (0 <= inValue) && (1440 > inValue) {
            if (nil != self.meeting) && (0 < self.meeting.count) {
                self.durationStepper.value = Double (inValue)
                
                let hours = inValue / 60
                let minutes = inValue - (hours * 60)
                
                var hrs: String = ""
                if 10 > hours {
                    hrs = "0"
                }
                
                hrs += String(hours)
                
                var min = ""
                if 10 > minutes {
                    min = "0" + min
                }
                
                min += String(minutes)
                
                self.meeting["duration_time"] = hrs + ":" + min + ":00"
                
                if inChangeTextItem {
                    self.durationEditField.text = String(hours) + ":" + min
                }
            }
        }
    }
    
    /* ################################################################## */
    /**
     Tells the object to add its meeting marker to the map.
     This also sets the map to be right at the meeting location.
     
     :param: inSetZoom If true, the map will force a zoom. Otherwise, the zoom will be unchanged.
     */
    func addMeetingMarker(_ inSetZoom: Bool) {
        let meeting: BMLTAdminMeeting = self.meeting
        if 0 < meeting.count { // Have to have valid data.
            let meetings: BMLTAdminMeetingList = [meeting]
            
            if let long = Float(meeting["longitude"]!) {
                if let lat = Float(meeting["latitude"]!) {
                    let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
                    
                    if nil != self.meetingMarker {
                        self.locationMapView.removeAnnotation(self.meetingMarker)
                    }
                    
                    // Now, zoom the map to just around the marker.
                    let currentRegion = self.locationMapView.region
                    
                    var span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: type(of: self).sMapSizeInDegrees, longitudeDelta: 0)
                    
                    if inSetZoom {
                        span = MKCoordinateSpan(latitudeDelta: type(of: self).sMapSizeInDegrees, longitudeDelta: 0)
                    } else {
                        span = currentRegion.span
                    }
                    
                    let newRegion: MKCoordinateRegion = MKCoordinateRegion(center: coordinate, span: span)
                    self.locationMapView.setRegion(newRegion, animated: false)
                    
                    self.meetingMarker = BMLTAdminAnnotation(coordinate: coordinate, meetings: meetings)
                    
                    // Set the marker up.
                    self.locationMapView.addAnnotation(self.meetingMarker)
                }
            }
        }
    }
    
    /* ################################################################## */
    /**
     :param: coordinate
     :param: inSetZoom If true, the map will force a zoom. Otherwise, the zoom will be unchanged.
     */
    func moveMeetingMarkerToLocation(_ coordinate: CLLocationCoordinate2D, inSetZoom: Bool) {
        self.meeting["longitude"] = coordinate.longString
        self.meeting["latitude"] = coordinate.latString
        
        self.addMeetingMarker(inSetZoom)
        self.checkSaveStatus()
    }
    
    /* ################################################################## */
    /**
     This makes sure that the label for the published checkbox is accurate to the state of the checkbox.
     */
    func setPublishedText() {
        var labelText = "LOCAL-MEETING-UNPUBLISHED-LABEL"
        
        if "1" == self.meeting["published"] {
            labelText = "LOCAL-MEETING-PUBLISHED-LABEL"
        }
        // The label for the published checkbox.
        self.publishedLabel.text = NSLocalizedString(labelText, comment: "")
    }
    
    /* ################################################################## */
    /**
     Generates the appropriate URI for the save operation.
     
     :returns: a String, with the URI.
     */
    func generateSaveURI() -> String {
        var ret: String = BMLTAdminAppDelegate.v_rootServerBaseURI + type(of: self).sPathPrefix
        ret += "?admin_action="
        
        self.newMeeting = self.meeting["id_bigint"] == "0"
        
        if self.newMeeting {
            ret += ("add_meeting&service_body_id=" + self.meeting["service_body_bigint"]!)
            self.meeting["published"] = "0"
        } else {
            ret += "modify_meeting&meeting_id=" + self.meeting["id_bigint"]!
        }
        
        for (key, value) in self.meeting {
            let ref = self.compMeeting[key]
            if (self.newMeeting && (key != "id_bigint")) || (!self.newMeeting && (value != ref))  {
                ret += "&meeting_field[]=" + key.URLEncodedString()! + "," + value.URLEncodedString()!
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This presents a confirmation dialog, asking if the user really wants to make the changes.
     
     If they say yes, the changes are made, and the edit screen is closed.
     
     If they say no, then they are returned to the editor.
     */
    func displaySaveConfirmAlert() {
        if nil != self.presentingViewController {
            self.presentingViewController?.dismiss(animated: false, completion: nil)
        }
        
        let title: String = NSLocalizedString("LOCAL-EDIT-SAVE-CONFIRM-TITLE", comment: "")
        let message: String = NSLocalizedString("LOCAL-EDIT-SAVE-MESSAGE-TITLE2", comment: "")
        let button1: UIAlertAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-OK-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: self.saveOKCallback)
        let button3: UIAlertAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-CANCEL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addAction(button1)
        
        alertController.addAction(button3)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /* ################################################################## */
    /**
     This presents a confirmation dialog, asking if the user wants to lose the changes they made.
     
     If they say yes, then the edit screen is closed, and the changes are not sent to the server.
     
     If they say no, then they are returned to the edit screen.
     */
    func displayCancelConfirmAlert() {
        if self.meetingDirty {
            if nil != self.presentingViewController {
                self.presentingViewController?.dismiss(animated: false, completion: nil)
            }
            
            let alertController = UIAlertController(title: NSLocalizedString("LOCAL-EDIT-CANCEL-CONFIRM-TITLE", comment: ""), message: NSLocalizedString("LOCAL-EDIT-CANCEL-MESSAGE-TITLE", comment: ""), preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-LOSE-CHANGES-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: self.cancelOKCallback)
            
            alertController.addAction(okAction)
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-CANCEL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        } else {
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    /* ################################################################## */
    /**
     Hides the editable items container, and displays the busy indicator.
     */
    func displaySavingActivity() {
        self.editableItemsScroller.isHidden = true
        self.savingLabel.isHidden = false
        self.savingActivityIndicator.isHidden = false
    }

    /* ################################################################## */
    /**
     Hides the busy indicator, and shows the editable items container.
     */
    func hideSavingActivity() {
        self.editableItemsScroller.isHidden = false
        self.savingLabel.isHidden = true
        self.savingActivityIndicator.isHidden = true
    }
    
    // MARK: - Callback Methods -
    /* ################################################################## */
    /**
     If the user wants to save the changes, we generate a change URI, and send it to the server.
     
     :param: inAction The alert action object (ignored)
     */
    func saveOKCallback(_ inAction: UIAlertAction) {
        self.displaySavingActivity()
        _ = BMLTAdminCommunicator (self.generateSaveURI(), dataSource: BMLTAdminAppDelegate.appDelegate().connectionSession, delegate: self, refCon: nil)
    }
    
    /* ################################################################## */
    /**
     If the user wants to lose the changes, the editor is closed with no other action.
     
     :param: inAction The alert action object (ignored)
     */
    func cancelOKCallback(_ inAction: UIAlertAction) {
        self.hideSavingActivity()
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    /* ################################################################## */
    /**
     Called after the address geocode is done.
     
     :param: placeMarks
     :param: error
     */
    func gecodeCompletionHandler (_ placeMarks: [CLPlacemark]?, error: Error?) {
        DispatchQueue.main.async(execute: {
            self.geocoder = nil
            if (nil != error) || (nil == placeMarks) || (0 == placeMarks!.count) {
                BMLTAdminAppDelegate.displayErrorAlert("LOCAL-EDIT-GEOCODE-FAIL-TITLE", inMessage: "LOCAL-EDIT-GEOCODE-FAILURE-MESSAGE")
            } else {
                if let location = placeMarks![0].location {
                    self.moveMeetingMarkerToLocation(location.coordinate, inSetZoom: false)
                }
            }
        })
    }
    
    /* ################################################################## */
    /**
     Called after the long/lat is reverse geocoded.
     
     :param: placeMarks
     :param: error
     */
    func reverseGecodeCompletionHandler (_ placeMarks: [CLPlacemark]?, error: Error?) {
        self.geocoder = nil
        if (nil != error) || (nil == placeMarks) || (0 == placeMarks!.count) {
            BMLTAdminAppDelegate.displayErrorAlert("LOCAL-EDIT-REVERSE-GEOCODE-FAIL-TITLE", inMessage: "LOCAL-EDIT-REVERSE-GEOCODE-FAILURE-MESSAGE")
        } else {
            let placeMark = placeMarks![0]
            if let location = placeMark.location {
                let coordinate = location.coordinate
                let locationName = placeMark.name
                let streetNumber = placeMark.subThoroughfare
                let streetName = placeMark.thoroughfare
                let borough = placeMark.subLocality
                let town = placeMark.locality
                let county = placeMark.subAdministrativeArea
                let state = placeMark.administrativeArea
                let zip = placeMark.postalCode
                
                DispatchQueue.main.async(execute: {
                        if (nil != streetNumber) && !(streetNumber?.isEmpty)! && (nil != streetName) && !(streetName?.isEmpty)! {
                            self.streetAddressEditField.text = streetNumber! + " " + streetName!
                        } else {
                            if (nil != streetName) && !(streetName?.isEmpty)! {
                                self.streetAddressEditField.text = streetName!
                            } else {
                                self.streetAddressEditField.text = ""
                            }
                        }
                        
                        // We don't want simple copies of the street address here.
                        if (nil != locationName) && !(locationName?.isEmpty)! {
                            if locationName != self.streetAddressEditField.text {
                                self.locationNameEditField.text = locationName
                            } else {
                                self.locationNameEditField.text = ""
                            }
                        } else {
                            self.locationNameEditField.text = ""
                        }
                        
                        if (nil != borough) && !(borough?.isEmpty)! {
                            self.neighborhoodEditField.text = borough
                            self.boroughEditField.text = borough
                        } else {
                            self.neighborhoodEditField.text = borough
                            self.boroughEditField.text = ""
                        }
                        
                        if (nil != town) && !(town?.isEmpty)! {
                            self.townEditField.text = town
                        } else {
                            self.townEditField.text = ""
                        }
                        
                        if (nil != county) && !(county?.isEmpty)! {
                            self.countyEditField.text = county
                        } else {
                            self.countyEditField.text = ""
                        }
                        
                        if (nil != state) && !(state?.isEmpty)! {
                            self.stateEditField.text = state
                        } else {
                            self.stateEditField.text = ""
                        }
                        
                        if (nil != zip) && !(zip?.isEmpty)! {
                            self.zipEditField.text = zip
                        } else {
                            self.zipEditField.text = ""
                        }
                    
                        self.extraInfoEditField.text = ""
                    
                        self.textFieldChanged(self.locationNameEditField)
                        self.textFieldChanged(self.streetAddressEditField)
                        self.textFieldChanged(self.extraInfoEditField)
                        self.textFieldChanged(self.neighborhoodEditField)
                        self.textFieldChanged(self.boroughEditField)
                        self.textFieldChanged(self.townEditField)
                        self.textFieldChanged(self.countyEditField)
                        self.textFieldChanged(self.stateEditField)
                        self.textFieldChanged(self.zipEditField)
                        
                        self.moveMeetingMarkerToLocation(coordinate, inSetZoom: false)
                    })
            }
        }
    }
    
    // MARK: - CLLocationManagerDelegate Methods -
    /* ################################################################## */
    /**
     Called if there was a failure with the location manager.
     
     :param: manager The Location Manager object that had the error.
     :param: error The error in question.
     */
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.locationManager.stopUpdatingLocation()
        self.locationManager = nil
        BMLTAdminAppDelegate.displayErrorAlert("LOCAL-EDIT-LOCATION-FAIL-TITLE", inMessage: "LOCAL-EDIT-LOCATION-FAILURE-MESSAGE")
    }
    
    /* ################################################################## */
    /**
     Called when the location manager updates the locations.
     
     :param: manager The Location Manager object that had the event.
     :param: locations an array of updated locations.
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        self.locationManager = nil
        if 0 < locations.count {
            let newLocation = locations[0]
            self.geocoder = CLGeocoder()
            self.geocoder.reverseGeocodeLocation(newLocation, completionHandler: self.reverseGecodeCompletionHandler )
        }
    }
    
    // MARK: - Base Class Override Methods -
    /* ################################################################## */
    /**
     Called when the view will appear.
     
     :param: animated True, if the appearance is animated.
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.formatTableView.reloadData()
        self.serviceBodyPickerView.reloadAllComponents()
    }
    
    /* ################################################################## */
    /**
     Called when the view will disappear.
     
     :param: animated True, if the disappearance is animated.
     */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if nil != self.locationManager {
            self.locationManager.stopUpdatingLocation()
            self.locationManager.delegate = nil
            self.locationManager = nil
        }
        
        if nil != self.geocoder {
            self.geocoder.cancelGeocode()
            self.geocoder = nil
        }
    }
    
    /* ################################################################## */
    /**
     Called when the view first loads.
     
     We use this to instantiate the editor contents.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register for keyboard show/hide events.
        NotificationCenter.default.addObserver(self, selector: #selector(type(of: self).keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(type(of: self).keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        var formatArray: [[String:String]] = []
        
        for format in BMLTAdminAppDelegate.v_formats {
            formatArray.append(format.1)
        }
        
        self.sortedFormats = formatArray.sorted {$0["key_string"] < $1["key_string"]}
        
        self.editorViewLoad()
        self.setMeetingData()
    }
    
    /* ################################################################## */
    /**
     Called when the view is about to lay out the subviews.
     
     We use this to make sure that the editor content view is laid out properly.
     */
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        // We set a fixed table height for the format table.
        self.formatTableView.rowHeight = CGFloat(type(of: self).sFormatCheckboxContainerHeight)
        self.numFormatCheckboxesPerRow = 0
    }
    
    /* ################################################################## */
    /**
     Called when the view has finished laying out the subviews.
     
     We use this to make sure that the editor content view is sized properly.
     */
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let serviceBodyIDs = BMLTAdminConnectViewController.v_userServiceBodyIDs
        
        // If we can only manage one Service body, then the picker has no meaning. Otherwise, show a picker that allows the Service body to be changed.
        if (nil != serviceBodyIDs) && (1 < serviceBodyIDs?.count) {
            self.serviceBodyLabel.text = NSLocalizedString(self.serviceBodyLabel.text!, comment: "")
            self.serviceBodyContainerView.isHidden = false
            self.serviceBodyShimConstraint.constant = self.serviceBodyContainerView.bounds.size.height
            
            // Select the appropriate Service body
            if let meetingServiceBodyID = self.meeting["service_body_bigint"] {
                if let intSBID = Int(meetingServiceBodyID) {
                    let index = BMLTAdminAppDelegate.getSelectedServiceBodyIndexFromID(intSBID)
                    self.serviceBodyPickerView.selectRow(index, inComponent: 0, animated: false)
                }
            }
        } else {
            self.serviceBodyShimConstraint.constant = 0
            self.serviceBodyContainerView.isHidden = true
        }
        
        // Calculate how many format cells we can fit in a table row.
        let tableBounds = self.view.bounds
        self.numFormatCheckboxesPerRow = Int(floor(tableBounds.size.width / CGFloat(type(of: self).sFormatCheckboxContainerWidth)))
        self.formatTableHeightConstraint.constant = CGFloat(self.tableView(self.formatTableView, numberOfRowsInSection: 0)) * self.formatTableView.rowHeight
        self.formatTableView.frame.size.height = self.formatTableHeightConstraint.constant
        
        var size = CGSize.zero
        // The width will be restricted to the width of the window.
        size.width = self.view.bounds.size.width
        
        // We set the bottom to the bottom of the furthest down subview.
        for subView in self.editorView.subviews {
            if !subView.isHidden {    // We only count visible subviews.
                let subviewBottom = subView.frame.origin.y + subView.frame.size.height
                if subviewBottom > size.height {
                    size.height = subviewBottom
                }
            }
        }
        
        // Add a scosche of padding at the bottom.
        size.height += type(of: self).sScreenPadding
        
        self.editorView.frame = CGRect (origin: CGPoint.zero, size: size)
        self.editableItemsScroller.contentSize = size
        self.formatTableView.reloadData()
    }
    
    // MARK: - IB Handler Methods -
    /* ################################################################## */
    /**
     Called when someone hits the "Set Map to Address" button.
     
     :param: sender The button item.
     */
    @IBAction func setMapToAddressButtonHit(_ sender: UIButton) {
        self.closeKeyboard(sender)
        self.geocoder = CLGeocoder()
        var addressString : String = ""
        
        // We build the address by adding components as we get to them.
        if let street = self.streetAddressEditField.text {
            addressString = street
        }
        
        if let borough = self.boroughEditField.text {
            if !borough.isEmpty {
                if !addressString.isEmpty {
                    addressString += ", "
                }
                
                addressString += borough
            }
        }
        
        if let town = self.townEditField.text {
            if !town.isEmpty {
                if !addressString.isEmpty {
                    addressString += ", "
                }
                
                addressString += town
            }
        }
        
        if let state = self.stateEditField.text {
            if !state.isEmpty {
                if !addressString.isEmpty {
                    addressString += ", "
                }
                
                addressString += state
            }
        }
        
        if let zip = self.zipEditField.text {
            if !zip.isEmpty {
                if !addressString.isEmpty {
                    addressString += ", "
                }
                
                addressString += zip
            }
        }
        
        self.geocoder.geocodeAddressString(addressString, completionHandler: self.gecodeCompletionHandler )
    }
    
    /* ################################################################## */
    /**
     Called when someone hits the "Set Address to Location" button.
     
     :param: sender The button item.
     */
    @IBAction func setAddressToLocationButtonHit(_ sender: UIButton) {
        self.closeKeyboard(sender)
        // Ask permission to track the user's location.
        self.locationManager = CLLocationManager()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
    
    /* ################################################################## */
    /**
     Called when someone hits the "published" checkbox.
     
     :param: sender The checkbox item.
     */
    @IBAction func publishedCheckboxSelected(_ sender: BMLTAdminCheckbox) {
        self.closeKeyboard(sender)
        if sender.checked {
            self.meeting["published"] = "1"
        } else {
            self.meeting["published"] = "0"
        }
        
        self.checkSaveStatus()
        self.setPublishedText()
        self.selectAppropriateBackground()
    }
    
    /* ################################################################## */
    /**
     Called when someone hits the "Cancel" Navbar item.
     
     :param: sender The UIBarButtonItem for the "Cancel" button
     */
    @IBAction func cancelButtonHit(_ sender: UIBarButtonItem) {
        self.newMeeting = false
        self.closeKeyboard(sender)
        self.displayCancelConfirmAlert()
    }
    
    /* ################################################################## */
    /**
     Called when someone hits the "Save" Navbar item.
     
     :param: sender The UIBarButtonItem for the "Save" button
     */
    @IBAction func saveButtonHit(_ sender: UIBarButtonItem) {
        self.newMeeting = false
        self.closeKeyboard(sender)
        self.displaySaveConfirmAlert()
    }

    /* ################################################################## */
    /**
     Called when editing completes in a text item.
     
     :param: sender The UITextField Affected
     */
    @IBAction func textEditingEnded(_ sender: UITextField) {
        switch(sender) {
        case self.meetingNameEditableText:
            sender.text = self.meeting["meeting_name"]
            
        case self.startTimeEditField:
            self.setTimeValueFromInteger(self.getTimeValueFromTextAsInteger(), inChangeTextItem: true)
            
        case self.durationEditField:
            self.setDurationValueFromInteger(self.getDurationValueFromTextAsInteger(), inChangeTextItem: true)
            
        case self.worldIDEditText:
            sender.text = self.meeting["worldid_mixed"]
            
        case self.locationNameEditField:
            sender.text = self.meeting["location_text"]
            
        case self.streetAddressEditField:
            sender.text = self.meeting["location_street"]
            
        case self.extraInfoEditField:
            sender.text = self.meeting["location_info"]
            
        case self.neighborhoodEditField:
            sender.text = self.meeting["location_neighborhood"]
            
        case self.boroughEditField:
            sender.text = self.meeting["location_city_subsection"]
            
        case self.townEditField:
            sender.text = self.meeting["location_municipality"]
            
        case self.countyEditField:
            sender.text = self.meeting["location_sub_province"]
            
        case self.stateEditField:
            sender.text = self.meeting["location_province"]
            
        case self.zipEditField:
            sender.text = self.meeting["location_postal_code_1"]

        default:
            print("Unknown TextField: \(sender)")
        }
    }
    
    /* ################################################################## */
    /**
     Called when someone changes text in a text item.
     
     :param: sender The UITextField Affected
     */
    @IBAction func textFieldChanged(_ sender: UITextField) {
        switch(sender) {
        case self.meetingNameEditableText:
            self.meeting["meeting_name"] = sender.text
            
        case self.startTimeEditField:
            // We update the start time value without touching the text field.
            self.setTimeValueFromInteger(self.getTimeValueFromTextAsInteger(), inChangeTextItem: false)
            
        case self.durationEditField:
            // We update the duration value without touching the text field.
            self.setDurationValueFromInteger(self.getDurationValueFromTextAsInteger(), inChangeTextItem: false)
            
        case self.worldIDEditText:
            self.meeting["worldid_mixed"] = sender.text
            
        case self.locationNameEditField:
            self.meeting["location_text"] = sender.text
            
        case self.streetAddressEditField:
            self.meeting["location_street"] = sender.text
            
        case self.extraInfoEditField:
            self.meeting["location_info"] = sender.text
            
        case self.neighborhoodEditField:
            self.meeting["location_neighborhood"] = sender.text
            
        case self.boroughEditField:
            self.meeting["location_city_subsection"] = sender.text
            
        case self.townEditField:
            self.meeting["location_municipality"] = sender.text
            
        case self.countyEditField:
            self.meeting["location_sub_province"] = sender.text
            
        case self.stateEditField:
            self.meeting["location_province"] = sender.text
            
        case self.zipEditField:
            self.meeting["location_postal_code_1"] = sender.text
            
        default:
            print("Unknown TextField: \(sender)")
        }
        
        self.checkSaveStatus()
    }
    
    /* ################################################################## */
    /**
     Called when the start time stepper changes.
     
     :param: sender The Stepper object Affected
     */
    @IBAction func startTimeStepperChanged(_ sender: UIStepper) {
        self.closeKeyboard(sender)
        // This makes sure that it is always a multiple of 5.
        let remainder = sender.value.truncatingRemainder(dividingBy: 5)
        if (0 != remainder) && (0 < sender.value) {
            sender.value -= remainder
        }
        // We update the start time value and the edit field.
        self.setTimeValueFromInteger( Int(sender.value), inChangeTextItem: true )
        
        self.checkSaveStatus()
    }
    
    /* ################################################################## */
    /**
     Called when the duration stepper changes.
     
     :param: sender The Stepper object Affected
     */
    @IBAction func durationStepperChanged(_ sender: UIStepper) {
        self.closeKeyboard(sender)
        // This makes sure that it is always a multiple of 5.
        let remainder = sender.value.truncatingRemainder(dividingBy: 5)
        if (0 != remainder) && (0 < sender.value) {
            sender.value -= remainder
        }
        // We update the start time value and the edit field.
        self.setDurationValueFromInteger( Int(sender.value), inChangeTextItem: true )
        
        self.checkSaveStatus()
    }
    
    /* ################################################################## */
    /**
     Called when the user taps on the map. Moves the marker to the tap location.
     
     :param: sender The gesture recognizer.
     */
    @IBAction func mapTapped(_ sender: UITapGestureRecognizer) {
        self.closeKeyboard(sender)
        let point:CGPoint = sender.location(in: self.locationMapView)
        let locPoint:CLLocationCoordinate2D = self.locationMapView.convert(point, toCoordinateFrom: self.locationMapView)
        self.moveMeetingMarkerToLocation(locPoint, inSetZoom: false)
    }
    
    /* ################################################################## */
    /**
     Called when the user double-taps on the map. Zooms in the map.
     
     :param: sender The gesture recognizer.
     */
    @IBAction func mapDoubleTapped(_ sender: UITapGestureRecognizer) {
        self.closeKeyboard(sender)
        let point:CGPoint = sender.location(in: self.locationMapView)
        let locPoint:CLLocationCoordinate2D = self.locationMapView.convert(point, toCoordinateFrom: self.locationMapView)
        let oldRegion: MKCoordinateRegion = self.locationMapView.region
        var newRegion: MKCoordinateRegion = oldRegion
        newRegion.span.latitudeDelta /= 2.0
        newRegion.span.longitudeDelta /= 2.0
        newRegion.center = locPoint
        self.locationMapView.region = newRegion
    }
    
    /* ################################################################## */
    /**
     Called when the user changes the map type.
     
     :param: sender The map type segmented switch.
     */
    @IBAction func mapSwitchChanged(_ sender: UISegmentedControl) {
        self.closeKeyboard(sender)
        switch sender.selectedSegmentIndex {
        case MapSwitchSegments.mapType.rawValue:
            self.locationMapView.mapType = MKMapType.standard
        case MapSwitchSegments.satelliteType.rawValue:
            self.locationMapView.mapType = MKMapType.hybridFlyover
        default:
            self.locationMapView.mapType = MKMapType.standard
        }
    }
    
    // MARK: - Notification Callbacks -
    /* ################################################################## */
    /**
     Called when the keyboard has been displayed.
     
     We use this to move the bottom of the editable items scroll area to the top of the keyboard.
     We also scroll in the active item, if necessary.
     
     :param: aNotification The notification object for this event.
     */
    @objc func keyboardWasShown(_ aNotification:Notification) {
        if let info = (aNotification as NSNotification).userInfo! as NSDictionary! {
            if let infoObject = info.object(forKey: UIKeyboardFrameBeginUserInfoKey) {
                let kbSize: CGSize = (infoObject as AnyObject).cgRectValue.size
                // Get the top of the keyboard. Also account for the top of the Editable Items Scroll Area.
                let frame = self.editableItemsScroller.frame
                let kbHeight = kbSize.height + frame.origin.y
                // This is how we shorten the Editable Items Scroll Area temporarily.
                self.bottomLayoutOfScrollArea.constant = kbSize.height - frame.origin.y

                // See if we need to scroll in the active item.
                var aRect: CGRect = self.view.bounds
                aRect.size.height -= kbHeight
                if let activeField = self.findFirstResponder() as! UIView! {
                    let activeFieldBottomRight = CGPoint(x: activeField.frame.origin.x + activeField.frame.size.width, y: activeField.frame.origin.y + activeField.frame.size.height)
                    let scrollOffsetY = aRect.size.height - activeFieldBottomRight.y
                    if scrollOffsetY < 0 {
                        self.editableItemsScroller.contentOffset.y = -scrollOffsetY
                    }
                }
            }
        }
    }
    
    /* ################################################################## */
    /**
     Called when the keyboard is dismissed.
     
     We use this to reset the bottom of the editable items scroll area.
     
     :param: aNotification The notification object for this event (ignored).
     */
    @objc func keyboardWillBeHidden(_ aNotification:Notification) {
        self.bottomLayoutOfScrollArea.constant = 0
    }
    
    // MARK: - Custom Action Handlers -
    /* ################################################################## */
    /**
     Called when the user selects a format checkbox.
     
     :param: sender The format checkbox.
     */
    @objc func formatCheckboxChanged(_ sender: BMLTAdminFormatCheckbox) {
        self.closeKeyboard(sender)
        if let baselineMeeting = self.compMeeting {
            if let originalFormatsString = self.meeting["formats"] {
                let originalFormats = originalFormatsString.components(separatedBy: ",")
                var newFormats = originalFormats
                if let currentFormat = sender.formatObject["key_string"] {
                    if !currentFormat.isEmpty {
                        if originalFormats.contains(currentFormat) {
                            if(!sender.checked) {
                                newFormats = []
                                for format in originalFormats {
                                    if format != currentFormat {
                                        newFormats.append(format)
                                    }
                                }
                            }
                        } else {
                            if(sender.checked) {
                                newFormats.append(currentFormat)
                            }
                        }
                    }
                }
                
                // The reason for this strange little dance, is that we want to exactly match the order in the original formats, so we can leave the meeting "as is," if we revert.
                if let baseLineFormatsString = baselineMeeting["formats"] {
                    let baselineFormatsArray = baseLineFormatsString.components(separatedBy: ",")
                    var tempFormats:[String] = []
                    for format in baselineFormatsArray {
                        if !format.isEmpty && newFormats.contains(format) {
                            tempFormats.append(format)
                        }
                    }
                    
                    for format in newFormats {
                        if !format.isEmpty && !tempFormats.contains(format) {
                            tempFormats.append(format)
                        }
                    }

                    newFormats = tempFormats
                }
                
                let newString = newFormats.joined(separator: ",")
                self.meeting["formats"] = newString
                self.checkSaveStatus()
            }
        }
    }
    
    // MARK: - UITextViewDelegate Protocol Methods -
    /* ################################################################## */
    /**
     :param: textView The text view that experienced the text change.
     */
    func textViewDidChange(_ textView: UITextView) {
        switch textView {
        case self.commentTextView:
            self.meeting["comments"] = textView.text
            self.checkSaveStatus()
        default:
            print("Unknown Text View: \(textView)")
        }
    }
    
    // MARK: - UITableViewDataSource Protocol Methods -
    /* ################################################################## */
    /**
     This returns the number of Meetings we'll need to display.
     
     :param: tableView The Table View
     :param: section The 0-based index of the section.
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (0 < self.numFormatCheckboxesPerRow) && (0 < BMLTAdminAppDelegate.v_formats.count) {
            return Int(ceil(CGFloat(BMLTAdminAppDelegate.v_formats.count) / CGFloat(self.numFormatCheckboxesPerRow)))
        } else {
            return 0
        }
    }

    /* ################################################################## */
    /**
     This returns one cell for a Meeting.
     
     :param: tableView The Table View
     :param: indexPath The index path to the cell we need.
     
     :returns:   one cell object for the given Meeting
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ret: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: String((indexPath as NSIndexPath).row))
        
        if nil == ret {
            ret = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: String((indexPath as NSIndexPath).row))
            ret.bounds = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.rowHeight)
            ret.backgroundColor = UIColor.clear
            ret.isUserInteractionEnabled = true
        } else {
            for subView in ret.subviews {
                subView.removeFromSuperview()
            }
        }
        
        let index_start = (indexPath as NSIndexPath).row * self.numFormatCheckboxesPerRow
        let index_end = min((index_start + self.numFormatCheckboxesPerRow), BMLTAdminAppDelegate.v_formats.count)
        
        var left: CGFloat = 0
        let meetingFormats = self.meeting["formats"]?.components(separatedBy: ",")
        for index in index_start..<index_end {
            let format: [String:String] = self.sortedFormats[index]
            var frame = CGRect(x: left, y: 0, width: CGFloat(type(of: self).sFormatCheckboxContainerWidth), height: CGFloat(type(of: self).sFormatCheckboxContainerHeight))
            let container = UIView(frame: frame)
            container.isUserInteractionEnabled = true
            container.backgroundColor = UIColor.clear
            frame.origin = CGPoint.zero
            frame.size.height = type(of: self).sFormatCheckboxContainerHeight
            frame.size.width = type(of: self).sFormatCheckboxContainerHeight
            frame = frame.insetBy(dx: type(of: self).sFormatCheckboxIndent, dy: type(of: self).sFormatCheckboxIndent)
            let checkbox = BMLTAdminFormatCheckbox(frame: frame)
            checkbox.checked = meetingFormats!.contains(format["key_string"]!)
            checkbox.formatObject = format
            checkbox.addTarget(self, action: #selector(type(of: self).formatCheckboxChanged), for: UIControlEvents.valueChanged)
            container.addSubview(checkbox)
            frame = container.bounds
            frame.origin.x = CGFloat(type(of: self).sFormatCheckboxContainerHeight)
            frame.size.width -= frame.origin.x
            let label:UILabel = UILabel(frame: frame)
            label.text = format["key_string"]
            label.backgroundColor = UIColor.clear
            label.textColor = UIColor.black
            container.addSubview(label)
            ret.addSubview(container)
            left += container.bounds.size.width
        }
        
        return ret
    }
    
    // MARK: - UIPickerViewDataSource Methods -
    /* ################################################################## */
    /**
     We only have 1 component.
     
     :param: pickerView The UIPickerView being checked
     
     :returns: 1
     */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    /* ################################################################## */
    /**
     Returns the number of weekdays
     
     :param: pickerView The UIPickerView being checked
     
     :returns: 7 (we assume).
     */
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.weekdayPickerView {
            return type(of: self).sWeekdays.count
        } else {
            return BMLTAdminConnectViewController.v_userServiceBodyIDs.count
        }
    }
    
    // MARK: - UIPickerViewDelegate Methods -
    /* ################################################################## */
    /**
     This returns the name for the given row.
     
     :param: pickerView The UIPickerView being checked
     :param: row The row being checked
     :param: component The component (always 0)
     
     :returns: a view, containing a label with the string for the row.
     */
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let size = pickerView.rowSize(forComponent: 0)
        var frame = pickerView.bounds
        frame.size.height = size.height
        frame.origin = CGPoint.zero
        
        let ret:UIView = UIView(frame: frame)
        
        ret.backgroundColor = UIColor.clear
        
        let label = UILabel(frame: frame)
        
        label.backgroundColor = UIColor.clear

        var string: String = ""
        
        if pickerView == self.weekdayPickerView {
            string = type(of: self).sWeekdays[row]
        } else {
            if let serviceBody = BMLTAdminAppDelegate.getSelectedServiceBodyObjectFromIndex(row) {
                string = serviceBody["name"]!
            }
        }
        
        label.text = string
        
        ret.addSubview(label)
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This is called when the user finishes selecting a row.
     We use this to set the weekday.
     
     :param: pickerView The UIPickerView being checked
     :param: row The row being checked
     :param: component The component (always 0)
     */
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.closeKeyboard(pickerView)
        if pickerView == self.weekdayPickerView {
            self.meeting["weekday_tinyint"] = String(row + 1)
        } else {
            let sbID = BMLTAdminAppDelegate.getSelectedServiceBodyIDFromIndex(row)
            self.meeting["service_body_bigint"] = String(sbID)
        }
        
        self.checkSaveStatus()
    }
    
    // MARK: - UITextFieldDelegate Methods -
    /* ################################################################## */
    /**
     Called when the "DONE" button is hit.
     
     :param: textField The Text Entry Field that triggered this.
     
     :returns: a Bool Always false (intercepts the event).
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()  // Close the keyboard. We don't need to do anything else.
        return false    // Intercept the event.
    }
    
    // MARK: - MKMapViewDelegate Methods -
    /* ################################################################## */
    /**
     */
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: BMLTAdminAnnotation.self) {
            let reuseID = self.meeting["id_bigint"]!
            let myAnnotation = annotation as! BMLTAdminAnnotation
            return BMLTAdminMarker(annotation: myAnnotation, draggable: true, reuseID: reuseID)
        }
        
        return nil
    }

    // MARK: - NSCoding Methods -
    /* ################################################################## */
    /**
     Called to save the current data out.
     
     We save the meeting.
     
     :param: aCoder The coder that will receive the data.
     */
    @objc override func encode(with aCoder: NSCoder) {
        aCoder.encode(self.meeting, forKey: self.sMeetingStorageKey)
    }
    
    /* ################################################################## */
    /**
     Initializer from coder.
     
     :param: aDecoder The coder that contains the data to be read out.
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let tempDecoded = aDecoder.decodeObject(forKey: self.sMeetingStorageKey) as! BMLTAdminMeeting! {
            self.meeting = tempDecoded
        } else {
            self.meeting = [:]
        }
    }
}
