//
//  BMLTAdminEditorViewController.swift
//  BMLTAdmin
/**
 :license:
 Created by MAGSHARE.
 This is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 BMLT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this code.  If not, see <http://www.gnu.org/licenses/>.
 */

import UIKit
import MapKit

// MARK: - Classes -
/* ###################################################################################################################################### */
/**
 This handles the display of the main editor screen.
 The editor has a scrolling area with a large set of editing tools.
 There are two buttons in the navbar: Cancel and Save. Save is not active unless there has been a change in the meeting.
 */
class BMLTAdminEditorViewController: BMLTAdminEditorBaseViewController {
    var _historyData: [[String:String]]! = nil
    
    var historyData:[[String:String]]! {
        set {
            self._historyData = newValue
            if let navController = self.navigationController {
                if let myParent = navController.viewControllers[0] as? BMLTAdminSearchViewController {
                    let id = Int(self.meeting["id_bigint"]!)!
                    myParent.meetingHistoryData[id] = self._historyData
                }
            }
        }
        
        get {
            return self._historyData
        }
    }
    
    // This is a semaphore that is set when the delete button was done. It tells the callback to simply close the screen.
    var meetingWasDeleted: Bool             = false

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    /** This is the History item. When the user clicks this, a history sheet will pop in. */
    @IBOutlet weak var historyButton: UIBarButtonItem!
    
    /** The labels that display the BMLT ID pf the meeting (non-editable). */
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var idValue: UILabel!
    
    // MARK: - Instance Methods -
    /* ################################################################## */
    /**
     Checks the edited state. If the meeting is different, the "Save" button is activated.
     */
    override func checkSaveStatus() {
        super.checkSaveStatus()
        self.saveButton.isEnabled = true
    }
    
    
    /* ################################################################## */
    /**
     Generates the appropriate URI for the delete operation.
     
     :returns: a String, with the URI.
     */
    func generateDeleteURI() -> String {
        var ret: String = BMLTAdminAppDelegate.v_rootServerBaseURI + type(of: self).sPathPrefix
        ret += "?admin_action=delete_meeting&meeting_id=" + self.meeting["id_bigint"]!
        return ret
    }
    
    /* ################################################################## */
    /**
     This presents a confirmation dialog, asking if the user really wants to make the changes.
     
     If they say yes, the changes are made, and the edit screen is closed.
     
     If they say no, then they are returned to the editor.
     */
    override func displaySaveConfirmAlert() {
        if nil != self.presentingViewController {
            self.presentingViewController?.dismiss(animated: false, completion: nil)
        }
        
        let title: String = NSLocalizedString("LOCAL-EDIT-SAVE-CONFIRM-TITLE", comment: "")
        let message1: String = NSLocalizedString("LOCAL-EDIT-SAVE-MESSAGE-TITLE1", comment: "")
        let message2: String = NSLocalizedString("LOCAL-EDIT-SAVE-MESSAGE-TITLE2", comment: "")
        let button1: UIAlertAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-OK-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: self.saveOKCallback)
        let button2: UIAlertAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-OK-COPY-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: self.saveOKCopyCallback)
        let button3: UIAlertAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-CANCEL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        
        var message: String = message1
        
        if self.meetingDirty {
            message = message2 + "\n\n" + message1
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if self.meetingDirty {
            alertController.addAction(button1)
        }
        
        alertController.addAction(button2)
        alertController.addAction(button3)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /* ################################################################## */
    /**
     If the user wants to save the meeting as a copy, we generate a change URI, and send it to the server.
     
     :param: inAction The alert action object (ignored)
     */
    func saveOKCopyCallback(_ inAction: UIAlertAction) {
        self.displaySavingActivity()
        self.meeting["id_bigint"] = "0"
        self.meeting["published"] = "0"
        self.newMeeting = true
        _ = BMLTAdminCommunicator (self.generateSaveURI(), dataSource: BMLTAdminAppDelegate.appDelegate().connectionSession, delegate: self, refCon: nil)
    }
    
    /* ################################################################## */
    /**
     This presents a confirmation dialog, asking if the user really wants to delete the meeting.
     
     If they say yes, the meeting is deleted, and the edit screen is closed.
     
     If they say no, then they are returned to the editor.
     */
    func displayDeleteConfirmAlert() {
        if nil != self.presentingViewController {
            self.presentingViewController?.dismiss(animated: false, completion: nil)
        }
        
        let alertController = UIAlertController(title: NSLocalizedString("LOCAL-EDIT-DELETE-CONFIRM-TITLE", comment: ""), message: NSLocalizedString("LOCAL-EDIT-DELETE-MESSAGE-TITLE", comment: ""), preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-DELETE-FINAL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: self.deleteOKCallback)
        
        alertController.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-CANCEL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /* ################################################################## */
    /**
     Hides the editable items container, and displays the busy indicator.
     */
    func displayDeletingActivity() {
        self.editableItemsScroller.isHidden = true
        self.savingLabel.isHidden = false
        self.savingLabel.text = NSLocalizedString("LOCAL-EDIT-DELETING-LABEL", comment: "")
        self.savingActivityIndicator.isHidden = false
    }
    
    // MARK: - Callback Methods -
    /* ################################################################## */
    /**
     If the user wants to delete the meeting, we generate a delete URI, and send it to the server.
     
     :param: inAction The alert action object (ignored)
     */
    func deleteOKCallback(_ inAction: UIAlertAction) {
        self.displayDeletingActivity()
        self.meetingWasDeleted = true
        _ = BMLTAdminCommunicator (self.generateDeleteURI(), dataSource: BMLTAdminAppDelegate.appDelegate().connectionSession, delegate: self, refCon: nil)
    }
    
    // MARK: - BMLTAdminCommunicatorDataSinkProtocol Methods -
    /* ################################################################## */
    /**
     The set changes response callback.
     
     :param: inHandler The handler for this call.
     :param: inResponseData The data from the URL request. If nil, the call failed to produce. Check the handler's error data member.
     :param: inError Any errors that occurred
     :param: inRefCon The data/object passed in via the 'refCon' parameter in the initializer.
     */
    override func responseData(_ inHandler: BMLTAdminCommunicator?, inResponseData: Data?, inError: Error?, inRefCon: AnyObject?) {
        DispatchQueue.main.async(execute: {
            self.hideSavingActivity()
            if (nil != inError) {   // This message is if there was some communication error.
                self.hideSavingActivity()
                BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-COMM-FAIL-TITLE", inMessage: "LOCAL-ERROR-COMM-FAILURE-MESSAGE")
            } else {
                if let myParent = self.navigationController?.viewControllers[0] as! BMLTAdminSearchViewController! {    // No meeting changes happened.
                    // If we saved the meeting as a new meeting, this is now the new baseline.
                    if self.newMeeting {
                        do {
                            let jsonObject = try JSONSerialization.jsonObject(with: inResponseData!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                            if let newMeetingObject = jsonObject["newMeeting"] as! NSDictionary? {
                                let idObject = newMeetingObject["id"] as! NSNumber
                                let id: Int = Int(idObject.int32Value)
                                if 0 < id {
                                    self.meeting["id_bigint"] = String(id)
                                    self.meeting["published"] = "0"
                                    self.compMeeting = self.meeting
                                    self.idValue.text = self.meeting["id_bigint"]
                                    self.publishedCheckbox.checked = false
                                    self.publishedCheckboxSelected(self.publishedCheckbox)
                                    self.meetingDirty = false
                                    self.newMeeting = false
                                    BMLTAdminAppDelegate.v_searchController.reloadAndSelectMeetingByID(id)
                                } else {
                                    BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-SERVER-FAIL-TITLE", inMessage: "LOCAL-ERROR-SERVER-FAILURE-MESSAGE")
                                }
                            } else {
                                BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-SERVER-FAIL-TITLE", inMessage: "LOCAL-ERROR-SERVER-FAILURE-MESSAGE")
                            }
                        } catch {
                            BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-CONN-FAIL-TITLE", inMessage: "LOCAL-ERROR-CONN-FAILURE-MESSAGE")
                        }
                    } else {
                        if 0 == myParent.filter.searchResultsBaseline.count {
                            BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-NO-CHANGE-TITLE", inMessage: "LOCAL-ERROR-NO-CHANGE-MESSAGE")
                        } else {    // Successful meeting changes.
                            if self.meetingWasDeleted {
                                myParent.needReload = true
                                _ = self.navigationController?.popToRootViewController(animated: true)
                                BMLTAdminAppDelegate.v_deletedController.appDisconnected = true // Forces the list to reload the next time the page is selected.
                            } else {
                                if nil != myParent.filter {
                                    BMLTAdminAppDelegate.v_searchController.reloadAndSelectMeetingByID(0)
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    // MARK: - Base Class Override Methods -
    /* ################################################################## */
    /**
     Called when the view first loads.
     
     We use this to instantiate the editor contents.
     */
    override func viewDidLoad() {
        self.deleteButtonHidden = false
        
        super.viewDidLoad()
        
        self.idLabel.text = NSLocalizedString(self.idLabel.text!, comment: "")
        self.idValue.text = self.meeting["id_bigint"]
        
        self.setAddressToLocationButton.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        self.setMapToAddressButton.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        self.mapTypeSwitch.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        self.startTimeStepper.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        self.durationStepper.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        
        self.cancelButton.tintColor = BMLTAdminAppDelegate.v_lightButtonColor
        self.historyButton.tintColor = BMLTAdminAppDelegate.v_lightButtonColor
        self.saveButton.tintColor = BMLTAdminAppDelegate.v_lightButtonColor
        
        // Set the page navbar title
        self.navItem.title = NSLocalizedString(self.navItem.title!, comment: "")
        self.title = NSLocalizedString(self.navItem.title!, comment: "")

        // Set the various button titles.
        self.historyButton.title = NSLocalizedString(self.historyButton.title!, comment: "")
    }
    
    // MARK: - IB Handler Methods -
    /* ################################################################## */
    /**
     Called when someone hits the "History" NavBar button.
     
     :param: sender The button item.
     */
    @IBAction func historyButtonHit(_ sender: UIBarButtonItem) {
        if let navController = self.navigationController {
            if let myParent = navController.viewControllers[0] as? BMLTAdminSearchViewController {
                let id = Int(self.meeting["id_bigint"]!)!
                self._historyData = myParent.meetingHistoryData[id]
            }
        }
        self.closeKeyboard(sender)
    }
    
    /* ################################################################## */
    /**
     Called when someone hits the "Delete This Meeting" button.
     
     :param: sender The button item.
     */
    @IBAction func deleteButtonHit(_ sender: UIButton) {
        self.closeKeyboard(sender)
        self.displayDeleteConfirmAlert()
    }
}
