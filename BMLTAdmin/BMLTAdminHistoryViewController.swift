//
//  BMLTAdminHistoryViewController.swift
//  BMLTAdmin
//
//  Created by Chris Marshall on 7/11/16.
//  Copyright © 2016 MAGSHARE. All rights reserved.
//

import UIKit


// MARK: - Classes -
/* ###################################################################################################################################### */
/**
 This class describes one cell of the history table.
 */
class BMLTAdminHistoryViewControllerTableCell: UITableViewCell {
    let sHistoryObjectKey: String = "BMLTAdminHistoryViewControllerTableCell"

    var historyItem: [String:String] = [:]
    var rollbackHandler: ((Int) -> (Void))? = nil
    var row: Int! = nil
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var rollbackButton: UIButton!
    
    // MARK: - Designated Initializer -
    /* ################################################################## */
    /**
     :param: inHistoryItem The history item object to be associated with this cell.
     :param: style The table style we will use.
     :param: reuseIdentifier The ID associated with this instance for reuse later.
     */
    init(inHistoryItem: [String:String], style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.historyItem = inHistoryItem
    }
    
    // MARK: - NSCoding Protocol Methods -
    /* ################################################################## */
    /**
     This method will restore its meeting object from the coder passed in.
     
     :param: aDecoder The coder that will contain the meeting.
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let tempDecoded = aDecoder.decodeObject(forKey: sHistoryObjectKey) as! [String:String]! {
            self.historyItem = tempDecoded
        }
    }
    
    /* ################################################################## */
    /**
     This method saves its meeting as part of it serialization.
     
     :param: aCoder The coder that contains the meeting.
     */
    override func encode(with aCoder: NSCoder) {
        aCoder.encode(self.historyItem, forKey: sHistoryObjectKey)
        super.encode(with: aCoder)
    }
    
    // MARK: - Override Methods -
    /* ################################################################## */
    /**
     In the layout function, we read the various strings from the meeting, and place them into the appropriate label objects.
     */
    override func layoutSubviews() {
        self.dateLabel.text = self.historyItem["date"]
        self.userLabel.text = self.historyItem["user"]
        self.detailsTextView.text = self.historyItem["details"]
        if "0" == self.historyItem["new_meeting"] {
            self.rollbackButton.isHidden = false
            self.rollbackButton.setTitle(NSLocalizedString(self.rollbackButton.title(for: UIControlState())!, comment:""), for: UIControlState())
        } else {
            self.rollbackButton.isHidden = true
        }
    }

    // MARK: - IBAction Methods -
    /* ################################################################## */
    /**
     In the layout function, we read the various strings from the meeting, and place them into the appropriate label objects.
     */
    @IBAction func rollbackButtonHit(_ sender: UIButton) {
        if (nil != self.rollbackHandler) && (nil != self.row) {
            self.rollbackHandler!(self.row)
        }
    }
}

/* ###################################################################################################################################### */
/**
 This class describes a view controller that displays the meeting history.
 */
class BMLTAdminHistoryViewController : UIViewController, BMLTAdminCommunicatorDataSinkProtocol, UITableViewDataSource, UIScrollViewDelegate {
    /** This is the URL path prefix for saving changes. */
    static let sPathPrefix: String  = "/local_server/server_admin/json.php"

    /** The communicator object while in operation. */
    var communicator: BMLTAdminCommunicator! = nil
    
    /** These are used as a semaphore for rolling back changes. */
    var rollbackMeetingID: Int = 0
    var rollbackChangeID: Int = 0
    
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var noHistoryLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var busyLabel: UILabel!
    
    // MARK: - Instance Methods -
    /* ################################################################## */
    /**
     Generates the history request URI as a string.
     
     :returns: The URI, as a string.
     */
    func _generateQueryURI() -> String {
        var ret: String = BMLTAdminAppDelegate.v_rootServerBaseURI + type(of: self).sPathPrefix + "?admin_action=get_changes&meeting_id="
        
        if let numControllers = self.navigationController?.viewControllers.count {
            if 1 < numControllers {
                if let myParent = self.navigationController?.viewControllers[numControllers - 2] as! BMLTAdminEditorViewController! {
                    ret += myParent.meeting["id_bigint"]!
                }
            }
        }
        
        return ret
    }

    /* ################################################################## */
    /**
     */
    func _generateRollbackURI() -> String {
        var ret: String = BMLTAdminAppDelegate.v_rootServerBaseURI + type(of: self).sPathPrefix + "?admin_action=rollback_meeting_to_before_change"
        
        ret += "&meeting_id=" + String(self.rollbackMeetingID)
        ret += "&change_id=" + String(self.rollbackChangeID)
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This presents a confirmation dialog, asking if the user really wants to rollback the meeting.
     
     If they say yes, the meeting is rolled, and the edit screen is brought up for that meeting.
     
     If they say no, then they are returned to the history list.
     */
    func _displayRollbackConfirmAlert() {
        if nil != self.presentingViewController {
            self.presentingViewController?.dismiss(animated: false, completion: nil)
        }
        
        let alertController = UIAlertController(title: NSLocalizedString("LOCAL-HISTORY-ROLLBACK-CONFIRM-TITLE", comment: ""), message: NSLocalizedString("LOCAL-HISTORY-ROLLBACK-MESSAGE-TITLE", comment: ""), preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-HISTORY-ROLLBACK-FINAL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: self._rollbackOKCallback)
        
        alertController.addAction(okAction)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("LOCAL-HISTORY-ROLLBACK-CANCEL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.cancel, handler: self._rollbackCancelCallback)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /* ################################################################## */
    /**
     If the user wants to rollback the meeting, we generate a rollback URI, and send it to the server.
     
     :param: inAction The alert action object (ignored)
     */
    func _rollbackOKCallback(_ inAction: UIAlertAction) {
        if let navController = self.navigationController {
            let viewControllers = navController.viewControllers
            let numControllers = viewControllers.count
            if 1 < numControllers {
                let parentController = viewControllers[numControllers - 2]
                if let myParent = parentController as? BMLTAdminEditorViewController {
                    myParent.historyData = nil
                }
            }
        }
        
        self.communicator = BMLTAdminCommunicator (self._generateRollbackURI(), dataSource: BMLTAdminAppDelegate.appDelegate().connectionSession, delegate: self, refCon: self.rollbackMeetingID as AnyObject?)
    }
    
    /* ################################################################## */
    /**
     If the user wants to cancel the rollback the meeting, this is called.
     
     :param: inAction The alert action object (ignored)
     */
    func _rollbackCancelCallback(_ inAction: UIAlertAction) {
    }
    
    /* ################################################################## */
    /**
     Starts a load from the server of the history.
     */
    func _reloadHistory() {
        self.historyTableView.isHidden = true
        self.busyLabel.isHidden = false
        self.activityIndicatorView.isHidden = false
        self.communicator = BMLTAdminCommunicator (self._generateQueryURI(), dataSource: BMLTAdminAppDelegate.appDelegate().connectionSession, delegate: self, refCon: nil)
    }
    
    /* ################################################################## */
    /**
     This is called when the user selects a rollback button in the history.
     We use this to select a changelist for rollback.
     
     :param: inRow The row of the history item
     */
    func _rollbackHandler(_ inRow: Int) {
        if let numControllers = self.navigationController?.viewControllers.count {
            if 1 < numControllers {
                if let myParent = self.navigationController?.viewControllers[numControllers - 2] as! BMLTAdminEditorViewController! {
                    if nil != myParent.historyData {
                        let changeItem = myParent.historyData[inRow]
                        self.rollbackMeetingID = Int(changeItem["meeting_id"]!)!
                        self.rollbackChangeID = Int(changeItem["change_id"]!)!
                        self._displayRollbackConfirmAlert()
                    }
                }
            }
        }
    }
    
    /* ################################################################## */
    /**
     Called when the user dismisses the success alert.
     */
    func successCallback() {
        BMLTAdminAppDelegate.v_searchController.reloadAndSelectMeetingByID(self.rollbackMeetingID)
    }
    
    // MARK: - Base Class Override Methods -
    /* ################################################################## */
    /**
     Called when the view first loads.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the page navbar title and display
        self.navigationController!.navigationBar.barTintColor = UIColor.black
        self.navigationController!.navigationBar.barStyle = UIBarStyle.black
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.title = NSLocalizedString("LOCAL-NAVBAR-HISTORY-TITLE", comment: "")
        self.navigationItem.title = NSLocalizedString(self.title!, comment: "")
        
        self.busyLabel.text = NSLocalizedString(self.busyLabel.text!, comment: "")
        self.noHistoryLabel.text = NSLocalizedString(self.noHistoryLabel.text!, comment: "")
        
        self.noHistoryLabel.isHidden = true
    }
    
    /* ################################################################## */
    /**
     Called when the view is about to appear.
     
     :param: animated True, if the transition is being animated.
     */
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.isNavigationBarHidden = false
        super.viewWillAppear(animated)
        if let navController = self.navigationController {
            let viewControllers = navController.viewControllers
            let numControllers = viewControllers.count
            if 1 < numControllers {
                let parentController = viewControllers[numControllers - 2]
                if let myParent = parentController as? BMLTAdminEditorViewController {
                    if nil != myParent.historyData {
                        self.historyTableView.isHidden = false
                        self.noHistoryLabel.isHidden = true
                        self.historyTableView.reloadData()
                    } else {
                        self._reloadHistory()
                    }
                }
            }
        }
    }
    
    /* ################################################################## */
    /**
     Called when the view is about to go away.
     
     :param: animated True, if the transition is being animated.
     */
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController!.isNavigationBarHidden = true
        super.viewWillDisappear(animated)
    }
    
    // MARK: - BMLTAdminCommunicatorDataSinkProtocol Methods -
    /* ################################################################## */
    /**
     The get history response callback.
     
     :param: inHandler The handler for this call.
     :param: inResponseData The data from the URL request. If nil, the call failed to produce. Check the handler's error data member.
     :param: inError Any errors that occurred
     :param: inRefCon The data/object passed in via the 'refCon' parameter in the initializer.
     */
    func responseData(_ inHandler: BMLTAdminCommunicator?, inResponseData: Data?, inError: Error?, inRefCon: AnyObject?) {
        DispatchQueue.main.async(execute: {
            self.historyTableView.isHidden = false
            self.busyLabel.isHidden = true
            self.activityIndicatorView.isHidden = true
            self.communicator = nil
            if (nil != inError) {   // This message is if there was some communication error.
                self.rollbackMeetingID = inRefCon as! Int
                BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-COMM-FAIL-TITLE", inMessage: "LOCAL-ERROR-COMM-FAILURE-MESSAGE")
            } else {
                if(nil != inResponseData) {
                    // See if this was a rollback operation. If so, we deal with that in a fairly straightforward manner.
                    if nil != inRefCon {
                        BMLTAdminAppDelegate.displayAlert("LOCAL-HISTORY-ROLLBACK-ALERT-TITLE", inMessage: "LOCAL-HISTORY-ROLLBACK-ALERT-MESSAGE", inCallback: self.successCallback)
                        BMLTAdminAppDelegate.v_searchController.reloadAndSelectMeetingByID(inRefCon as! Int)
                    } else {    // Otherwise, we need to parse the history data.
                        if let numControllers = self.navigationController?.viewControllers.count {
                            if 1 < numControllers {
                                if let myParent = self.navigationController?.viewControllers[numControllers - 2] as! BMLTAdminEditorViewController! {
                                    do {
                                        var historyData: [[String:String]] = []
                                        
                                        let jsonObject = try JSONSerialization.jsonObject(with: inResponseData!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary
                                        
                                        if nil != jsonObject {
                                            // Well, as it turns out, there's not really a "toll-free" bridging between the NS types and the native Swift types. We need to unwind the data, and re-form it.
                                            let castDictionary = jsonObject as! [String:AnyObject]
                                            
                                            var changeList: [[String:AnyObject?]] = []
                                            
                                            // If the data is in multiple elements, then they are gathered in a single array, called "row."
                                            if let rowElement = castDictionary["row"] {
                                                changeList = []
                                                let changeListTemp = rowElement as! NSArray
                                                let changeListTemp2 = changeListTemp as! [NSDictionary]
                                                for element in changeListTemp2 {
                                                    var newElement:[String:AnyObject?] = [:]
                                                    for (key, value) in element {
                                                        newElement[key as! String] = value as AnyObject??
                                                    }
                                                    changeList.append(newElement)
                                                }
                                            } else {    // However, a single element is all by itself. We push it into an array.
                                                changeList = [castDictionary]
                                            }
                                            
                                            // Extract the 3 pieces of information we're interested in.
                                            if !changeList.isEmpty {
                                                for changeRec in changeList {
                                                    var newMeeting = "0"
                                                    var change_id_string = ""
                                                    var meeting_id_string = ""
                                                    var dateString = NSLocalizedString("LOCAL-HISTORY-NO-DATE", comment: "")
                                                    var userString = NSLocalizedString("LOCAL-HISTORY-NO-USER", comment: "")
                                                    var detailsString = NSLocalizedString("LOCAL-HISTORY-NO-DETAILS", comment: "")
                                                    
                                                    if nil != changeRec["new_meeting"] {
                                                        let test = changeRec["new_meeting"] as! NSNumber
                                                        if 0 != test {
                                                            newMeeting = "1"
                                                        }
                                                    }
                                                    
                                                    if nil != changeRec["change_id"] {
                                                        let id = changeRec["change_id"] as! NSNumber
                                                        change_id_string = String(id.intValue)
                                                    }
                                                    
                                                    if nil != changeRec["meeting_id"] {
                                                        let id = changeRec["meeting_id"] as! NSNumber
                                                        meeting_id_string = String(id.intValue)
                                                    }
                                                    
                                                    if nil != changeRec["date_int"] {
                                                        let dateNumber = changeRec["date_int"] as! NSNumber
                                                        let dateInterval: TimeInterval = dateNumber.doubleValue
                                                        let date = Date(timeIntervalSince1970: dateInterval)
                                                        let formatter = DateFormatter()
                                                        // Long date, but short ante meridian time.
                                                        formatter.dateStyle = DateFormatter.Style.long
                                                        formatter.timeStyle = DateFormatter.Style.short
                                                        
                                                        dateString = formatter.string(from: date)
                                                    }
                                                    
                                                    if nil != changeRec["user_name"] {
                                                        userString = changeRec["user_name"] as! String!
                                                    }
                                                    
                                                    if nil != changeRec["details"] {
                                                        detailsString = changeRec["details"] as! String!
                                                    }
                                                    
                                                    var newMeetingObject: [String:String] = [:]
                                                    
                                                    newMeetingObject["new_meeting"] = newMeeting
                                                    newMeetingObject["change_id"] = change_id_string
                                                    newMeetingObject["meeting_id"] = meeting_id_string
                                                    newMeetingObject["date"] = dateString
                                                    newMeetingObject["user"] = userString
                                                    newMeetingObject["details"] = detailsString
                                                    
                                                    historyData.append(newMeetingObject)
                                                }
                                                
                                                myParent.historyData = historyData
                                                
                                                self.noHistoryLabel.isHidden = true
                                                self.historyTableView.isHidden = false
                                                self.historyTableView.reloadData()
                                            } else {
                                                self.historyTableView.isHidden = true
                                                self.noHistoryLabel.isHidden = false
                                                myParent.historyData = nil
                                            }
                                        } else {
                                            self.historyTableView.isHidden = true
                                            self.noHistoryLabel.isHidden = false
                                            myParent.historyData = nil
                                        }
                                    } catch {
                                        BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-CONN-FAIL-TITLE", inMessage: "LOCAL-ERROR-CONN-FAILURE-MESSAGE")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    // MARK: - UIScrollViewDelegate Protocol Methods -
    /* ################################################################## */
    /**
     This is called when the scroll view has ended dragging.
     We use this to trigger a reload, if the scroll was pulled beyond its limit by a certain number of display units.
     
     :param: scrollView The text view that experienced the text change.
     :param: velocity The velocity of the scroll at the time of this call.
     :param: targetContentOffset The offset we can send back (unused).
     */
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if ( (velocity.y < 0) && (scrollView.contentOffset.y < BMLTAdminAppDelegate.v_searchController.sScrollToReloadThreshold) ) {
            self._reloadHistory()
        }
    }
    
    // MARK: - UITableViewDataSource Protocol Methods -
    /* ################################################################## */
    /**
     This returns the number of Meetings we'll need to display.
     
     :param: tableView The Table View
     :param: section The 0-based index of the section.
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var ret: Int = 0
        
        if let numControllers = self.navigationController?.viewControllers.count {
            if 1 < numControllers {
                if let myParent = self.navigationController?.viewControllers[numControllers - 2] as! BMLTAdminEditorViewController! {
                    if nil != myParent.historyData {
                        ret = myParent.historyData.count
                    }
                }
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This returns one cell for a Meeting.
     
     :param: tableView The Table View
     :param: indexPath The index path to the cell we need.
     
     :returns:   one cell object for the given Meeting
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ret: BMLTAdminHistoryViewControllerTableCell! = nil
        
        var frame = tableView.bounds
        frame.size.height = tableView.rowHeight
        
        if 0 < self.tableView(tableView, numberOfRowsInSection: (indexPath as NSIndexPath).section) {
            if let numControllers = self.navigationController?.viewControllers.count {
                if 1 < numControllers {
                    if let myParent = self.navigationController?.viewControllers[numControllers - 2] as! BMLTAdminEditorViewController! {
                        ret = tableView.dequeueReusableCell(withIdentifier: String("indexPath.row")) as? BMLTAdminHistoryViewControllerTableCell
                        if nil == ret {
                            ret = BMLTAdminHistoryViewControllerTableCell(inHistoryItem: myParent.historyData[(indexPath as NSIndexPath).row], style: UITableViewCellStyle.default, reuseIdentifier: String("indexPath.row"))
                            // We load the prototype from our xib file.
                            if nil != ret {
                                if let view = Bundle.main.loadNibNamed("HistoryTablePrototype", owner: ret, options: nil)?.first as? UIView {
                                    view.frame = frame
                                    ret.frame = frame
                                    ret.backgroundColor = UIColor.clear
                                    ret.addSubview(view)
                                }
                            }
                        } else {
                            ret.historyItem = myParent.historyData[(indexPath as NSIndexPath).row]
                        }
                        
                        if (nil != ret) && (0 == ((indexPath as NSIndexPath).row % 2)) {
                            ret.backgroundColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.1)
                        } else {
                            if nil != ret {
                                ret.backgroundColor = UIColor.clear
                            }
                        }
                        
                        ret.rollbackHandler = self._rollbackHandler
                        ret.row = (indexPath as NSIndexPath).row
                        
                        ret.setNeedsLayout()
                    }
                }
            }
       }
        
        return ret
    }
}
