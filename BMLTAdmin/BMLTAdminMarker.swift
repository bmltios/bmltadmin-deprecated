//
//  BMLTAdminMarker.swift
//  BMLTAdmin
/**
 :license:
 Created by MAGSHARE.
 This is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 BMLT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this code.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 This file contains a couple of classes that allow us to establish and manipulate markers in our map.
 */

import MapKit

// MARK: - Classes -
/* ###################################################################################################################################### */
/**
 This handles the marker annotation.
 */
class BMLTAdminAnnotation : NSObject, MKAnnotation, NSCoding {
    let sCoordinateObjectKey: String = "BMLTAdminAnnotation_Coordinate"
    let sMeetingsObjectKey: String = "BMLTAdminAnnotation_Meetings"

    @objc var coordinate: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    var meetings: BMLTAdminMeetingList = []
    
    /* ################################################################## */
    /**
     Default initializer.
     
     :param: coordinate the coordinate for this annotation display.
     :param: meetings a list of meetings to be assigned to this annotation.
     */
    init(coordinate: CLLocationCoordinate2D, meetings: BMLTAdminMeetingList) {
        self.coordinate = coordinate
        self.meetings = meetings
    }
    
    // MARK: - NSCoding Protocol Methods -
    /* ################################################################## */
    /**
     This method will restore the meetings and coordinate objects from the coder passed in.
     
     :param: aDecoder The coder that will contain the coordinates.
     */
    @objc required init?(coder aDecoder: NSCoder) {
        self.meetings = aDecoder.decodeObject(forKey: self.sMeetingsObjectKey) as! BMLTAdminMeetingList
        if let tempCoordinate = aDecoder.decodeObject(forKey: self.sCoordinateObjectKey) as! [NSNumber]! {
            self.coordinate.longitude = tempCoordinate[0].doubleValue
            self.coordinate.latitude = tempCoordinate[1].doubleValue
        }
    }
    
    /* ################################################################## */
    /**
     This method saves the meetings and coordinates as part of the serialization.
     
     :param: aCoder The coder that contains the coordinates.
     */
    @objc func encode(with aCoder: NSCoder) {
        let long: NSNumber = NSNumber(value: self.coordinate.longitude as Double)
        let lat: NSNumber = NSNumber(value: self.coordinate.latitude as Double)
        let values: [NSNumber] = [long, lat]
        
        aCoder.encode(values, forKey: self.sCoordinateObjectKey)
        aCoder.encode(self.meetings, forKey: self.sMeetingsObjectKey)
    }
}

/* ###################################################################################################################################### */
/**
 This handles our map marker.
 */
class BMLTAdminMarker : MKAnnotationView {
    let sAnnotationObjectKey: String = "BMLTAdminMarker_Annotation"
    let sRegularAnnotationOffsetUp: CGFloat     = 24; /**< This is how many display units to shift the annotation view up. */
    let sRegularAnnotationOffsetRight: CGFloat  = 5;  /**< This is how many display units to shift the annotation view right. */

    // MARK: - Properties -
    var currentFrame: Int = 0
    var animationTimer: Timer! = nil
    var animationFrames: [UIImage] = []
    
    // MARK: - Computed Properties -
    /* ################################################################## */
    /**
     We override this, so we can be sure to refresh the need for a draw state when draggable is set (Meaning it's a black marker).
     */
    override var isDraggable: Bool {
        get {
            return super.isDraggable
        }
        
        set {
            // You can only drag if there are no meetings, or just one meeting.
            if 2 > self.meetings.count {
                super.isDraggable = newValue
            } else {
                super.isDraggable = false
            }
            
            if !super.isDraggable {
                self.animationFrames = []
            }
            
            self.setNeedsDisplay()
        }
    }
    
    /* ################################################################## */
    /**
     This gives us a shortcut to the annotation prpoerty.
     */
    var coordinate: CLLocationCoordinate2D {
        get {
            return (self.annotation?.coordinate)!
        }
    }
    
    /* ################################################################## */
    /**
     This gives us a shortcut to the annotation prpoerty.
     */
    var meetings: BMLTAdminMeetingList {
        get {
            return ((self.annotation as! BMLTAdminAnnotation).meetings)
        }
    }
    
    // MARK: - Instance Methods -
    /* ################################################################## */
    /**
     The default initializer.
     
     :param: annotation The annotation that represents this instance.
     :param: draggable If true, then this will be draggable (ignored if the annotation has more than one meeting).
     */
    init(annotation: MKAnnotation?, draggable : Bool, reuseID: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseID)
        self.isDraggable = draggable
        self.backgroundColor = UIColor.clear
        self.image = self.selectImage(false)
        self.centerOffset = CGPoint(x: self.sRegularAnnotationOffsetRight, y: -self.sRegularAnnotationOffsetUp)
    }
    
    /* ################################################################## */
    /**
     This selects the appropriate image for our display.
     :param: inAnimated If true, then the drag will be animated.
     */
    func selectImage(_ inAnimated: Bool) -> UIImage! {
        var image: UIImage! = nil
        if self.isDraggable && (2 > self.meetings.count) {
            if self.dragState == MKAnnotationViewDragState.dragging {
                if inAnimated {
                    image = self.animationFrames[self.currentFrame]
                } else {
                    image = UIImage(named: "MapMarkerGreen", in: nil, compatibleWith: nil)
                }
            } else {
                image = UIImage(named: "MapMarkerBlack", in: nil, compatibleWith: nil)
            }
        } else {
            if self.isSelected {
                image = UIImage(named: "MapMarkerGreen", in: nil, compatibleWith: nil)
            } else {
                if 1 < self.meetings.count {
                    image = UIImage(named: "MapMarkerRed", in: nil, compatibleWith: nil)
                } else {
                    image = UIImage(named: "MapMarkerBlue", in: nil, compatibleWith: nil)
                }
            }
        }
        
        return image
    }
    
    // MARK: - Base Class Override Methods -
    /* ################################################################## */
    /**
     Draws the image for the marker.
     
     :param: rect The rectangle in which this is to be drawn.
     */
    override func draw(_ rect: CGRect) {
        let image = self.selectImage(0 < self.animationFrames.count)
        if nil != image {
            image!.draw(in: rect)
        }
    }
    
    /* ################################################################## */
    /**
     Sets the drag state for this marker.
     
     :param: newDragState The new state that should be set after this call.
     :param: animated True, if the state change is to be animated (ignored).
     */
    override func setDragState(_ newDragState: MKAnnotationViewDragState, animated: Bool) {
        var subsequentDragState = MKAnnotationViewDragState.none
        switch newDragState {
        case MKAnnotationViewDragState.starting:
            subsequentDragState = MKAnnotationViewDragState.dragging
            self.currentFrame = 0
            self.animationFrames = []
            
        case MKAnnotationViewDragState.dragging:
            if animated && (0 == self.animationFrames.count) {
                // Set up the drag animation.
                // We have 10 frames in the drag animation.
                for c in 1 ..< 11 {
                    // Construct an image name for each frame.
                    var imageName = "Frame"
                    if 10 > c {
                        imageName += "0"
                    }
                    
                    imageName += String(c)
                    
                    if let image = UIImage(named: imageName, in: nil, compatibleWith: nil) {
                        self.animationFrames.append(image)
                    }
                }
            }
            
            _ = self.selectImage(true)
            self.currentFrame += 1
            subsequentDragState = MKAnnotationViewDragState.dragging
            if self.currentFrame == self.animationFrames.count {
                self.currentFrame = 0
            }

        default:
            subsequentDragState = MKAnnotationViewDragState.none
        }
        
        super.dragState = subsequentDragState
        self.setNeedsDisplay()
    }
    
    // MARK: - NSCoding Protocol Methods -
    /* ################################################################## */
    /**
     This class will restore its meeting object from the coder passed in.
     
     :param: aDecoder The coder that will contain the meeting.
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.annotation = aDecoder.decodeObject(forKey: self.sAnnotationObjectKey) as! BMLTAdminAnnotation
    }
    
    /* ################################################################## */
    /**
     This method saves the meetings and coordinates as part of the serialization.
     
     :param: aCoder The coder that contains the coordinates.
     */
    override func encode(with aCoder: NSCoder) {
        aCoder.encode(self.annotation, forKey: self.sAnnotationObjectKey)
        super.encode(with: aCoder)
    }
}
