//
//  BMLTAdminMeetingFilter.swift
//  BMLTAdmin
/**
     :license:
     Created by MAGSHARE.
     This is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.
     
     BMLT is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
     
     You should have received a copy of the GNU General Public License
     along with this code.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


// MARK: - Type Aliases -
/* ###################################################################################################################################### */
/**
 */
typealias BMLTAdminMeeting = [String:String]
typealias BMLTAdminMeetingList = [BMLTAdminMeeting]

/* ################################################################## */
/**
 This method compares two meeting lists.
 
 :param: inLeft The left side of the comparison
 :param: inRight The right side of the comparison
 
 :returns: a Bool. True, if the two filtered results are the same.
 */
func ==(inLeft: BMLTAdminMeetingList, inRight: BMLTAdminMeetingList) -> Bool {
    var ret: Bool = false
    
    // If we are not the same size, then we aren't equal.
    if inLeft.count == inRight.count {
        ret = true
        
        for meeting1 in inLeft {
            for meeting2 in inRight {
                if meeting1 != meeting2 {
                    ret = false
                    break
                }
            }
        }
    }
    
    return ret
}

/* ################################################################## */
/**
 This method compares two filters.
 
 :param: inLeft The left side of the comparison
 :param: inRight The right side of the comparison
 
 :returns: a Bool. True, if the two filtered results are the same.
 */
func ==(inLeft: BMLTAdminMeetingFilter, inRight: BMLTAdminMeetingFilter) -> Bool {
    let left: [String:String] = inLeft.searchFilter
    let right: [String:String] = inRight.searchFilter
    
    if left == right {
        return true
    }
    
    return false
}

// MARK: - Classes -
/* ###################################################################################################################################### */
/**
 This class is used to provide a very light and easy-to-use method of filtering quickly through a list of meetings for ones that match certain criteria.
 
 It works by applying a filter with some fixed criteria to a large list of meetings.
 
 All returns are realtime. The only static data are the initial (unfiltered) list of meetings, and the search criteria.
 */
class BMLTAdminMeetingFilter : Sequence, NSCoding {
    // MARK: - Instance Constants -
    /** This is used to key the encoder for this class. */
    let sFilterListStorageKey: String = "BMLTAdminMeetingFilter_FilterDictionary"
    
    // MARK: - Class Enums Constants -
    /** These are the keys for the basic filter class. Since this class is the basic class, we only have these filters available. */
    enum FilterKeys:String {
        /** This denotes a CSV 1-based list of weekday integers ("1,2,3,4,5,6,7"), where "1" is Sunday, and "7" is Saturday. */
        case Weekdays       = "weekdays"
        /** This denotes a CSV list of Service body BMLT IDs (integers). */
        case ServiceBodies  = "service-bodies"
        /** This denotes a pure string to be used for searching against the Meeting Name and String-based location elements. */
        case SearchString   = "search-string"
        /** This denotes looking for boroughs and towns. */
        case Town           = "town"
    }
    
    // MARK: - Instance Internal Properties -
    /** This contains the original, unfiltered search result. */
    var _searchResultsBaseline: BMLTAdminMeetingList! = nil
    /** This will contain a list of the search filters to be applied. */
    var _searchFilter: [String:String]
    
    // MARK: - Class Methods -
    /* ################################################################## */
    /**
     This method searches a meeting for a given substring.
     
     :param: inStringToBeFound The substring we are looking for.
     :param: inMeetingToBeSearched The meeting object to be searched.
     
     :returns: a Bool. True, if the substring was found.
     */
    class func doesMeetingContainThisString(_ inStringToBeFound: String, inMeetingToBeSearched: BMLTAdminMeeting) -> Bool {
        var ret : Bool = false
        // These are localized renditions of weekday names.
        let weekdays: [String] = [NSLocalizedString("LOCAL-WEEKDAY-SUN", comment: ""),
                                  NSLocalizedString("LOCAL-WEEKDAY-MON", comment: ""),
                                  NSLocalizedString("LOCAL-WEEKDAY-TUE", comment: ""),
                                  NSLocalizedString("LOCAL-WEEKDAY-WED", comment: ""),
                                  NSLocalizedString("LOCAL-WEEKDAY-THU", comment: ""),
                                  NSLocalizedString("LOCAL-WEEKDAY-FRI", comment: ""),
                                  NSLocalizedString("LOCAL-WEEKDAY-SAT", comment: "")]
        
        // We may also examine formats.
        let formats:[Int:[String:String]] = BMLTAdminAppDelegate.v_formats
        
        // Need to be street legal, first.
        if !inStringToBeFound.isEmpty && (0 < inMeetingToBeSearched.count) {
            for meetingField in inMeetingToBeSearched {
                if nil != meetingField.1.lowercased().range(of: inStringToBeFound.lowercased()) {
                    ret = true
                    break
                }
                
                // If the string was not found previously, we try one more thing. We create a string for the weekday, and look for that. */
                let weekdayIndex = Int ( inMeetingToBeSearched["weekday_tinyint"]! )
                if (0 < weekdayIndex) && (8 > weekdayIndex) {
                    let weekday = weekdays[weekdayIndex! - 1]
                    if (weekday.lowercased().range(of: inStringToBeFound.lowercased()) != nil) {
                        ret = true
                        break
                    }
                }
                
                // If the string still hasn't been found, let's look in the format names (not descriptions).
                // Yeah, it's inefficient, doing this every time, but let's see if we need to go to the extra agita of setting up a static table.
                var quickLookup: [String:String] = [:]
                for format in formats {
                    let formatFields = format.1
                    if let nameString = formatFields["name_string"] as String! {
                        if let keyString = formatFields["key_string"] as String! {
                            quickLookup.updateValue(nameString, forKey: keyString)
                        }
                    }
                }
                
                if 0 < quickLookup.count {
                    let meetingFormatArray = inMeetingToBeSearched["formats"]?.components(separatedBy: ",")
                    
                    for key in meetingFormatArray! {
                        if let lookup = quickLookup[key] {
                            if (lookup.lowercased().range(of: inStringToBeFound.lowercased()) != nil) {
                                ret = true
                                break
                            }
                        }
                    }
                }
            }
        }
        
        return ret
    }
    
    // MARK: - NSCoding Protocol Methods -
    /* ################################################################## */
    /**
     This adds the search criteria (but not the search results base) to the store.
     
     :param: aCoder The encoder store, passed by reference. This function changes that store.
     */
    @objc func encode(with aCoder: NSCoder) {
        aCoder.encode(self._searchFilter, forKey: sFilterListStorageKey)
    }
    
    /* ################################################################## */
    /**
     This function will initialize its filter criteria with any provided by the store.
     
     :param: aCoder The encoder store. This function reads that store.
     */
    @objc required init?(coder aDecoder: NSCoder) {
        self._searchResultsBaseline = nil
        if let tempDecoded = aDecoder.decodeObject(forKey: sFilterListStorageKey) as! [String:String]! {
            self._searchFilter = tempDecoded
        } else {
            self._searchFilter = [:]
        }
    }
    
    // MARK: - SequenceType Methods -
    /* ################################################################## */
    /**
     This is used to create a generator/iterator for this class, allowing it to be used in "for in" loops.
     
     :returns: a gaenerator object, primed with the number of items in the object.
     */
    func makeIterator() -> AnyIterator<BMLTAdminMeeting> {
        var nextIndex = 0
        
        /* ############################################################## */
        /**
         This is a special generator object that will return the next instance in the list.
         
         :returns: a BMLTAdminMeeting instance, the item in the filter list.
         */
        return AnyIterator {
            nextIndex += 1
            
            if nextIndex >= self.count {
                return nil
            }
            
            return self.searchResults[nextIndex - 1]
        }
    }

    // MARK: - Internal Methods -
    /* ################################################################## */
    /**
     This is the heart of the class. This method is where we will apply the
     filter.
     
     :returns: a BMLTAdminMeetingList, which is an array of BMLTAdminMeeting objects representing the filtered subset of the base.
     */
    func _applyFilter() -> BMLTAdminMeetingList {
        var ret: BMLTAdminMeetingList = []
        var temp = self.searchResultsBaseline
        
        // If we don't have anything to work with, we return an empty Array.
        if (nil == temp) || (0 == temp?.count) {
            ret = []
        } else {    // Otherwise, Woo-Hoo! Roll up yer sleeves!
            // First, see if they are specifying weekdays.
            if let filterValue = self._searchFilter[FilterKeys.Weekdays.rawValue] {
                // This decomposes a CSV String into an Array of Int.
                let weekdayArray = filterValue.components(separatedBy: ",").map { Int ($0) }
                
                // As long as we have at least one weekday.
                if 0 < weekdayArray.count {
                    // Vet the array by building a new one.
                    var newWeekdayArray: [Int] = []
                    for num in weekdayArray {
                        if (nil != num) && !newWeekdayArray.contains(num!) && (0 < num!) && (8 > num!) {
                            newWeekdayArray.append(num!)
                        }
                    }
                    // OK. We now have a simple Array of Int, from 1 to 7, unique values, that will be used to filter the baseline.
                    var tempusFuggit: BMLTAdminMeetingList = [] // We will build a temporary list, and add meetings that fit the criteria, excluding ones that don't.
                    for meeting in temp! {
                        if newWeekdayArray.contains(Int(meeting["weekday_tinyint"]!)!) {
                            tempusFuggit.append(meeting)
                        }
                    }
                    
                    temp = tempusFuggit
                }
            }
            
            // Next, see if they are specifying Service bodies.
            if let filterValue = self._searchFilter[FilterKeys.ServiceBodies.rawValue] {
                // This decomposes a CSV String into an Array of Int.
                let serviceBodyArray = filterValue.components(separatedBy: ",").map { Int ($0) }
                
                // As long as we have at least one weekday.
                if 0 < serviceBodyArray.count {
                    // Vet the array by building a new one.
                    var newServiceBodyArray: [Int] = []
                    for num in serviceBodyArray {
                        if (nil != num) && !newServiceBodyArray.contains(num!) && (0 < num!) {
                            newServiceBodyArray.append(num!)
                        }
                    }
                    // OK. We now have a simple Array of Int that will be used to filter the baseline.
                    var tempusFuggit: BMLTAdminMeetingList = [] // We will build a temporary list, and add meetings that fit the criteria, excluding ones that don't.
                    for meeting in temp! {
                        if newServiceBodyArray.contains(Int(meeting["service_body_bigint"]!)!) {
                            tempusFuggit.append(meeting)
                        }
                    }
                    
                    temp = tempusFuggit
                }
            }
            
            // Now, let's see if they are looking for a string.
            if let filterValue = self._searchFilter[FilterKeys.SearchString.rawValue] {
                var tempusFuggit: BMLTAdminMeetingList = [] // We will build a temporary list, and add meetings that fit the criteria, excluding ones that don't.
                for meeting in temp! {
                    if type(of: self).doesMeetingContainThisString(filterValue, inMeetingToBeSearched: meeting) {
                        tempusFuggit.append(meeting)
                    }
                }
                
                temp = tempusFuggit
            }
           
            // Now, see if they are looking for a town.
            if let filterValue = self._searchFilter[FilterKeys.Town.rawValue] {
                var tempusFuggit: BMLTAdminMeetingList = [] // We will build a temporary list, and add meetings that fit the criteria, excluding ones that don't.

                for meeting in temp! {
                    var town: String = ""
                    
                    if let borough = meeting["location_city_subsection"] {
                        if !borough.isEmpty {
                            town = borough
                        }
                        
                        if borough.isEmpty {
                            if let townName = meeting["location_municipality"] {
                                if !townName.isEmpty {
                                    town = townName
                                }
                                town = townName
                            }
                        }
                        
                    } else {
                        if let townName = meeting["location_municipality"] {
                            if !townName.isEmpty {
                                town = townName
                            }
                            town = townName
                        }
                    }
                    
                    if let state = meeting["location_province"] {
                        if !town.isEmpty {
                            town += (", " + state)
                        }
                    }
                    
                    if filterValue == town {
                        tempusFuggit.append(meeting)
                    }
                }
                
                temp = tempusFuggit
            }
            
            ret = temp!
        }
        
        return ret
    }

    // MARK: - Public Property Methods -
    /* ################################################################## */
    /**
     This accesses the base list (unfiltered).
     */
    var searchResultsBaseline: BMLTAdminMeetingList! {
        get {
            return self._searchResultsBaseline
        }
        
        set {
            self._searchResultsBaseline = newValue
        }
    }
    
    /* ################################################################## */
    /**
     This will return the filtered set of meetings.
     
     :returns: an optional BMLTAdminMeetingList object, with the filtered result set.
     */
    var searchResults: BMLTAdminMeetingList {
        get {
            return self._applyFilter()
        }
    }
    
    /* ################################################################## */
    /**
     This returns the number of meetings in the filtered set.
     
     :returns: an Int, with the number of meetings in the set that results from application of the filters.
     */
    var count: Int {
        get {
            return self._applyFilter().count
        }
    }
    
    /* ################################################################## */
    /**
     This allows the object to be treated like an Array. It returns the indexed element from the filtered response.
     
     :returns: an optional BMLTAdminMeeting instance, with the indexed meeting object. nil, if there is no object at the index.
     */
    subscript(index:Int) -> BMLTAdminMeeting! {
        get {
            switch index {
            case 0..<self.count:
                let meeting = self.searchResults[index] as BMLTAdminMeeting
                return meeting
            default:
                return nil
            }
        }
    }
    
    /* ################################################################## */
    /**
     This access the search filter Dictionary.
     */
    var searchFilter: [String:String] {
        get {
            return self._searchFilter
        }
        
        set {
            self._searchFilter = newValue
        }
    }
    
    // MARK: - Public Methods -
    /* ################################################################## */
    /**
     Initializer (default) with a preset for the search base.
     
     :param: inList a primed Array for the baseline.
     */
    init(inList: BMLTAdminMeetingList) {
        self._searchFilter = [:]
        self.searchResultsBaseline = inList
    }
    
    /* ################################################################## */
    /**
     This function adds (or updates) one filter in the filter set.
     
     It follows the rules set out in the Enum, above.
     
     :param: inKey the search filter key
     :param: inFilterValue the value for that filter, redered as a String (either CSV Ints, or a String)
     
     :returns: the new filtered search results.
     */
    func addFilter(_ inKey: FilterKeys, inFilterValue: String) -> BMLTAdminMeetingList {
        self._searchFilter[inKey.rawValue] = inFilterValue
        
        return self.searchResults
    }
    
    /* ################################################################## */
    /**
     This function clears one filter from the filter set. It is ignored if the filter is not part of the set.
     
     :param: inKey the search filter key to be removed
     */
    func clearFilter(_ inKey: FilterKeys) {
        if let _ = self._searchFilter[inKey.rawValue] {
            self._searchFilter.removeValue(forKey: inKey.rawValue)
        }
    }
}
