//
//  BMLTAdminNewMeetingViewController.swift
//  BMLTAdmin
/**
 :license:
 Created by MAGSHARE.
 This is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 BMLT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this code.  If not, see <http://www.gnu.org/licenses/>.
 */

import UIKit

// MARK: - Classes -
/* ###################################################################################################################################### */
/**
 This class presents the New Meeting display. It embeds the editor.
 */
class BMLTAdminNewMeetingViewController: BMLTAdminEditorBaseViewController {
    @IBOutlet weak var resetButton: UIBarButtonItem!
    
    // MARK: - Internal Methods -
    /* ################################################################## */
    /**
     This presents a confirmation dialog, asking if the user wants to lose the changes they made.
     
     If they say yes, then the meeting data is reset to the default.
     
     If they say no, then they are returned to the new meeting screen.
     */
    func displayResetConfirmAlert() {
        if self.meetingDirty {
            if nil != self.presentingViewController {
                self.presentingViewController?.dismiss(animated: false, completion: nil)
            }
            
            let alertController = UIAlertController(title: NSLocalizedString("LOCAL-NEW-RESET-CONFIRM-TITLE", comment: ""), message: NSLocalizedString("LOCAL-NEW-RESET-MESSAGE-TITLE", comment: ""), preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-LOSE-CHANGES-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: self.resetOKCallback)
            
            alertController.addAction(okAction)
            
            let cancelAction = UIAlertAction(title: NSLocalizedString("LOCAL-EDIT-CANCEL-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    /* ################################################################## */
    /**
     If the user wants to lose the changes, the editor is closed with no other action.
     
     :param: inAction The alert action object (ignored)
     */
    func resetOKCallback(_ inAction: UIAlertAction) {
        self.meeting = nil
        self.compMeeting = nil
        self.setMeetingData()
    }

    // MARK: - Base Class Override Methods -
    /* ################################################################## */
    /**
     This is called when the view first loads.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setAddressToLocationButton.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        self.setMapToAddressButton.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        self.mapTypeSwitch.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        self.startTimeStepper.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        self.durationStepper.tintColor = BMLTAdminAppDelegate.v_darkButtonColor
        
        self.resetButton.tintColor = BMLTAdminAppDelegate.v_lightButtonColor
        self.saveButton.tintColor = BMLTAdminAppDelegate.v_lightButtonColor
        self.publishedLabel.isHidden = true
        self.publishedCheckbox.isHidden = true
        self.nameTopConstraint.constant = 8
        self.resetButton.title = NSLocalizedString(self.resetButton.title!, comment: "")
        
        // Set the page navbar title
        self.navItem.title = NSLocalizedString(self.navItem.title!, comment: "")
        self.title = NSLocalizedString(self.navItem.title!, comment: "")
    }
    
    /* ################################################################## */
    /**
     We grab this, because we don't want the orange background.
     */
    override func selectAppropriateBackground() {
    }
    
    /* ################################################################## */
    /**
     This is called just before the view appears.
     
     :param: animated If true, the transition will be animated.
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkSaveStatus()
        if self.appDisconnected && !self.meetingDirty {
            self.setMeetingData()
        }
        self.appDisconnected = false
    }
    
    /* ################################################################## */
    /**
     Checks the edited state. If the meeting is different, the "Save" button is activated.
     */
    override func checkSaveStatus() {
        super.checkSaveStatus()
        self.resetButton.isEnabled = self.meetingDirty
    }
    
    // MARK: - IBAction Methods -
    /* ################################################################## */
    /**
     This is called if the user hits the "Reset" navbar button.
     
     :param: sender The button object.
     */
    @IBAction func resetBUttonHit(_ sender: UIBarButtonItem) {
        self.closeKeyboard(sender)
        self.displayResetConfirmAlert()
    }
    
    // MARK: - BMLTAdminCommunicatorDataSinkProtocol Methods -
    /* ################################################################## */
    /**
     The set changes response callback.
     
     :param: inHandler The handler for this call.
     :param: inResponseData The data from the URL request. If nil, the call failed to produce. Check the handler's error data member.
     :param: inError Any errors that occurred
     :param: inRefCon The data/object passed in via the 'refCon' parameter in the initializer.
     */
    override func responseData(_ inHandler: BMLTAdminCommunicator?, inResponseData: Data?, inError: Error?, inRefCon: AnyObject?) {
        DispatchQueue.main.async(execute: {
            self.hideSavingActivity()
            if (nil != inError) {   // This message is if there was some communication error.
                self.hideSavingActivity()
                BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-COMM-FAIL-TITLE", inMessage: "LOCAL-ERROR-COMM-FAILURE-MESSAGE")
            } else {
                do {
                    let jsonObject = try JSONSerialization.jsonObject(with: inResponseData!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    let newMeetingObject = jsonObject["newMeeting"] as! NSDictionary
                    let idObject = newMeetingObject["id"] as! NSNumber
                    let id: Int = Int(idObject.int32Value)
                    if 0 < id {
                        self.meeting = nil
                        self.compMeeting = nil
                        self.meetingDirty = false
                        self.newMeeting = false
                        self.setMeetingData()
                        BMLTAdminAppDelegate.v_searchController.reloadAndSelectMeetingByID(id)
                    } else {
                        BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-CONN-FAIL-TITLE", inMessage: "LOCAL-ERROR-CONN-FAILURE-MESSAGE")
                    }
                } catch {
                    BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-CONN-FAIL-TITLE", inMessage: "LOCAL-ERROR-CONN-FAILURE-MESSAGE")
                }
            }
        })
    }
}
