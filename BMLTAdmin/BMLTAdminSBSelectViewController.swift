//
//  BMLTAdminSBSelectViewController.swift
//  BMLTAdmin
/**
    :license:
    Created by MAGSHARE.
    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BMLT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this code.  If not, see <http://www.gnu.org/licenses/>.
*/

import UIKit

/* ###################################################################################################################################### */
/**
    This is the class that controls the Service Body Selection Screen. Each Service body that the user is allowed to edit is presented
    in a table, as a checkbox and a name.

    This screen works by displaying the Service bodies the logged-in user is allowed to edit in a hierarchical fashion.
    If a Service body "contains" other Service bodies, they will have their checkbox indented, and their state will be controlled
    by their "parent" Service body checkbox.
*/
class BMLTAdminSBSelectViewController : UIViewController, UITableViewDataSource {
    // MARK: - Instance Properties -
    /** This is the key for the selected Service bodies  */
    let s_selectedServiceBodiesKey: String  =  "BMLTAdmin.Key.SelectedServices"

    /** This is how many pixels to indent a checkbox that is a "child" of another checkbox. */
    let sIndentDepthInPixels: CGFloat                               = 26
    
    /** This is the organized grouping of our Service bodies. */
    static var serviceBodies: [BMLTAdminServiceBody?]!  = nil
    
    // MARK: - Instance Properties (IB) -
    /** This is the instructions prompt. */
    @IBOutlet weak var promtTextField: UITextView!
    /** This is the table where the checkboxes are displayed. */
    @IBOutlet weak var displayTableView: UITableView!
    
    // MARK: - Static Properties -
    /** This will hold our checkbox objects in an easy-to-index form.
        The string key will be the Service body ID as an Int. */
    static var sCheckboxObjects: [Int:BMLTAdminServiceBodyCheckbox] = Dictionary()
    
    // MARK: - Class Methods -
    /* ################################################################## */
    /**
     This class method returns a Service body object, given its BMLT ID.
     
     :param: inID This is the BMLT ID of the Service body that you want.
     :param: inServiceBodies This is a straight array of Service body objects to examine.
     
     :returns: The BMLTAdminServiceBody that represents the requested Service body.
     */
    static func getServiceBodyByID(_ inID: Int, inServiceBodies: [BMLTAdminServiceBody]) -> BMLTAdminServiceBody! {
        var ret: BMLTAdminServiceBody! = nil
        
        for serviceBody in inServiceBodies {
            if(inID == serviceBody.id) {
                ret = serviceBody
                break
            }
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This function checks the various checkboxes, then constructs a Dictionary
     based on them. It saves that in the app static Dictionary.
     It will also update the stored Service bodies.
     
     :returns: a Bool, false, if there are 0 checkboxes checked.
     */
    static func updateFromServiceBodyCheckboxes() -> Bool {
        var sbIDs: [Int] = []
        
        for sb in self.serviceBodies {
            let sbID: Int = sb!.id
            if let sbObject = self.sCheckboxObjects[sbID] {
                if sbObject.checked {
                    sbIDs.append(sbID)
                }
            }
        }
        
        BMLTAdminConnectViewController.storeServiceBodyIDs(BMLTAdminConnectViewController.v_loginURI, inLoginID: BMLTAdminConnectViewController.v_loginID, inServiceBodyIDs: sbIDs)
        BMLTAdminConnectViewController.v_userServiceBodyIDs = sbIDs
        BMLTAdminAppDelegate.v_searchController.needReload = true
        BMLTAdminAppDelegate.v_deletedController.appDisconnected = true
        
        return true
    }
    
    // MARK: - Embedded Classes -
    /* ################################################################## */
    /**
        This class represents one Service body to be displayed and represented.
    */
    class BMLTAdminServiceBody {
        // MARK: - Instance Properties -
        /** This is the Service body ID. */
        var id: Int                             = 0
        /** This is the entire dataset for the Service body (as a JSON object)*/
        var data: [String:String]!              = nil
        /** This points to any parent instances that "own" this Service body. */
        var parent: BMLTAdminServiceBody!       = nil
        /** These are Service body instances "owned" by this Service body. */
        var children: [BMLTAdminServiceBody]!   = nil
        
        // MARK: - Initializers -
        /* ############################################################## */
        /**
            Designated Inititializer. We extract the ID from the data
            that is derived from the JSON object on record.
        */
        required init(inServiceBodyRecord:[String:String]) {
            self.data = inServiceBodyRecord
            if(nil != self.data["id"]) {
               self.id = Int ( self.data["id"]! )!
            }
        }
    }
    
    /* ###################################################################################################################################### */
    /**
        This is a derived class of the standard checkbox that adds information directly applicable to Service bodies.
    */
    class BMLTAdminServiceBodyCheckbox: BMLTAdminCheckbox {
        // MARK: - Instance Properties -
        /** The ID of the Service body to which this checkbox is assigned. */
        let serviceBody: BMLTAdminServiceBody!
        
        /* ############################################################## */
        /**
         This is the boolean variable that indicates whether or not the control
         is checked. We override it, so we can intercept the setter.
         */
        override var checked: Bool {
            get {
                return super.checked
            }
            /* ############################################################## */
            /**
             This is the set function. We intercept it, so that we can cascade
             the value to its "children."
             */
            set {
                super.checked = newValue
                if(nil != self.serviceBody.children) {
                    for child in self.serviceBody.children {
                        if let checkbox = BMLTAdminSBSelectViewController.sCheckboxObjects[child.id] {
                            checkbox.checked = newValue
                        }
                    }
                }
                
                self.sendActions(for: UIControlEvents.valueChanged)
                self.setNeedsLayout()
            }
        }
        
        // MARK: - Initializers -
        /* ############################################################## */
        /**
            Designated Inititializer. We extract the ID from the data
        */
        init(frame: CGRect, inServiceBody: BMLTAdminServiceBody!) {
            self.serviceBody = inServiceBody
            super.init(frame: frame)
            // We will be looking for kids later, so we keep the static variable up to date with who we are.
            BMLTAdminSBSelectViewController.sCheckboxObjects.updateValue(self, forKey: inServiceBody.id)
            super.checked = BMLTAdminConnectViewController.isServiceBodyValid(inServiceBody.id)
        }

        /* ############################################################## */
        /**
         This is the NSCoder init. We intercept it, as it is the one called
         when reconstructing from IB, and we need to clear the Service body.
        */
        required init?(coder aDecoder: NSCoder) {
            self.serviceBody = nil
            super.init(coder: aDecoder)
        }
    }
    
    // MARK: - Instance Methods -
    /* ################################################################## */
    /**
        This method sorts through all of the available Service bodies, and
        makes a local copy of them. It also links up the hierarchy, and stores
        the data in a local class instance array.
    */
    func arrangeServiceBodies() {
        let keys = BMLTAdminAppDelegate.v_serviceBodyData.keys
        var newServiceBodies: [BMLTAdminServiceBody]    = []
        
        // We read in all the availalbe (authorized) Service bodies, and add them to our simple array.
        for key in keys {
            if let serviceBody = BMLTAdminAppDelegate.v_serviceBodyData[key] {
                newServiceBodies.append(BMLTAdminServiceBody(inServiceBodyRecord: serviceBody))
            }
        }
        
        // What we do next, is assign the singular parent object to each Service body (if one is necessary)
        for serviceBody in newServiceBodies {
            if let parentString = serviceBody.data["parent_id"] {
                if let parentID = Int ( parentString ) {
                    // This may return nil if the parent is not a Service body we are allowed to edit.
                    serviceBody.parent = type(of: self).getServiceBodyByID(parentID, inServiceBodies: newServiceBodies)
                }
            }
        }
        
        // Then we go back through, and sort out the (maybe multiple) child Service bodies (if necessary).
        for serviceBody in newServiceBodies {
            for childBody in newServiceBodies {
                if let parent = childBody.parent {
                    if(parent.id == serviceBody.id) {
                        if(nil == serviceBody.children) {
                            serviceBody.children = []
                        }
                        serviceBody.children.append(childBody)
                    }
                }
            }
        }
        
        // Here, we will arrange in a vertically hierarchical form. This is so we can have a simple linear row count, without the need to crawl a tree.
        var temSB: [BMLTAdminServiceBody] = []
        
        // The first thing that we do, is add the top-level Service bodies to our Array.
        for serviceBody in newServiceBodies {
            if(nil == serviceBody.parent) {
                temSB.append(serviceBody)
            }
        }
        
        // Sort them by ID.
        temSB.sort(by: {$0.id < $1.id})
        
        type(of: self).serviceBodies = []
        
        // We now fill out the hierarchies.
        for serviceBody in temSB {
            self.sortServiceBodies(serviceBody)
        }
    }
    
    /* ################################################################## */
    /**
        This is a recursive method that builds the serviceBodies array.
        It adds the given Service body, then crawls the children.
        Ugh. That sounds like Creepy Uncle Ernie, doesn't it?
    */
    func sortServiceBodies(_ inServiceBodyParent: BMLTAdminServiceBody) {
        type(of: self).serviceBodies.append(inServiceBodyParent)
        
        if(nil != inServiceBodyParent.children) {
            inServiceBodyParent.children.sort(by: {$0.id < $1.id})
            for serviceBody in inServiceBodyParent.children {
                self.sortServiceBodies(serviceBody)
            }
        }
    }
    
    /* ################################################################## */
    /**
        This method dynamically creates a checkbox object to correspond to the given Service body.
     
        :param: inServiceBody The Service body object to be used as the "inspiration" for this checkbox.
        :param: inFrame The frame, in local Container View coordinates, for the Checkbox
     
        :returns: BMLTAdminServiceBodyCheckbox object, set up with the Service body, and ready for placement.
    */
    func generateCheckbox(_ inServiceBody: BMLTAdminServiceBody!, inFrame: CGRect) -> BMLTAdminServiceBodyCheckbox! {
        var ret: BMLTAdminServiceBodyCheckbox! = nil
        var indentLevel: CGFloat = -1
        var serviceBody: BMLTAdminServiceBody! = inServiceBody
        while(nil != serviceBody) {
            serviceBody = serviceBody.parent
            indentLevel += 1
        }
        
        let newFrame: CGRect = CGRect(x: indentLevel * sIndentDepthInPixels, y: (inFrame.height - 26.0) / 2.0, width: 26.0, height: 26.0)
        
        ret = BMLTAdminServiceBodyCheckbox(frame: newFrame, inServiceBody: inServiceBody)
        return ret
    }
    
    // MARK: - Overridden Methods -
    /* ################################################################## */
    /**
        This is called after the view has been loaded.
        We override it in order to set the text item values for the main text items.
    */
    override func viewDidLoad() {
        super.viewDidLoad()

        if let controller = self.navigationController {
            controller.isNavigationBarHidden = false
            self.navigationItem.title = NSLocalizedString(self.navigationItem.title!, comment: "")
        }
        
        if let textView = self.promtTextField {
            textView.text = NSLocalizedString(textView.text, comment: "")
        }
    }
    
    /* ################################################################## */
    /**
        This is called just prior to the view disappearing.
        We override it in order to save our state.
    */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        _ = BMLTAdminSBSelectViewController.updateFromServiceBodyCheckboxes()
        BMLTAdminAppDelegate.setConnectionStatus()
        
        if let controller = self.navigationController {
            controller.isNavigationBarHidden = true
        }
    }
    
    /* ################################################################## */
    /**
        This is called just prior to the view laying out the subviews.
        We override it in order to set up our checkboxes and whatnot.
    */
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.arrangeServiceBodies()
        self.displayTableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource Protocol Methods -
    /* ################################################################## */
    /**
        This returns the number of Service bodies we'll need to display.
     
        :param: tableView The Table View
        :param: section The 0-based index of the section.
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (nil == type(of: self).serviceBodies) ? 0 : type(of: self).serviceBodies.count
    }
    
    /* ################################################################## */
    /**
        This returns one cell for a Service body.

        :param: tableView The Table View
        :param: indexPath The index path to the cell we need.
     
        :returns:   one cell object for the given Service body.
                    This cell will contain the checkbox control, along with a label, with the Service body name.
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ret: UITableViewCell! = nil
        if let serviceBody = type(of: self).serviceBodies[(indexPath as NSIndexPath).row] {
            ret = tableView.dequeueReusableCell(withIdentifier: String ( serviceBody.id )) as UITableViewCell!
            
            if(nil == ret){
                ret = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: serviceBody.data["id"])
                // We do this little dance, so we don't have magic numbers.
                let sbHeightString = NSLocalizedString("LOCAL-SB-CHECKBOX-HEIGHT-IN-DISPLAY-UNITS", comment: "")
                if !sbHeightString.isEmpty {
                    let height: CGFloat = CGFloat ( Int ( sbHeightString )! )
                    var frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: height)
                    
                    let checkBox = self.generateCheckbox(serviceBody, inFrame: frame)
                    ret.addSubview(checkBox!)
                    
                    ret.backgroundColor = UIColor.clear
                    
                    frame.origin.x = ((checkBox?.frame.origin.x)! + (checkBox?.frame.size.width)! + 4)
                    frame.size.width -= frame.origin.x
                    
                    let label = UILabel(frame: frame)
                    label.backgroundColor = UIColor.clear
                    label.text = serviceBody.data["name"]
                    label.adjustsFontSizeToFitWidth = true
                    ret.addSubview(label)
                }
            }
        }
        
        return ret
    }
}
