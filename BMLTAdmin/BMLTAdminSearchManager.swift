//
//  BMLTAdminSearchManager.swift
//  BMLTAdmin
/**
    :license:
    Created by MAGSHARE.
    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BMLT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this code.  If not, see <http://www.gnu.org/licenses/>.
*/

import Foundation

/* ###################################################################################################################################### */
/**
    This extends the communicator protocol to add a callback for meeting search results.
*/
protocol BMLTAdminSearchManagerDataSinkProtocol : BMLTAdminCommunicatorDataSinkProtocol {
    /* ################################################################## */
    /**
        This is the completion block definition for the search results.
        
        :param: inSearchManager The Search manager object that executed the search.
        :param: inMeetingIDs an optional array of Int. This contains the id_bigint for the meetings turned up by the search. Nil for errors or no result.
                Check the inSearchManager.searchResults property for the full JSON response.
        :param: inError If there was an error, this optional parameter will be non-nil, and contain the error response.
    */
    func searchResponse(_ inSearchManager: BMLTAdminSearchManager?, inMeetingIDs: [Int]?, inError: Error?)
}

/* ###################################################################################################################################### */
/**
    This class is a base class that specializes the communicator for the functionality of the Search screen. It contains common properties.
 
    In order to use a Search Manager, you create one explicitly for a search. It is not meant to be stable and reused. Just one for each search.
*/
class BMLTAdminSearchManager : BMLTAdminCommunicator {
    // MARK: - Static Data Members
    /** This is the fundamental prefix for the search URI. */
    static let sPathPrefix: String  = "/client_interface/json/?switcher=GetSearchResults&advanced_published=0"
    /** This is the prefix for the weekday selection. You need one of these for each weekday. */
    static let sServicesPrefix: String  = "&services[]="
    /** This is the prefix for the weekday selection. You need one of these for each weekday. */
    static let sWeekdaysPrefix: String  = "&weekdays[]="
    /** This is the prefix for the "Starts Before" hours. It should be followed by an integer, 0-23 */
    static let sStartsBeforeHPrefix: String  = "&StartsBeforeH="
    /** This is the prefix for the "Starts After" hours. It should be followed by an integer, 0-23 */
    static let sStartsAfterHPrefix: String  = "&StartsAfterH="
    /** This is the prefix for the "Starts After" minutes. It should be followed by an integer, 0-59 */
    static let sStartsAfterMPrefix: String  = "&StartsAfterM="
    /** This is the value for the "Starts Before" hours for "Early" meetings. */
    static let sEarlyStartsBeforeH: String  = "13"
    /** This is the value for the "Starts Before" minutes for "Early" meetings. */
    static let sLateStartsAfterH: String  = "12"
    /** This is the value for the "Starts After" minutes for "Late" meetings. */
    static let sLateStartsAfterM: String  = "59"
    /** This is the value for the Search String prefix */
    static let sSearchStringPrefix: String = "&SearchString="
    /** This is the value for the "Search String is an Address" flag */
    static let sSearchStringLocation: String = "&SearchStringIsAnAddress=1"

    // MARK: - Enums
    /* ################################################################## */
    /**
        These are used for the meeting start time selection (all day, before noon, after noon).
    */
    enum MeetingStartTime: Int {
        case allDay = 0, early, late
    }
    
    // MARK: - Instance Properties
    /** This holds which Service bodies we'll be looking at. Default is all of the available ones. */
    var selectedServiceBodyIDs: [Int]!          = nil
    /** This holds which weekdays will be selected for the search. Default is every one selected. */
    var selectedWeekdays: [Bool]                = [true, true, true, true, true, true, true]
    /** This contains a bias as to the time of day the meeting starts. Default is all day. */
    var startTime: MeetingStartTime             = .allDay
    /** This contains any string we're searching for. */
    var searchString: String                    = ""
    /** This contains a flag that is true if the search string indicates a geographic location. */
    var searchStringIsALocation: Bool           = false
    /** This will contain an array of JSON objects that contains the meeting search results. */
    var searchResults: [Int:[String:String]]!   = nil
    /** This will contain an Array of String, with all the town names. */
    var resultTowns: [String]!                  = nil
    
    /* ################################################################## */
    /**
        This is the designated initializer for this class. You have to give
        a URL, as well as a delegate (sink) and data source.
        This class executes the connection upon initialization.
    
        :param: inURI The URL to be called, as a string. This points to the Root Server base.
        :param: inDataSource The data source.
        :param: inDelegate The delegate object (the sink) to be notified when new data arrives.
        :param: inRefCon This is any object or data that you want to send to the delegate
    */
    init(dataSource inDataSource:BMLTAdminCommunicatorDataSourceProtocol, delegate inDelegate:BMLTAdminCommunicatorDataSinkProtocol, refCon inRefCon: AnyObject! = nil) {
        // We do not execute immediately. Remember that.
        super.init("", dataSource: inDataSource, delegate: inDelegate, refCon: inRefCon, executeImmediately: false)
    }

    // MARK: - Instance Methods
    /* ################################################################## */
    /**
     This goes through the results, and extracts the towns and states, putting them together.
     It will use boroughs, instead of towns, if it finds them.
     The result is stored in the resultTowns property.
     
     :returns: an Array of String, the contents of the resultTowns property. Nil, if no results.
     */
    func extractTowns() -> [String]! {
        var ret: [String]! = nil
        
        if (nil != self.searchResults) && (0 < self.searchResults.count) {
            for meeting in self.searchResults {
                var town: String = ""
                
                if let borough = meeting.1["location_city_subsection"] {
                    if !borough.isEmpty {
                        town = borough
                    }
                    
                    if borough.isEmpty {
                        if let townName = meeting.1["location_municipality"] {
                            if !townName.isEmpty {
                                town = townName
                            }
                            town = townName
                        }
                    }
                    
                } else {
                    if let townName = meeting.1["location_municipality"] {
                        if !townName.isEmpty {
                            town = townName
                        }
                        town = townName
                    }
                }
                
                if let state = meeting.1["location_province"] {
                    if !town.isEmpty {
                        town += (", " + state)
                    }
                }
                
                if !town.isEmpty && !((nil != ret) && ret.contains(town)) {
                    if nil == ret {
                        ret = [town]
                    } else {
                        ret.append(town)
                    }
                }
            }
        }
        
        if nil != ret {
            ret.sort(by: {$0 < $1})
        }
        
        self.resultTowns = ret
        
        return ret
    }

    /* ################################################################## */
    /**
        Returns the URL to execute a search.
    
        :returns: a String. The first part of the search URL.
    */
    func searchURLAsAString() -> String {
        var path: String = ""   // This will be the response.
        
        var scramTheReactor: Bool = true    // This is a "dead man's switch." It will prevent the user from doing searches if there are no Service bodies they are allowed to edit.
        
        // This app only allows the user to search for meetings they are cleared to administer.
        for id in BMLTAdminConnectViewController.getSelectedServiceBodyIDs() {
            if BMLTAdminConnectViewController.isServiceBodyValid(id) {
                scramTheReactor = false
                break
            }
        }
        
        // If we haven't had any Service bodies explicitly set, we just grab all the ones the user is editing.
        if !scramTheReactor && ((nil == self.selectedServiceBodyIDs) || (1 > self.selectedServiceBodyIDs.count)) {
            self.selectedServiceBodyIDs = BMLTAdminConnectViewController.getSelectedServiceBodyIDs()
        }
        
        // If we are allowed to edit here (in truth, we should never get this far if we aren't allowed, but belt and suspenders), then we proceed.
        if !scramTheReactor && (nil != self.selectedServiceBodyIDs) {
            for id in self.selectedServiceBodyIDs {
                if BMLTAdminConnectViewController.isServiceBodyValid(id) {
                    path += type(of: self).sServicesPrefix + String(id)
                }
            }
            
            path = BMLTAdminAppDelegate.v_rootServerBaseURI + type(of: self).sPathPrefix + path
            
            // Check for any selected weekdays.
            var temp: String = "";
            var allDays: Bool = true
            
            for c in 0 ..< 7 {
                if(self.selectedWeekdays[c]) {
                    temp += type(of: self).sWeekdaysPrefix + String(c + 1)  // Weekday indexes are 1-based.
                }
                else {
                    allDays = false // Oops. found a hole.
                }
            }
            
            // We don't need to specify anything if we have every day checked, so only append temp if we had a hole.
            if(!allDays) {
                path += temp
            }
            
            // See if the user wants a particular time range.
            switch(self.startTime) {
            case .early:
                path += type(of: self).sStartsBeforeHPrefix + type(of: self).sEarlyStartsBeforeH // 1PM is the boundary between early and late.
            case .late:
                path += (type(of: self).sStartsAfterHPrefix + type(of: self).sLateStartsAfterH + type(of: self).sStartsAfterMPrefix + type(of: self).sLateStartsAfterM)
            case .allDay:
                break   // No need to do anything if we are all day.
            }
            
            // If we have a string specified, then we append that.
            if !searchString.isEmpty {
                path += (type(of: self).sSearchStringPrefix + searchString.URLEncodedString()!)
            }
            
            // See if we have a location.
            if self.searchStringIsALocation {
                path += type(of: self).sSearchStringLocation
            }
        }
        
        return path
    }
    
    /* ################################################################## */
    /**
        This actually executes the connection.
    */
    override func execute() {
        _ = self.dataSource.callURI(self, inURIAsAString: self.searchURLAsAString(), inCompletionBlock: self.handleResponseFromHandler)
    }
    
    /* ################################################################## */
    /**
        This is the callback for the URL request.
    
        We will partially parse the JSON here, and store it as an object.
        We will also get all of the meeting IDs, and return them in an array
        to the second callback provided by the delegate.
     
        :param: inData the data from the URL request.
        :param: inError Any error that ocurred. Nil, if no error.
    */
    override func handleResponseFromHandler(_ inData: Data?, inError: Error?) {
        self.searchResults = nil
        
        super.handleResponseFromHandler(inData, inError: inError)
        
        // What we do, is unwind the meeting response. The meeting values and field keys are all given as strings, so we can cast to a dictionary.
        if(nil != self.delegate) {
            // Make sure the delegate implements the extension protocol.
            if let delegate = self.delegate as? BMLTAdminSearchManagerDataSinkProtocol {
                var meetingIDs: [Int]! = nil
                
                // We carefully unwind the JSON response.
                if(nil != inData) {
                    do {
                        let jsonObject: AnyObject? = try JSONSerialization.jsonObject(with: inData!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSArray
                        
                        if ( nil != jsonObject ) {
                            if(0 < jsonObject!.count) {
                                for c in 0 ..< jsonObject!.count {
                                    if let object = jsonObject!.object(at: c) as? NSDictionary {
                                        if let meetingObject = object as? [String:String] {
                                            if let id = meetingObject["id_bigint"] {
                                                if let idInt = Int ( id ) {
                                                    // We only allocate this if we have at least one good meeting.
                                                    if(nil == self.searchResults) {
                                                        self.searchResults = [Int:[String:String]]()
                                                    }
                                                    self.searchResults.updateValue(meetingObject, forKey: idInt)
                                                    // Same deal here.
                                                    if(nil == meetingIDs) {
                                                        meetingIDs = []
                                                    }
                                                    meetingIDs.append( Int ( id )!)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch {
                        BMLTAdminAppDelegate.displayErrorAlert("LOCAL-ERROR-CONN-FAIL-TITLE", inMessage: "LOCAL-ERROR-CONN-FAILURE-MESSAGE")
                    }
                }
                
                _ = self.extractTowns()
                
                // At this point, self.searchResults is an Int-keyed dictionary of meeting objects (dictionaries of String:String), indexed by the meeting ID.
                // We will pass in an array of Int meeting IDs, which can be used as keys for their meeting objects.
                DispatchQueue.main.async(execute: {
                    delegate.searchResponse(self, inMeetingIDs: meetingIDs, inError: inError)
                    BMLTAdminAppDelegate.setConnectionStatus()  // Update the app delegate.
                })
            }
        }
    }
}
