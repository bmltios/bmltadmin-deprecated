//
//  BMLTAdminSearchViewController.swift
//  BMLTAdmin
/**
    :license:
    Created by MAGSHARE.
    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BMLT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this code.  If not, see <http://www.gnu.org/licenses/>.
*/

import UIKit
import MapKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


/* ###################################################################################################################################### */
/**
 This class describes one table cell for the Search display.
 */
class BMLTAdminSearchViewControllerTableCell: UITableViewCell {
    // MARK: - Instance Constants -
    /** This is used to key the encoder for this class. */
    let sMeetingObjectKey: String = "BMLTAdminSearchViewControllerTableCell_Meeting"
    let sWeekdays: [String] = [ NSLocalizedString("LOCAL-WEEKDAY-SUN", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-MON", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-TUE", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-WED", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-THU", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-FRI", comment: ""),
                                NSLocalizedString("LOCAL-WEEKDAY-SAT", comment: "")]

    // MARK: - Public Standard Properties -
    /** This will hold the meeting object represented by this cell. */
    var meeting: BMLTAdminMeeting = [:]
    
    // MARK: - IB Properties -
    /** We use an attributed text cell to display the meeting information. */
    /** The label that displays the day of week. */
    @IBOutlet weak var weekdayLabel: UILabel!
    /** The label that displays the meeting start time. */
    @IBOutlet weak var timeLabel: UILabel!
    /** The label that displays the name of the meeting. */
    @IBOutlet weak var meetingNameLabel: UILabel!
    /** The label that displays the town and Service body. */
    @IBOutlet weak var additionalInfoLabel: UILabel!
    /** This is the container view. */
    @IBOutlet weak var mainView: UIView!
    
    // MARK: - Designated Initializer -
    /* ################################################################## */
    /**
     :param: inMeeting The meeting object to be associated with this cell.
     :param: style The table style we will use.
     :param: reuseIdentifier The ID associated with this instance for reuse later.
     */
    init(inMeeting: BMLTAdminMeeting, style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.meeting = inMeeting
    }
    
    // MARK: - Override Methods -
    /* ################################################################## */
    /**
     In the layout function, we read the various strings from the meeting, and place them into the appropriate label objects.
     */
    override func layoutSubviews() {
        super.layoutSubviews()
        if let weekdayIndex = self.meeting["weekday_tinyint"] {
            let index = Int(weekdayIndex)! - 1
            switch index {
            case 0...6:
                self.weekdayLabel.text = self.sWeekdays[index]
            default:
                self.weekdayLabel.text = ""
            }
        }
        
        if "0" == self.meeting["published"] {
            self.backgroundColor = UIColor.orange
        } else {
            self.backgroundColor = UIColor.clear
        }
        
        // We make a fairly "natural" display of the start time.
        if let time = self.meeting["start_time"] {
            let timeArray = time.components(separatedBy: ":").map { Int($0) }
            
            if 1 < timeArray.count {
                var hours: Int = timeArray[0]!
                let minutes: Int = timeArray[1]!
                var label: String = ""
                
                if ((0 == hours) && (0 == minutes)) || ((23 == hours) && (55 < minutes)) {
                    label = NSLocalizedString("LOCAL-TIME-MIDNIGHT", comment: "")
                } else {
                    if (12 == hours) && (0 == minutes) {
                        label = NSLocalizedString("LOCAL-TIME-NOON", comment: "")
                    } else {
                        let isMilitaryString = NSLocalizedString("LOCAL-TIME-IS-MILITARY", comment: "")
                        if "0" == isMilitaryString {
                            let isPM = hours > 11
                            
                            if isPM && (hours > 12) {
                                hours -= 12
                            }
                            var amPm = ""
                            
                            if isPM {
                                amPm = NSLocalizedString("LOCAL-TIME-PM", comment: "")
                            } else {
                                amPm = NSLocalizedString("LOCAL-TIME-AM", comment: "")
                            }
                            
                            var min = ""
                            
                            if 10 > minutes {
                                min += "0"
                            }
                            
                            label = String(hours) + ":" + min + String(minutes) + " " + amPm
                        } else {
                            var min = ""
                            
                            if 10 > minutes {
                                min += "0"
                            }
                            
                            label += String(hours) + ":" + min + String(minutes)
                        }
                    }
                }
                
                self.timeLabel.text = label
            }
        }
        
        if let name = self.meeting["meeting_name"] {
            self.meetingNameLabel.text = name
        }
        
        var extraData: String = ""
        
        // We prefer boroughs to towns.
        if let meetingTown = self.meeting["location_municipality"] {
            if let meetingBorough = self.meeting["location_city_subsection"] {
                if !meetingBorough.isEmpty {
                    extraData += meetingBorough
                } else {
                    extraData += meetingTown
                }
            } else {
                extraData += meetingTown
            }
        }
        
        // If there are more than one Service body, we indicate which one after the town/borough.
        if 1 < BMLTAdminConnectViewController.v_userServiceBodyIDs.count {
            if let sbIDString = self.meeting["service_body_bigint"] {
                if let sbInt: Int = Int(sbIDString) {
                    if let sbData = BMLTAdminAppDelegate.v_serviceBodyData[sbInt] {
                        if let name = sbData["name"] {
                            extraData += " (" + name + ")"
                        }
                    }
                }
            }
        }
        
        self.additionalInfoLabel.text = extraData
    }
    
    // MARK: - NSCoding Protocol Methods -
    /* ################################################################## */
    /**
     This method will restore its meeting object from the coder passed in.
     
     :param: aDecoder The coder that will contain the meeting.
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let tempDecoded = aDecoder.decodeObject(forKey: sMeetingObjectKey) as! [String:String]! {
            self.meeting = tempDecoded
        }
    }
    
    /* ################################################################## */
    /**
     This method saves its meeting as part of it serialization.
     
     :param: aCoder The coder that contains the meeting.
     */
    override func encode(with aCoder: NSCoder) {
        aCoder.encode(self.meeting, forKey: sMeetingObjectKey)
        super.encode(with: aCoder)
    }
}

/* ###################################################################################################################################### */
/**
    This class manages the Search screen.
 
    The way this screen works, is that we preload all of the meetings available to the admin as the view is loaded. Once that is done, they
    are displayed in a table.
    The search specifiers will work on the found set (instant response), and narrow down the selection.
    Currently, the only specifiers are weekday and town (which includes boroughs).
    If the user taps on a table row, it will open an editor for that row.
*/
class BMLTAdminSearchViewController: BMLTAdminBaseViewController, BMLTAdminSearchManagerDataSinkProtocol, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIScrollViewDelegate, UIPickerViewDataSource, CLLocationManagerDelegate {
    let sSegueID: String = "sendInTheClowns"
    
    /** This is used to determine if we are near a meeting. */
    let sProximityDistanceInMeters: CLLocationDistance = 100
    
    /** This is used to determine if we have dragged the scroll enough to rate a reload. */
    let sScrollToReloadThreshold: CGFloat = -80
    
    /** This is used to determine if we are near a meeting start time. This is applied before and after the meeting time. */
    let sMeetingTimeWindowInMinutes: Int = 15

    // MARK: - Public Standard Properties -
    /** This will handle our searches. */
    var searchManager: BMLTAdminSearchManager! = nil
    
    /** This will be the filter we use to manage the display */
    var filter: BMLTAdminMeetingFilter! = nil

    /** This should be set to true if a reload is necessary. */
    var needReload: Bool = true
    
    /** This will contain all the towns we found. */
    var resultTowns: [String]! = nil
    
    /** This tracks the currently selected table row. */
    var currentlySelectedIndexPath: IndexPath = IndexPath()
    
    /** This is a semaphore to prevent multiple updates. */
    var handlingLocationUpdate: Bool = false
    
    /** This will hold our location manager. */
    var locationManager: CLLocationManager! = nil
    
    /** This is used when we do a reload. */
    var reloadID: Int = 0
    
    var meetingHistoryData: [Int:[[String:String]]] = [:]

    // MARK: - Public IB Properties -
    /** These are displayed during meeting data load, and hidden otherwise. */
    @IBOutlet weak var busyLabel: UILabel!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    /** This contains the search items (other than the results table). */
    @IBOutlet weak var searchItemsContainerView: UIView!
    
    /** The weekday checkboxes. */
    @IBOutlet weak var allDaysCheckboxLabel: UILabel!
    @IBOutlet weak var allDaysCheckbox: BMLTAdminCheckbox!
    
    @IBOutlet weak var sundayCheckboxLabel: UILabel!
    @IBOutlet weak var sundayCheckbox: BMLTAdminCheckbox!
    
    @IBOutlet weak var mondayCheckboxLabel: UILabel!
    @IBOutlet weak var mondayCheckbox: BMLTAdminCheckbox!
    
    @IBOutlet weak var tuesdayCheckboxLabel: UILabel!
    @IBOutlet weak var tuesdayCheckbox: BMLTAdminCheckbox!
    
    @IBOutlet weak var wednesdayCheckboxLabel: UILabel!
    @IBOutlet weak var wednesdayCheckbox: BMLTAdminCheckbox!
    
    @IBOutlet weak var thursdayCheckboxLabel: UILabel!
    @IBOutlet weak var thursdayCheckbox: BMLTAdminCheckbox!
    
    @IBOutlet weak var fridayCheckboxLabel: UILabel!
    @IBOutlet weak var fridayCheckbox: BMLTAdminCheckbox!
    
    @IBOutlet weak var saturdayCheckboxLabel: UILabel!
    @IBOutlet weak var saturdayCheckbox: BMLTAdminCheckbox!
    
    @IBOutlet weak var whereAmIButton: UIButton!
    
    /** This is the table that shows the current meetings. */
    @IBOutlet weak var searchResultsTableView: UITableView!
    
    @IBOutlet weak var townPickerView: UIPickerView!
    
    // MARK: - Internal Methods -
    /* ################################################################## */
    /**
     This hides most of the main window items, and displays a busy throbber.
     */
    func _showBusyItems() {
        self.searchItemsContainerView.isHidden = true
        self.searchResultsTableView.isHidden = true
        self.townPickerView.isHidden = true
        self.whereAmIButton.isHidden = true
        self.busyLabel.isHidden = false
        self.activityView.isHidden = false
    }
    
    /* ################################################################## */
    /**
     This hides the busy throbber, and restores the main window.
     */
    func _hideBusyItems() {
        self.searchItemsContainerView.isHidden = false
        self.searchResultsTableView.isHidden = false
        self.townPickerView.isHidden = false
        self.whereAmIButton.isHidden = false
        self.busyLabel.isHidden = true
        self.activityView.isHidden = true
    }
    
    /* ################################################################## */
    /**
     This begins the process of loading all available meetings into the app.
     */
    func _loadMeetings() {
        self.meetingHistoryData = [:]
        self._showBusyItems()
        self.selectedMeeting = nil
        self.currentlySelectedIndexPath = IndexPath()
        self.searchResultsTableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)    // Scroll to top.
        self.searchManager = BMLTAdminSearchManager (dataSource: BMLTAdminAppDelegate.appDelegate().connectionSession, delegate: self, refCon: nil)
        self.searchManager.selectedServiceBodyIDs = BMLTAdminConnectViewController.v_userServiceBodyIDs
        self.searchManager.execute()
    }
    
    /* ################################################################## */
    /**
     This method compares the given location to the ones available in the meeting
     list, and also looks at the time.
     Given these, it tries to see if there's a meeting that corresponds to the
     location and time.
     If it finds that meeting, it opens it up for editing.
     
     :param: coordinate The coordinate that corresponds to our current location.
     */
    func _figureOutWhereWeAre(_ coordinate: CLLocationCoordinate2D, todayNow: Date) {
        let meetingList = self.filter.searchResults
        var found: Bool = false
        
        let myCalendar:Calendar = Calendar.init(identifier: Calendar.Identifier.gregorian)
        let unitFlags: NSCalendar.Unit = [NSCalendar.Unit.hour, NSCalendar.Unit.minute, NSCalendar.Unit.weekday]
        let myComponents = (myCalendar as NSCalendar).components(unitFlags, from: todayNow)
        let weekDay = myComponents.weekday
        let hours = myComponents.hour
        let minutes = myComponents.minute
        let timeInMinutes = (hours! * 60) + minutes!
        if (0 < weekDay) && (0 < meetingList.count) {
            var index = 0
            for aMeeting in meetingList as BMLTAdminMeetingList {
                index += 1
                let id = Int(aMeeting["id_bigint"]!)
                if (nil != aMeeting["weekday_tinyint"]) && (nil != aMeeting["latitude"]) && (nil != aMeeting["longitude"]) && (nil != aMeeting["start_time"]) && (nil != aMeeting["duration_time"]) {
                    if let meetingWeekday = Int(aMeeting["weekday_tinyint"]!) {
                        if weekDay == meetingWeekday {
                            let meetingLocation = CLLocationCoordinate2D(latitude: aMeeting["latitude"]!, longitude: aMeeting["longitude"]!)
                            let distance = coordinate.distanceInMetersFrom(meetingLocation)
                            // See if we are close to the meeting.
                            if distance <= self.sProximityDistanceInMeters {
                                let startTimeInMinutes = BMLTAdminAppDelegate.parseTimeValueAsInteger(aMeeting["start_time"]!) - sMeetingTimeWindowInMinutes
                                let windowTimeInMinutes = BMLTAdminAppDelegate.parseTimeValueAsInteger(aMeeting["duration_time"]!) + (sMeetingTimeWindowInMinutes * 2)
                                let timeInterval: ClosedRange = startTimeInMinutes...(startTimeInMinutes + windowTimeInMinutes)
                                // If we find a meeting in the correct time and weekday, we select it in the table.
                                switch timeInMinutes {
                                case timeInterval:
                                    // Simulate selecting that meeting in our table.
                                    _ = self.selectMeetingByID(id!)
                                    found = true    // Prevent the alert from displaying.
                                    break
                                default:    // Otherwise, just skip it.
                                    continue
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // If we didn't find any meetings, we throw up an alert, saying so.
        if false == found {
            BMLTAdminAppDelegate.displayErrorAlert("LOCAL-SEARCH-WHERE-AM-I-FAIL-TITLE", inMessage: "LOCAL-SEARCH-WHERE-AM-I-FAIL")
        }
    }
    
    // MARK: - Public Methods -
    /* ################################################################## */
    /**
     Scrolls the table to expose the meeting for the given ID.
     
     :param: inID The BMLT ID of the meeting you want exposed.
     
     :returns: The discovered row position.
     */
    func scrollTableToMeeting(_ inID: Int) -> Int {
        var rowPosition: Int = 0
        
        for meeting in self.filter.searchResults {
            if Int(meeting["id_bigint"]!) == inID {
                break
            }
            rowPosition += 1
        }
        
        let index = IndexPath(row: rowPosition, section: 0)
        
        self.searchResultsTableView.scrollToRow(at: index, at: UITableViewScrollPosition.middle, animated: true)
        
        return rowPosition
    }
    
    /* ################################################################## */
    /**
     Scrolls the table to expose the meeting for the given ID, then selects the meeting for editing.
     
     :param: inID The BMLT ID of the meeting you want exposed.
     
     :returns: The discovered row position.
     */
    func selectMeetingByID(_ inID: Int) -> Int {
        let rowPosition: Int = self.scrollTableToMeeting(inID)
        let index = IndexPath(row: rowPosition, section: 0)
        self.tableView(self.searchResultsTableView, didSelectRowAt: index)
        return rowPosition
    }
    
    /* ################################################################## */
    /**
     First, it reloads the meetings, then it selects the given meeting.
     
     Scrolls the table to expose the meeting for the given ID, then selects the meeting for editing.
     
     :param: inID The BMLT ID of the meeting you want exposed.
     */
    func reloadAndSelectMeetingByID(_ inID: Int) {
        _ = self.navigationController?.popToRootViewController(animated: false)
        BMLTAdminAppDelegate.v_tabBarController.selectedIndex = BMLTAdminAppDelegate.ControllerIndex.search.rawValue
        self.reloadID = inID
        self._loadMeetings()
    }
    
    // MARK: - IBAction Handlers -
    /* ################################################################## */
    /**
     This is called when the user hits the "Where Am I Now?" button
     
     :param: sender The button object
     */
    @IBAction func whereAmIButtonHit(_ sender: UIButton) {
        // Ask permission to track the user's location.
        self.handlingLocationUpdate = false
        self.locationManager = CLLocationManager()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
    
    // MARK: - UIScrollViewDelegate Protocol Methods -
    /* ################################################################## */
    /**
     This is called when the scroll view has ended dragging.
     We use this to trigger a reload, if the scroll was pulled beyond its limit by a certain number of display units.
     
     :param: scrollView The text view that experienced the change.
     :param: velocity The velocity of the scroll at the time of this call.
     :param: targetContentOffset We can use this to send an offset to the scroller (ignored).
     */
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if ( (velocity.y < 0) && (scrollView.contentOffset.y < self.sScrollToReloadThreshold) ) {
            self.reloadID = 0
            self._loadMeetings()
        }
    }
    
    // MARK: - CLLocationManagerDelegate Methods -
    /* ################################################################## */
    /**
     This is called if the location manager suffers a failure.
     
     :param: manager The Location Manager object that had the error.
     :param: error The error in question.
     */
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.handlingLocationUpdate = true
        self.locationManager.stopUpdatingLocation()
        self.locationManager.delegate = nil
        self.locationManager = nil
        BMLTAdminAppDelegate.displayErrorAlert("LOCAL-SEARCH-LOCATION-FAIL-TITLE", inMessage: "LOCAL-SEARCH-LOCATION-FAILURE-MESSAGE")
    }
    
    /* ################################################################## */
    /**
     
     :param: manager The Location Manager object that had the event.
     :param: locations an array of updated locations.
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !self.handlingLocationUpdate {
            self.handlingLocationUpdate = true
            self.locationManager.stopUpdatingLocation()
            self.locationManager.delegate = nil
            self.locationManager = nil
            if 0 < locations.count {
                let coordinate = locations[0].coordinate
                DispatchQueue.main.async(execute: {
                    self._figureOutWhereWeAre(coordinate, todayNow: Date())
                })
            }
        }
    }
    
    // MARK: - UIPickerViewDataSource Methods -
    /* ################################################################## */
    /**
     We only have 1 component.
     
     :param: pickerView The UIPickerView being checked
     
     :returns: 1
     */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    /* ################################################################## */
    /**
     We will always have 2 more than the number of towns, as we have the first and second rows.
     
     :param: pickerView The UIPickerView being checked
     
     :returns: Either 0, or the number of towns to be displayed, plus 2.
     */
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if nil == self.resultTowns {
            return 0
        } else {
            return self.resultTowns.count + 2
        }
    }
    
    // MARK: - UIPickerViewDelegate Methods -
    /* ################################################################## */
    /**
     This returns the town name for the given row.
     
     :param: pickerView The UIPickerView being checked
     :param: row The row being checked
     :param: component The component (always 0)
     
     :returns: an attributed String, with the town for that row. The first row is "No Town Selected," and the second is blank.
     */
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var ret: String = ""
        
        if (nil != self.resultTowns) && (1 != row) {
            if 0 == row {
                ret = NSLocalizedString("LOCAL-SEARCH-PICKER-NONE", comment: "")
            } else {
                if nil != self.resultTowns {
                ret = self.resultTowns[row - 2]
                }
            }
        }
        
        return NSAttributedString(string: ret, attributes: [NSForegroundColorAttributeName:UIColor.white])
    }
    
    /* ################################################################## */
    /**
     This is called when the user finishes selecting a row.
     We use this to add the selected town to the filter.
     
     If it is one of the top 2 rows, we select the first row, and ignore it.
     
     :param: pickerView The UIPickerView being checked
     :param: row The row being checked
     :param: component The component (always 0)
     */
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if 1 < row {
            _ = self.filter.addFilter(BMLTAdminMeetingFilter.FilterKeys.Town, inFilterValue: self.resultTowns[row - 2])
        } else {
            self.filter.clearFilter(BMLTAdminMeetingFilter.FilterKeys.Town)
            pickerView.selectRow(0, inComponent: 0, animated: true)
        }
        self.searchResultsTableView.reloadData()
        BMLTAdminAppDelegate.setConnectionStatus()  // Update the app delegate.
    }
    
    // MARK: - BMLTAdminSearchManagerDataSinkProtocol Methods -
    /* ################################################################## */
    /**
     This is the completion block definition for the search results.
     
     :param: inSearchManager The Search manager object that executed the search.
     :param: inMeetingIDs an optional array of Int. This contains the id_bigint for the meetings turned up by the search. Nil for errors or no result.
     Check the inSearchManager.searchResults property for the full JSON response.
     :param: inError If there was an error, this optional parameter will be non-nil, and contain the error response.
     */
    func searchResponse(_ inSearchManager: BMLTAdminSearchManager?, inMeetingIDs: [Int]?, inError: Error?) {
        self._hideBusyItems()
        BMLTAdminAppDelegate.v_searchResults = nil
        if nil != inSearchManager {
            BMLTAdminAppDelegate.v_searchResults = inSearchManager!.searchResults
            var searchResults: BMLTAdminMeetingList = []
            
            for meeting in BMLTAdminAppDelegate.v_searchResults {
                searchResults.append(meeting.1)
            }
            
            /* ###################################################### */
            /**
             This contains a sort block. It sorts on weekday first, then start time,
             then town (alphabetically), then borough (alphabetically), then Service body (by ID)
             */
            searchResults.sort(by: {
                var ret: Bool = false
                
                let meetingA = $0 as BMLTAdminMeeting
                let meetingB = $1 as BMLTAdminMeeting
                
                if let weekdayA = meetingA["weekday_tinyint"] {
                    if let weekdayB = meetingB["weekday_tinyint"] {
                        if Int(weekdayA) < Int(weekdayB) {
                            ret = true
                        } else {
                            if Int(weekdayA) == Int(weekdayB) {
                                if let timeA = meetingA["start_time"] {
                                    if let timeB = meetingB["start_time"] {
                                        let timeArrayA = timeA.components(separatedBy: ":").map { Int($0) }
                                        let timeArrayB = timeB.components(separatedBy: ":").map { Int($0) }
                                        
                                        if timeArrayA[0] < timeArrayB[0] {
                                            ret = true
                                        } else {
                                            if timeArrayA[0] == timeArrayB[0] {
                                                if timeArrayA[1] < timeArrayB[1] {
                                                    ret = true
                                                } else {
                                                    if timeArrayA[1] == timeArrayB[1] {
                                                        if let meetingATown = meetingA["location_municipality"] {
                                                            if let meetingBTown = meetingB["location_municipality"] {
                                                                if meetingATown < meetingBTown {
                                                                    ret = true
                                                                } else {
                                                                    if meetingATown == meetingBTown {
                                                                        if let meetingABorough = meetingA["location_city_subsection"] {
                                                                            if let meetingBBorough = meetingB["location_city_subsection"] {
                                                                                if meetingABorough < meetingBBorough {
                                                                                    ret = true
                                                                                } else {
                                                                                    if let sbAString = meetingA["service_body_bigint"] {
                                                                                        if let sbBString = meetingB["service_body_bigint"] {
                                                                                            if Int(sbAString) < Int(sbBString) {
                                                                                                ret = true
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                return ret
            })
            
            // This cleans everything up.
            self.resultTowns = inSearchManager!.resultTowns
            self.filter = BMLTAdminMeetingFilter(inList: searchResults)
            _ = self.filter.addFilter(BMLTAdminMeetingFilter.FilterKeys.Weekdays, inFilterValue: "1,2,3,4,5,6,7")
            self.appDisconnected = false
            self.searchManager = nil
            DispatchQueue.main.async(execute: {
                self.searchResultsTableView.reloadData()
                BMLTAdminAppDelegate.setConnectionStatus()  // Update the app delegate.
                self.townPickerView.reloadAllComponents()
                self.townPickerView.selectRow(0, inComponent: 0, animated: false)
                self.allDaysCheckbox.checked = true
                self.sundayCheckbox.checked = true
                self.mondayCheckbox.checked = true
                self.tuesdayCheckbox.checked = true
                self.wednesdayCheckbox.checked = true
                self.thursdayCheckbox.checked = true
                self.fridayCheckbox.checked = true
                self.saturdayCheckbox.checked = true
                if 0 < self.reloadID {
                    self.searchResultsTableView.layoutIfNeeded()
                    _ = self.selectMeetingByID(self.reloadID)
                }
            })
            self.needReload = false
        }
    }
    
    // MARK: - UITableViewDelegate Protocol Methods -
    /* ################################################################## */
    /**
     This is called when the user selects a table row. It will open the editor
     for the selected meeting.
     
     :param: tableView The Table View
     :param: indexPath The index path of the selected row.
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let meeting: BMLTAdminMeeting = self.filter[(indexPath as NSIndexPath).row] {
            if 0 < meeting.count {
                // We indicate whic meeting and table cell is "active," then go and edit that.
                tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
                self.currentlySelectedIndexPath = indexPath
                self.selectedMeeting = meeting
                self.performSegue(withIdentifier: self.sSegueID, sender: self)
            }
        }
    }
    
    // MARK: - UITableViewDataSource Protocol Methods -
    /* ################################################################## */
    /**
     This returns the number of Meetings we'll need to display.
     
     :param: tableView The Table View
     :param: section The 0-based index of the section.
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var ret: Int = 0
        
        if !self.needReload && (nil != self.filter) {
            ret = self.filter.count
        }
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This returns one cell for a Meeting.
     
     :param: tableView The Table View
     :param: indexPath The index path to the cell we need.
     
     :returns:   one cell object for the given Meeting
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ret: BMLTAdminSearchViewControllerTableCell! = nil
        
        if 0 < self.tableView(tableView, numberOfRowsInSection: (indexPath as NSIndexPath).section) {
            if let meeting = self.filter[(indexPath as NSIndexPath).row] {
                if let reuseID = meeting["id_bigint"] {
                    // If we can't reuse an existing cell, we create a new one.
                    ret = tableView.dequeueReusableCell(withIdentifier: reuseID) as? BMLTAdminSearchViewControllerTableCell
                    if nil == ret {
                        ret = BMLTAdminSearchViewControllerTableCell(inMeeting: meeting, style: UITableViewCellStyle.default, reuseIdentifier: reuseID)
                        // We load the prototype from our xib file.
                        if nil != ret {
                            if let view = Bundle.main.loadNibNamed("TablePrototype", owner: ret, options: nil)?.first as? UIView {
                                ret.addSubview(view)
                            }
                        }
                    } else {
                        ret.meeting = meeting
                    }
                    
                    ret.setNeedsLayout()
                }
            }
        }
        
        if (nil != ret) && (0 == ((indexPath as NSIndexPath).row % 2)) {
            ret.mainView.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0.5, alpha: 0.1)
        } else {
            if nil != ret {
                ret.mainView.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0)
            }
        }
        
        // KLUDGE ALERT! For some reason, when we do a ReloadData() after deleting a meeting, the table caches the number of rows, so a deleted row is requested. In order to keep the app from crashing, we send back an empty generic cell.
        if nil == ret {
            return UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "")
        }
        
        return ret
    }
    
    // MARK: - Callback Methods -
    /* ################################################################## */
    /**
     This is called when a weekday checkbox changes state.
     
     :param: inCheckbox This is the checkbox object that changed state.
     */
    @IBAction func handleWeekdayCheckboxSelection(_ inCheckbox: BMLTAdminCheckbox!) {
        var currentWeekdayFilter = self.filter.searchFilter[BMLTAdminMeetingFilter.FilterKeys.Weekdays.rawValue]
        
        if nil == currentWeekdayFilter {
            currentWeekdayFilter = ""
        }
        
        var weekdayArray: [Int] = []
        
        if let weekdayArrayString = currentWeekdayFilter?.components(separatedBy: ",") {
            for weekday in weekdayArrayString {
                if let weekdayInt: Int = Int(weekday) {
                    switch Int(weekdayInt) {
                    case 1...7:
                        if !weekdayArray.contains(weekdayInt) {
                            weekdayArray.append(weekdayInt)
                        }
                    default:
                        print("ERROR: Bad Weekday! \(weekdayInt)")
                    }
                }
            }
        }
        
        switch inCheckbox {
        case self.allDaysCheckbox:
            if nil != self.filter {
                self.sundayCheckbox.checked = inCheckbox.checked
                self.mondayCheckbox.checked = inCheckbox.checked
                self.tuesdayCheckbox.checked = inCheckbox.checked
                self.wednesdayCheckbox.checked = inCheckbox.checked
                self.thursdayCheckbox.checked = inCheckbox.checked
                self.fridayCheckbox.checked = inCheckbox.checked
                self.saturdayCheckbox.checked = inCheckbox.checked
                if inCheckbox.checked {
                    weekdayArray = [1,2,3,4,5,6,7]
                } else {
                    weekdayArray = []
                }
            }
        
        default:
            var index = 0
            
            switch inCheckbox {
            case self.sundayCheckbox:
                index = 1
            case self.mondayCheckbox:
                index = 2
            case self.tuesdayCheckbox:
                index = 3
            case self.wednesdayCheckbox:
                index = 4
            case self.thursdayCheckbox:
                index = 5
            case self.fridayCheckbox:
                index = 6
            case self.saturdayCheckbox:
                index = 7
            default:
                index = 0
            }
            
            if 0 < index {
                if inCheckbox.checked {
                    if      self.sundayCheckbox.checked
                        &&  self.mondayCheckbox.checked
                        &&  self.tuesdayCheckbox.checked
                        &&  self.wednesdayCheckbox.checked
                        &&  self.thursdayCheckbox.checked
                        &&  self.fridayCheckbox.checked
                        &&  self.saturdayCheckbox.checked {
                        self.allDaysCheckbox.checked = true
                    }

                    var found: Bool = false
                    for c in 0..<weekdayArray.count {
                        let weekday = weekdayArray[c]
                        if index == weekday {
                            found = true
                            break
                        }
                    }
                    
                    if !found {
                        weekdayArray.append(index)
                    }
                } else {
                    self.allDaysCheckbox.checked = false
                    for c in 0..<weekdayArray.count {
                        let weekday = weekdayArray[c]
                        if index == weekday {
                            weekdayArray.remove(at: c)
                            break
                        }
                    }
                }
            }
        }
        
        weekdayArray.sort(by: {$0 < $1})
        
        let stringVal = weekdayArray.map({String($0)}).joined(separator: ",")
        _ = self.filter.addFilter(BMLTAdminMeetingFilter.FilterKeys.Weekdays, inFilterValue: stringVal)
        self.searchResultsTableView.reloadData()
        BMLTAdminAppDelegate.setConnectionStatus()  // Update the app delegate.
    }
    
    // MARK: - Base Class Override Methods -
    /* ################################################################## */
    /**
     This icalled when the view is first selected.
     */
    override func runFirstSelectionTasks() {
        super.runFirstSelectionTasks()
        
        if self.appDisconnected || self.needReload {
            self.searchManager = nil
            self.selectedMeeting = nil
            self.needReload = true
            self.reloadID = 0
        }
        
        self.appDisconnected = false
    }
    
    /* ################################################################## */
    /**
     Called before the view will appear.
     
     :param: animated set to true if the view appearance will be animated
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if nil != self.selectedMeeting {
            self.selectedMeeting = nil
            self.searchResultsTableView.deselectRow(at: self.currentlySelectedIndexPath, animated: true)
            self.currentlySelectedIndexPath = IndexPath()
        }
        
        if self.needReload {
            self.reloadID = 0
            self._loadMeetings()
        }
    }
    
    /* ################################################################## */
    /**
        Called after the view has been loaded.
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        self.handlingLocationUpdate = false
        self.busyLabel.text = NSLocalizedString("LOCAL-SEARCH-BUSY-LABEL", comment: "")
        self.allDaysCheckboxLabel.text = NSLocalizedString("LOCAL-SEARCH-WEEKDAY-ALL", comment: "")
        self.sundayCheckboxLabel.text = NSLocalizedString("LOCAL-SEARCH-WEEKDAY-SUN", comment: "")
        self.mondayCheckboxLabel.text = NSLocalizedString("LOCAL-SEARCH-WEEKDAY-MON", comment: "")
        self.tuesdayCheckboxLabel.text = NSLocalizedString("LOCAL-SEARCH-WEEKDAY-TUE", comment: "")
        self.wednesdayCheckboxLabel.text = NSLocalizedString("LOCAL-SEARCH-WEEKDAY-WED", comment: "")
        self.thursdayCheckboxLabel.text = NSLocalizedString("LOCAL-SEARCH-WEEKDAY-THU", comment: "")
        self.fridayCheckboxLabel.text = NSLocalizedString("LOCAL-SEARCH-WEEKDAY-FRI", comment: "")
        self.saturdayCheckboxLabel.text = NSLocalizedString("LOCAL-SEARCH-WEEKDAY-SAT", comment: "")
        self.navigationItem.title = NSLocalizedString(self.navigationItem.title!, comment: "")
        self.title = NSLocalizedString(self.navigationItem.title!, comment: "")
        
        self.filter = nil
        self.selectedMeeting = nil

        self.whereAmIButton.setTitle(NSLocalizedString(self.whereAmIButton.title(for: UIControlState())!, comment: ""), for: UIControlState())
        
        self.needReload = true
    }
}

