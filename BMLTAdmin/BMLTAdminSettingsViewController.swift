//
//  BMLTAdminSettingsViewController.swift
//  BMLTAdmin
/**
 :license:
 Created by MAGSHARE.
 This is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 BMLT is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this code.  If not, see <http://www.gnu.org/licenses/>.
 */

import UIKit

// MARK: - Classes -
/* ###################################################################################################################################### */
/**
 This class displays the Settings tab.
 */
class BMLTAdminSettingsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var timeoutItemsContainerView: UIView!
    @IBOutlet weak var timeoutPickerLabel: UILabel!
    @IBOutlet weak var timeoutPickerView: UIPickerView!
    @IBOutlet weak var sslLabel: UILabel!
    @IBOutlet weak var sslSwitch: UISwitch!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var durationEditText: UITextField!
    @IBOutlet weak var durationStepper: UIStepper!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var magshareLabel: UILabel!
    @IBOutlet weak var magshareURILabel: UILabel!
    
    /* ################################################################## */
    /**
     Returns the time from the start time text field as an integer (number of minutes).
     
     :returns: an Int, 0 to 1439
     */
    func getDurationValueFromTextAsInteger() -> Int {
        return BMLTAdminAppDelegate.parseTimeValueAsInteger ( self.durationEditText.text )
    }
    
    /* ################################################################## */
    /**
     Sets the duration and the text item from an integer.
     
     :param: inValue an Int, 0 to 1439
     :param: inChangeTextItem If true, then we also change the text item.
     */
    func setDurationValueFromInteger( _ inValue: Int, inChangeTextItem: Bool ) {
        if (0 <= inValue) && (1440 > inValue) {
            BMLTAdminAppDelegate.v_appPrefs.defaultDuration = inValue
            self.durationStepper.value = Double (inValue)
            let hours = inValue / 60
            let minutes = inValue - (hours * 60)
            
            var hrs: String = ""
            if 10 > hours {
                hrs = "0"
            }
            
            hrs += String(hours)
            
            var min = ""
            if 10 > minutes {
                min = "0" + min
            }
            
            min += String(minutes)
            
            if inChangeTextItem {
                self.durationEditText.text = String(hours) + ":" + min
            }
            
            if  (nil != BMLTAdminAppDelegate.v_newMeetingController) && !BMLTAdminAppDelegate.v_newMeetingController.meetingDirty {
                BMLTAdminAppDelegate.v_newMeetingController.appDisconnected = true
                BMLTAdminAppDelegate.v_newMeetingController.meeting = nil
                BMLTAdminAppDelegate.v_newMeetingController.compMeeting = nil
            }
        }
    }
    
    /* ################################################################## */
    /**
     Called when the view first loads.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.timeoutPickerLabel.text = NSLocalizedString(self.timeoutPickerLabel.text!, comment: "")
        self.sslLabel.text = NSLocalizedString(self.sslLabel.text!, comment: "")
        self.durationLabel.text = NSLocalizedString(self.durationLabel.text!, comment: "")
        self.magshareLabel.text = NSLocalizedString(self.magshareLabel.text!, comment: "")
        self.magshareURILabel.text = NSLocalizedString(self.magshareURILabel.text!, comment: "")
        
        var selectedRow: Int = 0
        for row in BMLTAdminAppDelegate.s_availableTimeoutsInSeconds {
            let valueOfRow: Int = row.rawValue
            
            if valueOfRow == BMLTAdminAppDelegate.v_autoDisconnectTimeoutInSeconds {
                break
            }
            
            selectedRow += 1
        }
        
        self.timeoutPickerView.selectRow(selectedRow, inComponent: 0, animated: true)
        self.sslSwitch.isOn = BMLTAdminAppDelegate.v_appPrefs.requireSSL
        self.setDurationValueFromInteger(BMLTAdminAppDelegate.v_appPrefs.defaultDuration, inChangeTextItem: true)
    }
    
    // MARK: - UIPickerViewDataSource Methods -
    /* ################################################################## */
    /**
     We only have 1 component.
     
     :param: pickerView The UIPickerView being checked
     
     :returns: 1
     */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    /* ################################################################## */
    /**
     We will always have 2 more than the number of towns, as we have the first and second rows.
     
     :param: pickerView The UIPickerView being checked
     
     :returns: Either 0, or the number of available times.
     */
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return BMLTAdminAppDelegate.s_availableTimeoutsInSeconds.count
    }
    
    // MARK: - UIPickerViewDelegate Methods -
    /* ################################################################## */
    /**
     This returns the name for the given row.
     
     :param: pickerView The UIPickerView being checked
     :param: row The row being checked
     :param: component The component (always 0)
     
     :returns: a view, containing a label with the string for the row.
     */
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let size = pickerView.rowSize(forComponent: 0)
        var frame = pickerView.bounds
        frame.size.height = size.height
        frame.origin = CGPoint.zero
        
        let ret:UIView = UIView(frame: frame)
        
        ret.backgroundColor = UIColor.clear
        
        let label = UILabel(frame: frame)
        
        label.textAlignment = NSTextAlignment.center
        
        var textColor = UIColor.white
        var backgroundColor = UIColor.clear
        
        let value = BMLTAdminAppDelegate.s_availableTimeoutsInSeconds[row].rawValue
        
        var string: NSString = ""
        
        switch value {
        case BMLTAdminAppDelegate.AutoDisconnectTimeouts.never.rawValue:
            textColor = UIColor.white
            backgroundColor = UIColor.red
            string = NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-NEVER", comment: "") as NSString
            
        case 1:
            string = NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-FORMAT-SECOND", comment: "") as NSString
            
        case 2...15:
            textColor = UIColor.green
            string = string.appendingFormat(NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-FORMAT-SECONDS", comment: "") as NSString, value)
            
        case 16...30:
            textColor = UIColor.white
            string = string.appendingFormat(NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-FORMAT-SECONDS", comment: "") as NSString, value)
            
        case 31...59:
            textColor = UIColor.orange
            string = string.appendingFormat(NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-FORMAT-SECONDS", comment: "") as NSString, value)
            
        case 60:
            textColor = UIColor.orange
            string = NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-FORMAT-MINUTE", comment: "") as NSString
            
        case 60...120:
            textColor = UIColor.orange
            string = string.appendingFormat(NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-FORMAT-MINUTES", comment: "") as NSString, value / 60)
            
        case 121...(BMLTAdminAppDelegate.s_availableTimeoutsInSeconds[BMLTAdminAppDelegate.s_availableTimeoutsInSeconds.count - 2].rawValue):
            textColor = UIColor.red
            string = string.appendingFormat(NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-FORMAT-MINUTES", comment: "") as NSString, value / 60)
            
        case (BMLTAdminAppDelegate.s_availableTimeoutsInSeconds[BMLTAdminAppDelegate.s_availableTimeoutsInSeconds.count - 1].rawValue):
            textColor = UIColor.white
            backgroundColor = UIColor.red
            string = string.appendingFormat(NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-FORMAT-MINUTES", comment: "") as NSString, value / 60)
            
        default:
            textColor = UIColor.white
            backgroundColor = UIColor(red: 0, green: 0.75, blue: 0, alpha: 1)
            string = NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-IMMEDIATELY", comment: "") as NSString
        }
        
        label.backgroundColor = backgroundColor
        label.textColor = textColor
        label.text = string as String
        
        ret.addSubview(label)
        
        return ret
    }
    
    /* ################################################################## */
    /**
     This is called when the user finishes selecting a row.
     We use this to add the selected town to the filter.
     
     If it is one of the top 2 rows, we select the first row, and ignore it.
     
     :param: pickerView The UIPickerView being checked
     :param: row The row being checked
     :param: component The component (always 0)
     */
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        BMLTAdminAppDelegate.v_autoDisconnectTimeoutInSeconds = BMLTAdminAppDelegate.s_availableTimeoutsInSeconds[row].rawValue
        BMLTAdminAppDelegate.v_appPrefs.timeoutInSeconds = BMLTAdminAppDelegate.v_autoDisconnectTimeoutInSeconds
    }
    
    /* ################################################################## */
    /**
     This reacts to the MAGSHARE branding items being hit.
     
     :param: sender The button.
     */
    @IBAction func brandingButtonHit(_ sender: AnyObject?) {
        UIApplication.shared.openURL(URL(string: NSLocalizedString("LOCAL-MAGSHARE-URI", comment:""))!)
    }
    
    /* ################################################################## */
    /**
     This reacts to the duration edit field being changed.
     
     :param: sender The edit field.
     */
    @IBAction func durationEditTextChanged(_ sender: UITextField) {
        self.setDurationValueFromInteger(self.getDurationValueFromTextAsInteger(), inChangeTextItem: false)
    }

    /* ################################################################## */
    /**
     This reacts to the duration stepper being changed.
     
     :param: sender The stepper.
     */
    @IBAction func durationStepperChanged(_ sender: UIStepper) {
        self.closeKeyboard(sender)
        // This makes sure that it is always a multiple of 5.
        let remainder = sender.value.truncatingRemainder(dividingBy: 5)
        if (0 != remainder) && (0 < sender.value) {
            sender.value -= remainder
        }
        // We update the start time value and the edit field.
        self.setDurationValueFromInteger( Int(sender.value), inChangeTextItem: true )
    }

    /* ################################################################## */
    /**
     This reacts to the SSL switch being changed.
     
     :param: sender The button that was hit.
     */
    @IBAction func sslSwitchChanged(_ sender: UISwitch) {
        BMLTAdminAppDelegate.v_appPrefs.requireSSL = sender.isOn
        BMLTAdminAppDelegate.v_newMeetingController.appDisconnected = true
    }
    
    /* ################################################################## */
    /**
     This displays an information button for the timeout picker.
     
     :param: sender The button that was hit.
     */
    @IBAction func timeoutInfoButtonHit(_ sender: UIButton) {
        let alertController = UIAlertController(title: NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-EXPLAIN-TITLE", comment: ""), message: NSLocalizedString("LOCAL-SETTINGS-TIMEOUT-EXPLAIN", comment: ""), preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-SETTINGS-EXPLAIN-OK-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: nil)
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /* ################################################################## */
    /**
     This displays an information button for the SSL switch.
     
     :param: sender The button that was hit.
     */
    @IBAction func sslInfoButtonHit(_ sender: UIButton) {
        let alertController = UIAlertController(title: NSLocalizedString("LOCAL-SETTINGS-SSL-EXPLAIN-TITLE", comment: ""), message: NSLocalizedString("LOCAL-SETTINGS-SSL-EXPLAIN", comment: ""), preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-SETTINGS-EXPLAIN-OK-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: nil)
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /* ################################################################## */
    /**
     This displays an information button for the default duration.
     
     :param: sender The button that was hit.
     */
    @IBAction func durationInfoButtonHit(_ sender: UIButton) {
        let alertController = UIAlertController(title: NSLocalizedString("LOCAL-SETTINGS-DURATION-EXPLAIN-TITLE", comment: ""), message: NSLocalizedString("LOCAL-SETTINGS-DURATION-EXPLAIN", comment: ""), preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: NSLocalizedString("LOCAL-SETTINGS-EXPLAIN-OK-BUTTON-TITLE", comment: ""), style: UIAlertActionStyle.default, handler: nil)
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
