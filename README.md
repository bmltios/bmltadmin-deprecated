DESCRIPTION
-----------

Project homepage: http://bmlt.magshare.net/

BMLTAdmin is an iOS app that is designed to be an iPhone- or iPad-based meeting list administrator tool.

It will allow a Service Body Administrator (not a Server Administrator or Observer) to log into a Root Server,
and do searches and edit meetings.

It is designed to be very secure, using the same session-based connection that the browser-based administration
uses (it's not Fort Knox, but is pretty good).

BMLTAdmin uses TouchID, for devices that support it, and will allow the administrator to administer more than one
Root Server, or log into the same Root Server with more than one ID.

This project uses this publicly-accessible library to ease one of the more complex tasks:

* [FXKeychain][id1] (Keychain Abstraction)

REQUIREMENTS
------------
This is an iOS app, made for iPhones and iPads. It is only available via the Apple App Store, and requires iOS 8.0 or above.

INSTALLATION
------------

This will be installed via the Apple App Store, and will be a free app.

LICENSE
-------
This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BMLT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code.  If not, see <http://www.gnu.org/licenses/>.

CHANGELIST
----------
***Version 1.1.2001* ** *- TBD*
- Fixed a bug, in which locking the app would invalidate the timeout.

***Version 1.1.2000* ** *- September 25, 2016*
- Swift 3 refactor. No visible UX changes.

***Version 1.0.1.3000* ** *- August 12, 2016*
- This one is straight to the App Store, to save time.
- Fixed a bug, in which a badly-formed login could bork the app.

***Version 1.0.0.3000* ** *- August 8, 2016*
- First App Store Submission
- Now make sure that the login ID and password fields are cleared if the URI is completely deleted.
- Simplified the version checking mechanism slightly, in order to improve the granularity of the server test.

***Version 1.0.0.2015* ** *- August 6, 2016*
- There was a bug in the hiding flag for the "Why Can't I Connect?" button.
- Modified the URI in the Settings page to point to [the app page on the site](http://bmlt.magshare.net/bmltadmin).
- Added alerts after restoring deleted meetings or rolling back meetings.
- Testing using "List" instead of "Search".

***Version 1.0.0.2014* ** *- August 6, 2016*
- Turned off scrolling in the map, and made the map 100% the width of the window. This means that you need to tap to change the marker (which re-centers the map), or zoom out/in. The reason for this, was because the map would interfere with scrolling the page.
- Added an alert that displays an explanation of why the connection won't allow you to log in.

***Version 1.0.0.2013* ** *- August 5, 2013*
- Fixed a bug in which text entry into the "Extra Info" text field did not result in the meeting being marked "dirty."
- Fixed some issues with the way things were getting enabled and disabled in the "Connect" tab.
- Fixed some problems with the way that logins were being stored (and deleted) in the "Connect" tab.

***Version 1.0.0.2012* ** *- August 3, 2016*
- This is the first approved beta test version.
- Fixed a memory leak in the history listing.
- added code to ensure that certain methods don't try to access data that isn't ready (probably useless, but what the heck).

***Version 1.0.0.2011* ** *- August 2, 2016*
- Addressed an issue where an erroneous error could get reported after the very first successful server connection.

***Version 1.0.0.2010* ** *- August 1, 2016*
- Added a number of dispatch_async calls inside of communication handlers, to ensure that UI stuff gets handled on main threads.
- Moved the setting of the connection session until after the app starts, in case there are issues.

***Version 1.0.0.2009* ** *- July 31, 2016*
- Reworked a bunch of project-level stuff, in the hope that this will finally give Apple what it needs to prevent a startup crash.

***Version 1.0.0.2008* ** *- July 30, 2016*
- Fixed an issue, where the "Where Am I now?" slider would happen twice.
- Made the "Where Am I Now?" button more visible.
- Made the map slightly smaller, in order to accommodate scrolling a bit better.

***Version 1.0.0.2007* ** *- July 29, 2016*

- Fixed an issue that prevented newly-restored deleted meetings from immediately loading.
- Fixed an issue (in the server) that caused problems when creating new, empty meetings.
- The app now checks more than just the server version. It now also makes sure the server has all the data keys we need.
- The security framework was not being linked into the release build, which caused a startup crash.

***Version 1.0.0.2006* ** *- July 28, 2016*

- First public beta test.

[id1]: https://github.com/nicklockwood/FXKeychain
